# EasyReview #

EasyReview é uma aplicação destinada a revisão de código para repositórios Git e SVN. Seu desenvolvimento foi focado em duas premissas básicas: ser fácil de usar e fácil de instalar. Muitas das ferramentas de revisão de código existentes no mercado não atentem nenhuma das duas premissas. 

Outro ponto importante é que o foco da ferramenta é na revisão de código e não na sua aprovação. Embora possam parecer iguais, revisão e aprovação são coisas distintas. A aprovação de código é muito importante para projetos open-source, aonde uma alteração deve analisada e aprovada pelos membros do projeto. O pull request de ferramentas como Bitbuket e GitHub serve para isso. Contudo em muitos projetos, especialmente em equipes pequenas e distribuídas (as vezes com somente um programador) isso se torna enviável principalmente com integração contínua de software. 

A revisão transcende a aprovação e pode ser realizada mesmo após alguma alteração ter entrado em produção pois é ideal para troca de conhecimento sobre diferentes projetos e aprendizagem sobre diferentes técnicas e ferramentas para membros de uma empresa ou até mesmo para membros de uma mesma equipe. EasyReview não impede que você adote alguma forma de aprovação de código porém da liberdade para que você escolha a metodologia que melhor se adeque a sua equipe e a sua empresa.

* * *

### Requisitos ###

Para utilização da ferramenta é necessário:

* Java! Deve ter sido instalado no computador no qual for executado a aplicação a [JRE 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jre8-downloads-2133155.html) ou a [JDK 8](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk8-downloads-2133151.html)
* Ter o [Git](https://git-scm.com/book/pt-br/v1/Primeiros-passos-Instalando-Git) instalado
* Ter alguma ferramenta para descompactação de arquivos

### Baixando e Executando ###

* Baixe a última versão: [easyreview-1.6.3.jar](https://bitbucket.org/alexandre_rodrigues_poa/easyreview/raw/158f43f1b8a16d5d9c0e3bbf77ec56fb16237207/easyreview-1.6.3.jar)
* Abra o terminal/prompt localize o diretório onde baixou a versão e digite: java -jar easyreview-1.6.3.jar
* Abra seu navegador e preferido e digite [http://localhost:8080/](http://localhost:8080/)
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)
* Para o primeiro login utilize **sysadmin@easyreview.org** para o usuário e **sysadmin** para a senha (após cadastrar um usuário como administrador o usuário **sysadmin** é desabilitado)
* Cadastre um novo usuário administrador [Usuários->Inserir Novo](http://localhost:8080/users/insertNew)
* Faça logout e realize o login com o novo usuário

Pronto!!! Agora é só cadastrar os novos usuários e os projetos e usar!!!! Não esqueça de vincular os projetos aos usuários, então cadastre primeiro os usuários e e depois os projetos