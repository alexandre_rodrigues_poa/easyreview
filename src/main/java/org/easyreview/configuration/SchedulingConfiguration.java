package org.easyreview.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

@Configuration
@PropertySource(value="file:scheduling.properties", ignoreResourceNotFound=true)
public class SchedulingConfiguration {
}
