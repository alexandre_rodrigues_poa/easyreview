package org.easyreview.configuration;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

@Configuration
@PropertySource(value="file:mail.properties", ignoreResourceNotFound=true)
public class MailConfiguration {

	@Value("${mail.host:localhost}")
	private String host;
	@Value("${mail.port:25}")
	private int port;  
	@Value("${mail.username:username}")
	private String username;
	@Value("${mail.password:password}")
	private String password;
	@Value("${mail.smtp.auth:false}")
	private boolean smtpAuthentication;
	@Value("${mail.smtp.starttls.enable:false}")
	private boolean smtpStarttlsEnable;

	@Bean
	public JavaMailSender javaMailSender() {
		JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        
		Properties mailProperties = new Properties();
        mailProperties.put("mail.smtp.auth", smtpAuthentication);
        mailProperties.put("mail.smtp.starttls.enable", smtpStarttlsEnable);
        
        mailSender.setJavaMailProperties(mailProperties);
        mailSender.setHost(host);
        mailSender.setPort(port);
        mailSender.setUsername(username);
        mailSender.setPassword(password);
        
        return mailSender;
   }
}
