package org.easyreview.configuration;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@EntityScan(basePackages = {"org.easyreview.domain"})
@EnableJpaRepositories(basePackages = {"org.easyreview.domain"})
public class RepositoryConfiguration {}
