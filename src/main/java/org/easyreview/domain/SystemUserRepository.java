package org.easyreview.domain;

import java.util.List;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

@Repository
public class SystemUserRepository {

	@PersistenceContext
	private EntityManager entityManager;
	
	public boolean emailInUse(Email email) {
		TypedQuery<Boolean> query = createQueryEmailInUse(email);
		return query.getSingleResult();
	}

	public List<SystemUser> listAllActive() {
		TypedQuery<SystemUser> query = queryListAllActiveUsers();
		return query.getResultList();
	}

	public Long findIdUserBy(Email email) {
		TypedQuery<Long> query = createQueryFindIdFromUserByEmail(email);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public SystemUser findUserBy(Long id) {
		return entityManager.find(SystemUser.class, id);
	}

	public SystemUser findUserBy(Email email) {
		TypedQuery<SystemUser> query = createQueryFindUserByEmail(email);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public boolean hasAdminUser() {
		TypedQuery<Boolean> query = createQueryHasAdmin();
		return query.getSingleResult();
	}
	
	public boolean notHasAdminUser() {
		return !hasAdminUser();
	}

	private TypedQuery<Boolean> createQueryHasAdmin() {
		String jpql = "SELECT (count(user) > 0) FROM SystemUser user WHERE user.removed = false AND user.isAdmin = true" ;
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		return query;
	}

	private TypedQuery<Boolean> createQueryEmailInUse(Email email) {
		String jpql = "SELECT (count(user) > 0) FROM SystemUser user WHERE user.removed = false AND user.email = :email" ;
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		query.setParameter("email", email);
		return query;
	}

	private TypedQuery<SystemUser> queryListAllActiveUsers() {
		String jpql = "SELECT user FROM SystemUser user WHERE user.removed = false";
		TypedQuery<SystemUser> query = entityManager.createQuery(jpql, SystemUser.class);
		return query;
	}

	private TypedQuery<Long> createQueryFindIdFromUserByEmail(Email email) {
		String jpql = "SELECT user.id FROM SystemUser user WHERE user.removed = false AND user.email = :email" ;
		
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("email", email);
		return query;
	}

	private TypedQuery<SystemUser> createQueryFindUserByEmail(Email email) {
		String jpql = "SELECT user FROM SystemUser user WHERE user.removed = false AND user.email = :email" ;
		TypedQuery<SystemUser> query = entityManager.createQuery(jpql, SystemUser.class);
		query.setParameter("email", email);
		return query;
	}
}
