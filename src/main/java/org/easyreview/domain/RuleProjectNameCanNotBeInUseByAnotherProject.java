package org.easyreview.domain;

import org.easyreview.factorys.ContainerFactory;

public class RuleProjectNameCanNotBeInUseByAnotherProject {

	private String projectName;
	private long projectId;

	public RuleProjectNameCanNotBeInUseByAnotherProject(String projectName, long projectId) {
		this.projectName = projectName;
		this.projectId = projectId;
	}

	public boolean inAccord() {
		return nameNotInUse() || nameInUserByInformedProject();
	}

	public boolean notInAccord() {
		return !inAccord();
	}

	private boolean nameNotInUse() {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		return !repository.nameInUse(projectName);
	}

	private boolean nameInUserByInformedProject() {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		Long id = repository.findActiveProjectIdBy(projectName);
		return id.equals(projectId);
	}
}
