package org.easyreview.domain;

import java.io.File;
import java.util.*;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

@Repository
public class ReviewRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public List<Review> listReviewsFrom(String commitId, File file) {
		TypedQuery<Review> query = createQuerListReviewsFrom(commitId, file);
		return query.getResultList();
	}

	public long countReviewsFrom(String commitId, File file) {
		TypedQuery<Long> query = createQueryCountReviewsFrom(commitId, file);
		return query.getSingleResult();
	}

	public long countFilesWithReviewFrom(String commitId) {
		TypedQuery<Long> query = createQueryCountFilesWithReviewFrom(commitId);
		return query.getSingleResult();
	}

	public Review findReviewBy(ReplyReview reply) {
		TypedQuery<Review> query = createQueryFindReviewBy(reply);
		return query.getSingleResult();
	}

	public boolean userReviewedTheCommit(String commitId, SystemUser author) {
		TypedQuery<Boolean> query = createQueryUserReviewedTheCommit(commitId, author);
		return query.getSingleResult();
	}

	public boolean userReviewedTheFile(String commitId, File file, SystemUser user) {
		TypedQuery<Boolean> query = createQueryUserReviewedTheFile(commitId, file, user);
		return query.getSingleResult();
	}

	public List<CodeAnalysisStatus> listCodeAnalysisStatusFrom(String commitId) {
		TypedQuery<CodeAnalysisStatus> query = createQueryListCodeAnalysisStatusFrom(commitId);
		return query.getResultList();
	}

	public List<CodeAnalysisStatus> listCodeAnalysisStatusFrom(String commitId, File file) {
		TypedQuery<CodeAnalysisStatus> query = createQueryListCodeAnalysisStatusFrom(commitId, file);
		return query.getResultList();
	}

	private TypedQuery<Review> createQuerListReviewsFrom(String commitId, File file) {
		String jpql = "SELECT review FROM Review review WHERE review.commitId = :commitId AND review.file = :file";
		TypedQuery<Review> query = entityManager.createQuery(jpql, Review.class);
		query.setParameter("commitId", commitId);
		query.setParameter("file", file);
		return query;
	}

	private TypedQuery<Long> createQueryCountReviewsFrom(String commitId, File file) {
		String jpql = "SELECT count(review.id) FROM Review review WHERE review.commitId = :commitId AND review.file = :file";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("commitId", commitId);
		query.setParameter("file", file);
		return query;
	}

	private TypedQuery<Long> createQueryCountFilesWithReviewFrom(String commitId) {
		String jpql = "SELECT COUNT(DISTINCT review.file) FROM Review review WHERE review.commitId = :commitId";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("commitId", commitId);
		return query;
	}

	private TypedQuery<Review> createQueryFindReviewBy(ReplyReview reply) {
		String jpql = "SELECT review FROM Review review WHERE :reply member of review.replies";
		TypedQuery<Review> query = entityManager.createQuery(jpql, Review.class);
		query.setParameter("reply", reply);
		return query;
	}

	private TypedQuery<Boolean> createQueryUserReviewedTheCommit(String commitId, SystemUser author) {
		String jpql = "SELECT COUNT(review) > 0 FROM Review review WHERE review.commitId = :commitId AND review.author = :author";
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		query.setParameter("commitId", commitId);
		query.setParameter("author", author);
		return query;
	}

	private TypedQuery<Boolean> createQueryUserReviewedTheFile(String commitId, File file, SystemUser author) {
		String jpql = "SELECT COUNT(review) > 0 FROM Review review WHERE review.author = :author AND review.commitId = :commitId AND review.file =:file";
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		query.setParameter("author", author);
		query.setParameter("commitId", commitId);
		query.setParameter("file", file);
		return query;
	}

	private TypedQuery<CodeAnalysisStatus> createQueryListCodeAnalysisStatusFrom(String commitId) {
		String jpql = "SELECT DISTINCT review.analysisStatus FROM Review review WHERE review.commitId = :commitId";
		TypedQuery<CodeAnalysisStatus> query = entityManager.createQuery(jpql, CodeAnalysisStatus.class);
		query.setParameter("commitId", commitId);
		return query;
	}

	private TypedQuery<CodeAnalysisStatus> createQueryListCodeAnalysisStatusFrom(String commitId, File file) {
		String jpql = "SELECT DISTINCT review.analysisStatus FROM Review review WHERE review.commitId = :commitId AND review.file = :file";
		TypedQuery<CodeAnalysisStatus> query = entityManager.createQuery(jpql, CodeAnalysisStatus.class);
		query.setParameter("commitId", commitId);
		query.setParameter("file", file);
		return query;
	}
}
