package org.easyreview.domain;

import java.io.File;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReviewManipulationService {

	@Autowired
	private NotificationService notify;
	
	@Transactional
	public Review insertNewReview(Long userId, Long projectId, String commitId, String fileName, CodeAnalysisStatus analysisStatus, String comment, String urlAccess) {
		SystemUser author = SystemUser.findBy(userId);
		Project project = Project.findBy(projectId);

		GitCommit commit = getCommit(project, commitId);
		File file = new File(fileName);
		
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		
		notify.addedNewReview(project, urlAccess);
		
		return review;
	}

	@Transactional
	public Review editReview(Long userId, Long reviewId, CodeAnalysisStatus analysisStatus, String comment, String urlAccess) {
		SystemUser author = SystemUser.findBy(userId);
		
		Review review = Review.findBy(reviewId);
		review.setAnalysisStatus(analysisStatus, author);
		review.setComment(comment, author);
		
		Project project = review.getProject();
		notify.editedReview(project, urlAccess);
		
		return review;
	}

	private GitCommit getCommit(Project project, String commitId) {
		GitRepository gitRepository = new GitRepository(project);
		GitCommit commit = gitRepository.findCommitBy(commitId);
		return commit;
	}
}
