package org.easyreview.domain;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.*;

import static org.easyreview.repositories.git.RepositoryFactory.createFrom;

import org.easyreview.ExceptionsMessages;
import org.easyreview.repositories.git.ServiceGitCommit;
import org.easyreview.utils.DateConverter;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

public class GitRepository {

	private Project project;

	GitRepository(Project project) {
		validate(project);
		this.project = project;
	}

	public int getNumberOfCommits() {
		File repositoryPath = project.getRepositoryPath();
		try (Repository repository = createFrom(repositoryPath)){
			return countNumberOfCommits(repository);
		} catch (GitAPIException | IOException e) {
			throw new GitException(e);
		}
	}

	public List<GitCommit> listCommits() {
		File repositoryPath = project.getRepositoryPath();
		try (Repository repository = createFrom(repositoryPath)) {
			return listCommitsFrom(repository);
		} catch (GitAPIException | IOException e) {
			throw new GitException(e);
		}
	}

	public GitCommit findCommitBy(String commitId) {
		
		File repositoryPath = project.getRepositoryPath();
		try (Repository repository = createFrom(repositoryPath)) {
			return findCommityById(commitId, repository);
		} catch (GitAPIException | IOException e) {
			throw new GitException(e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + project.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof GitRepository))
			return false;
		
		GitRepository other = (GitRepository) obj;
		return project.equals(other.project);
	}

	private void validate(Project project) {
		if (project == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("project"));
	}

	private int countNumberOfCommits(Repository repository) throws GitAPIException, NoHeadException, IOException {
		Date date = getFromDate();
				
		ServiceGitCommit service = new ServiceGitCommit(repository);
		return service.countNumberOfCommitsFrom(date);
	}

	private List<GitCommit> listCommitsFrom(Repository repository) throws GitAPIException, NoHeadException, IOException {
		Date date = getFromDate();
		
		List<GitCommit> result = new ArrayList<>();
		ServiceGitCommit service = new ServiceGitCommit(repository);
		
		for(RevCommit commit : service.listCommitsFrom(date))
			result.add(new GitCommit(project, commit));
		
		return result;
	}

	private Date getFromDate() {
		LocalDate date = project.getCommitsFromDate();
		return DateConverter.localDateToDate(date);
	}

	private GitCommit findCommityById(String commitId, Repository repository) throws NoHeadException, GitAPIException, IOException {
		ServiceGitCommit service = new ServiceGitCommit(repository);
		RevCommit commit = service.findCommitBy(commitId); 
		return new GitCommit(project, commit);
	}
}
