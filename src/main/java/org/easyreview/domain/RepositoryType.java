package org.easyreview.domain;

import org.easyreview.repositories.git.GitPullCommandBuilder;
import org.easyreview.repositories.git.GitSvnFetchCommandBuilder;
import org.easyreview.repositories.git.GitUpdateCommandBuilder;

public enum RepositoryType {
	GIT("Git", new GitPullCommandBuilder()), 
	SVN("Svn", new GitSvnFetchCommandBuilder());

	private String description;
	private GitUpdateCommandBuilder gitUpdateCommandBuilder;
	
	RepositoryType(String description, GitUpdateCommandBuilder gitUpdateCommandBuilder) {
		this.description = description;
		this.gitUpdateCommandBuilder = gitUpdateCommandBuilder;
	}

	public String getDescription() {
		return description;
	}

	public GitUpdateCommandBuilder getGitUpdateCommandBuilder() {
		return gitUpdateCommandBuilder;
	}
}
