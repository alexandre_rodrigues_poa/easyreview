package org.easyreview.domain;

import java.io.File;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

@Service
public class ProjectManipulationService {

	@Transactional
	public Project insertNewProject(String name, String repositoryPath, Set<Long> userIDs, LocalDate commitsFromDate, RepositoryType type) {
		Set<SystemUser> users = listUsers(userIDs);
		return Project.newInstance(name, new File(repositoryPath), type, users, commitsFromDate);
	}

	@Transactional
	public Project editProject(Long projectId, String name, String repositoryPath, RepositoryType repositoryType, Set<Long> userIDs, LocalDate commitsFromDate) {
		Set<SystemUser> users = listUsers(userIDs);
		
		Project project = Project.findBy(projectId);
		project.setName(name);
		project.setRepositoryPath(new File(repositoryPath));
		project.setRepositoryType(repositoryType);
		project.setUsers(users);
		project.setCommitsFromDate(commitsFromDate);
		
		return project;
	}

	@Transactional
	public Project removeProject(Long projectId) {
		Project project = Project.findBy(projectId);
		project.remove();
		return project;
	}

	private Set<SystemUser> listUsers(Set<Long> userIDs) {
		Set<SystemUser> users = new HashSet<>();
		for(Long userId : userIDs)
			users.add(SystemUser.findBy(userId));
		return users;
	}
}
