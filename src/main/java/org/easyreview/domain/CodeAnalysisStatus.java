package org.easyreview.domain;

public enum CodeAnalysisStatus {
	OK, CAUTION, ERROR; 
}
