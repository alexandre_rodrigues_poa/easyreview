package org.easyreview.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.easyreview.mail.MailSendingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class NotificationService {
	
	@Autowired
	private MailSendingService mail;

	public void addedNewReview(Project project, String accessUrl) {
		String subject = "EasyReview - Added new review for the project " + project.getName();
		String message = String.format("Added new review for the project %s! <a href='%s'>Click here to access</a>", project.getName(), accessUrl);

		String emails[] = getEmailsAdressFrom(project);
		mail.send(emails, subject, message, true);
	}

	public void editedReview(Project project, String accessUrl) {
		String subject = "EasyReview - Edited the review of the project " + project.getName();
		String message = String.format("Edited the review of the project %s! <a href='%s'>Click here to access</a>", project.getName(), accessUrl);
		String emails[] = getEmailsAdressFrom(project);
		mail.send(emails, subject, message, true);
	}

	public void addedNewReply(Project project, String accessUrl) {
		String subject = "EasyReview - Added new reply to revision of the project " + project.getName();
		String message = String.format("Added new reply to revision of the project %s! <a href='%s'>Click here to access</a>", project.getName(), accessUrl);
		String emails[] = getEmailsAdressFrom(project);
		mail.send(emails, subject, message, true);
	}

	public void editedReply(Project project, String accessUrl) {
		String subject = "EasyReview - Edited the reply of the review! Project: " + project.getName();
		String message = String.format("Edited the reply of the review! Project: %s! <a href='%s'>Click here to access</a>", project.getName(), accessUrl);
		String emails[] = getEmailsAdressFrom(project);
		mail.send(emails, subject, message, true);
	}

	private String[] getEmailsAdressFrom(Project project) {
		Set<SystemUser> membersOfProject =  project.getUsers();
		List<String> list = new ArrayList<>();
		for(SystemUser member : membersOfProject) {
			Email email = member.getEmail();
			list.add(email.toString());
		}
		String emails[] = new String[list.size()];
		list.toArray(emails);
		
		return emails;
	}
}
