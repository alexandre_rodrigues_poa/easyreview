package org.easyreview.domain;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReplyReviewManipulationService {

	@Autowired
	private NotificationService notify;
	
	@Autowired
	private ReviewRepository reviewRepository;
	
	@Transactional
	public ReplyReview insertNewReply(Long userId, Long reviewId, String text, String accessUrl) {
		SystemUser author = SystemUser.findBy(userId);
		Review review = Review.findBy(reviewId);
		
		ReplyReview reply = ReplyReview.newInstance(author, text);
		review.addIfNotExist(reply);
		
		Project project = review.getProject();
		notify.addedNewReply(project, accessUrl);
		
		return reply;
	}

	@Transactional
	public ReplyReview editReply(Long userId, Long replyId, String text, String accessUrl) {
		SystemUser author = SystemUser.findBy(userId);
		ReplyReview reply = ReplyReview.findBy(replyId);
		reply.setText(text, author);
		
		Review review = reviewRepository.findReviewBy(reply);
		Project project = review.getProject();
		notify.editedReply(project, accessUrl);

		return reply;
	}
}
