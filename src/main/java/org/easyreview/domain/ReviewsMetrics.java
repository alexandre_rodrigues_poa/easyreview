package org.easyreview.domain;

import org.easyreview.ExceptionsMessages;
import org.easyreview.factorys.ContainerFactory;

public class ReviewsMetrics {

	private ReviewsMetricsRepository repository;
	private Project project;
	
	public ReviewsMetrics(Project project) {
		validate(project);
		this.repository = ContainerFactory.get(ReviewsMetricsRepository.class);
		this.project = project; 
	}

	public long numberOfReviews() {
		return repository.numberOfReviewsFrom(project);
	}

	public long numberOfReviewsWithOkStatus() {
		return repository.numberOfReviewsWithOkStatusFrom(project);
	}

	public long numberOfReviewsWithCautionStatus() {
		return repository.numberOfReviewsWithCautionStatusFrom(project);
	}

	public long numberOfReviewsWithErrorStatus() {
		return repository.numberOfReviewsWithErrorStatusFrom(project);
	}

	public long numberOfFilesRevised() {
		return repository.numberOfFilesRevisedFrom(project);
	}

	public long numberOfFilesWithoutProblemsAndErrors() {
		return repository.numberOfFilesWithoutProblemsAndErrorsFrom(project);
	}

	public long numberOfFilesWithProblems() {
		return repository.numberOfFilesWithProblemsFrom(project);
	}

	public long numberOfFilesWithErrors() {
		return repository.numberOfFilesWithErrorsFrom(project);
	}

	public double percentageOfReviewsWithOkStatus() {
		double okStatus = numberOfReviewsWithOkStatus();
		return percentageOfReviewsWith(okStatus);
	}

	public double percentageOfReviewsWithCautionStatus() {
		double cautionStatus = numberOfReviewsWithCautionStatus();
		return percentageOfReviewsWith(cautionStatus);
	}

	public double percentageOfReviewsWithErrorStatus() {
		double errorStatus = numberOfReviewsWithErrorStatus();
		return percentageOfReviewsWith(errorStatus);
	}

	public double percentageOfFilesWithoutProblemsAndErrors() {
		double withoutProblemsAndErrors = numberOfFilesWithoutProblemsAndErrors();
		return percentageOfFiles(withoutProblemsAndErrors);
	}

	public double percentageOfFilesWithProblems() {
		double withProblems = numberOfFilesWithProblems();
		return percentageOfFiles(withProblems);
	}

	public double percentageOfFilesWithErrors() {
		double withErrors = numberOfFilesWithErrors();
		return percentageOfFiles(withErrors);
	}

	private void validate(Project project) {
		if (project == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("project"));
	}
	
	private double percentageOfReviewsWith(double status) {
		double numberOfReviews = numberOfReviews();
		return numberOfReviews == 0 ? 0 : status / numberOfReviews * 100.0;
	}

	private double percentageOfFiles(double status) {
		double numberOfFiles = numberOfFilesRevised();
		return numberOfFiles == 0 ? 0 : status / numberOfFiles * 100.0;
	}
}
