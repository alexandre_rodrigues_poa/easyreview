package org.easyreview.domain;

import org.easyreview.ExceptionsMessages;

public class RuleEmailCanNotBeInUseByAnotherUser {

	private Email email;
	private long userId;
	
	public RuleEmailCanNotBeInUseByAnotherUser(Email email, long userId) {
		validate(email);
			
		this.email = email;
		this.userId = userId;
	}

	public boolean inAccord() {
		boolean emailNoBeInUse = !email.inUse();
		boolean emailIsOfTheUser = email.isOfTheUserWith(userId);
		
		return emailNoBeInUse || emailIsOfTheUser;
	}

	public boolean notInAccord() {
		return !inAccord();
	}

	private void validate(Email email) {
		if (email == null) {
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("email"));
		}
	}
}
