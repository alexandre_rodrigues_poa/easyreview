package org.easyreview.domain;

public class GitException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public GitException(Exception e) {
		super(e);
	}

	public GitException(String message) {
		super(message);
	}
}
