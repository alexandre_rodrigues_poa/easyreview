package org.easyreview.domain;

import java.io.File;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

@Repository
public class ProjectRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public boolean nameInUse(String name) {
		TypedQuery<Boolean> query = createQueryNameInUse(name);
		return query.getSingleResult();
	}

	public boolean repositoryPathInUse(File repositoryPath) {
		TypedQuery<Boolean> query = createQueryRepositoryPathInUse(repositoryPath);
		return query.getSingleResult();
	}

	public Long findActiveProjectIdBy(String projectName) {
		TypedQuery<Long> query = createQueryFindProjectIdBy(projectName);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Long findActiveProjectIdBy(File repositoryPath) {
		TypedQuery<Long> query = createQueryFindProjectIdBy(repositoryPath);
		
		try {
			return query.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Project> listAllActiveProjects() {
		TypedQuery<Project> query = createQueryListAllActiveProjects();
		return query.getResultList();
	}

	public Project findProjectBy(Long id) {
		return entityManager.find(Project.class, id);
	}

	public File getRepositoryPathBy(Long projectId) {
		String jpql = "SELECT project.repositoryPath FROM Project project WHERE project.id = :id";
		TypedQuery<File> query = entityManager.createQuery(jpql, File.class);
		query.setParameter("id", projectId);
		
		return query.getSingleResult();
	}

	public List<Project> listAllActiveUserProjects(SystemUser user) {
		String jpql = "SELECT project FROM Project project WHERE project.removed = false and :user member of project.users";
		TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
		query.setParameter("user", user);
		
		return query.getResultList();
	}

	private TypedQuery<Boolean> createQueryNameInUse(String name) {
		String jpql = "SELECT (count(project) > 0) FROM Project project WHERE project.removed = false AND project.name = :name";
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		query.setParameter("name", name);
		return query;
	}

	private TypedQuery<Boolean> createQueryRepositoryPathInUse(File repositoryPath) {
		String jpql = "SELECT (count(project) > 0) FROM Project project WHERE project.removed = false AND project.repositoryPath = :repositoryPath";
		TypedQuery<Boolean> query = entityManager.createQuery(jpql, Boolean.class);
		query.setParameter("repositoryPath", repositoryPath);
		return query;
	}

	private TypedQuery<Long> createQueryFindProjectIdBy(String projectName) {
		String jpql = "SELECT project.id FROM Project project WHERE project.removed = false AND project.name = :name";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("name", projectName);
		return query;
	}

	private TypedQuery<Long> createQueryFindProjectIdBy(File repositoryPath) {
		String jpql = "SELECT project.id FROM Project project WHERE project.removed = false AND project.repositoryPath = :repositoryPath";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("repositoryPath", repositoryPath);
		return query;
	}

	private TypedQuery<Project> createQueryListAllActiveProjects() {
		String jpql = "SELECT project FROM Project project WHERE project.removed = false";
		TypedQuery<Project> query = entityManager.createQuery(jpql, Project.class);
		return query;
	}
}
