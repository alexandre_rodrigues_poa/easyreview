package org.easyreview.domain;

import javax.persistence.Embeddable;

import org.easyreview.ExceptionsMessages;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Embeddable
public class Password {
	private String hash;
	
	public static Password newInstance(String password) {
		validatePassword(password);

		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		Password result = new Password();
		result.hash = encoder.encode(password);
		return result;
	}

	public String getHash() {
		return hash;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + hash.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Password))
			return false;
		
		Password other = (Password) obj;
		return hash.equals(other.hash);
	}

	@Override
	public String toString() {
		return "Password [hash=" + hash + "]";
	}

	private static void validatePassword(String password) {
		if (password == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("password"));
		if (password.trim().isEmpty())
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeBlank("password"));
	}
}
