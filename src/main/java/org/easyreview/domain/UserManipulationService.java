package org.easyreview.domain;

import javax.transaction.Transactional;

import org.springframework.stereotype.Service;

@Service
public class UserManipulationService {
	
	@Transactional
	public SystemUser insertNewUser(String name, String emailAddress, String password, boolean isAdmin) {
		Email userEmail = Email.newInstance(emailAddress);
		Password userPassword = Password.newInstance(password);

		return SystemUser.newInstance(name, userEmail, userPassword, isAdmin);
	}

	@Transactional
	public SystemUser editUser(Long userId, String name, String emailAddress, boolean isAdmin) {
		SystemUser user = SystemUser.findBy(userId);
		if (user == null)
			throw new IllegalArgumentException("Can't found user with id: " + userId);
		
		user.setAdmin(isAdmin);
		user.setEmail(Email.newInstance(emailAddress));
		user.setName(name);
		
		return user;
	}

	@Transactional
	public SystemUser removeUser(Long userId) {
		SystemUser user = SystemUser.findBy(userId);
		if (user == null)
			throw new IllegalArgumentException("Can't found user with id: " + userId);
		
		user.remove();
		
		return user;
	}
}
