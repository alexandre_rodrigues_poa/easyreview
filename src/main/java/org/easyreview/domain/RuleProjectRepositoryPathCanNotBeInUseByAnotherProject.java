package org.easyreview.domain;

import java.io.File;

import org.easyreview.factorys.ContainerFactory;

public class RuleProjectRepositoryPathCanNotBeInUseByAnotherProject {

	private File repositoryPath;
	private long projectId;

	public RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(File repositoryPath, long projectId) {
		this.repositoryPath = repositoryPath;
		this.projectId = projectId;
	}

	public boolean inAccord() {
		return repositoryPathNotInUse() || repositoryPathInUserByInformedProject();
	}

	public boolean notInAccord() {
		return !inAccord();
	}

	private boolean repositoryPathNotInUse() {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		return !repository.repositoryPathInUse(repositoryPath);
	}

	private boolean repositoryPathInUserByInformedProject() {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		Long id = repository.findActiveProjectIdBy(repositoryPath);
		
		return id.equals(projectId);
	}
}
