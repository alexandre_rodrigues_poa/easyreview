package org.easyreview.domain;

import static org.easyreview.ExceptionsMessages.*;
import static org.easyreview.repositories.git.RepositoryFactory.createFrom;

import java.io.File;
import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.easyreview.repositories.CommittedFile;
import org.easyreview.repositories.git.GitCommitedFiles;
import org.easyreview.repositories.git.ModifiedFile;
import org.easyreview.repositories.git.ServiceGitCommit;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

public class GitCommit {

	private Project project;
	private RevCommit commit;
	private PersonIdent committer;

	GitCommit(Project project, RevCommit commit) {
		validate(project);
		validate(commit);
		this.commit = commit;
		this.project = project;
	}

	public String getId() {
		return commit.getName();
	}

	public Long getProjectId() {
		return project.getId();
	}

	public String getMessage() {
		return commit.getShortMessage();
	}

	public LocalDateTime getDateTime() {
		Instant instant = getInstantOfTheCommit();
		ZoneId zoneId = getTimeZoneOfTheCommit(); 
		
		ZonedDateTime zonedDateTime = instant.atZone(zoneId);
		LocalDateTime dateTime = zonedDateTime.toLocalDateTime();
		
		return dateTime;
	}

	public Email getCommitterEmail() {
		String emailAddress = committer().getEmailAddress();
		return Email.newInstance(emailAddress, false);
	}

	public String getCommitterName() {
		return committer().getName();
	}

	public List<CommittedFile> listFiles() {
		File repositoryPath = project.getRepositoryPath();
		
		try (Repository repository = createFrom(repositoryPath)) {
			return listFiles(repository);
		} catch (IOException | GitAPIException e) {
			throw new GitException(e);
		}
	}

	public int getNumberOfFiles() {
		List<CommittedFile> files = listFiles();
		return files.size();
	}

	public String readFile(String filePath) {
		File repositoryPath = project.getRepositoryPath();
		
		try (Repository repository = createFrom(repositoryPath)) {
			return readFile(commit, filePath, repository);
		} catch (IOException e) {
			throw new GitException(e);
		}
	}

	public ModifiedFile readModifiedFile(String filePath) {
		File repositoryPath = project.getRepositoryPath();
		
		try (Repository repository = createFrom(repositoryPath)) {
			return new ModifiedFile(repository, commit, filePath);
		} catch (IOException | GitAPIException e) {
			throw new GitException(e);
		}
	}

	public String readRemovedFile(String filePath) {
		File repositoryPath = project.getRepositoryPath();
		
		try (Repository repository = createFrom(repositoryPath)) {
			ServiceGitCommit service = new ServiceGitCommit(repository);
			
			ObjectId parentId = commit.getParent(0).getId();
			RevCommit parent = service.findCommitBy(parentId);

			return readFile(parent, filePath, repository);
		} catch (IOException | GitAPIException e) {
			throw new GitException(e);
		}
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + commit.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof GitCommit))
			return false;
		
		GitCommit other = (GitCommit) obj;
		return commit.equals(other.commit);
	}

	@Override
	public String toString() {
		return String.format("GitCommit [project=%s, commit=%s, committer=%s]", project, commit, committer);
	}

	private Instant getInstantOfTheCommit() {
		Date date = committer().getWhen();
		Instant instant = date.toInstant();
		return instant;
	}

	private ZoneId getTimeZoneOfTheCommit() {
		TimeZone timeZone = committer().getTimeZone();
		ZoneId zoneId = timeZone != null ? timeZone.toZoneId() : ZoneId.systemDefault();
		return zoneId;
	}

	private PersonIdent committer() {
		if (committer == null)
			committer = commit.getCommitterIdent();
		return committer;
	}

	private void validate(RevCommit commit) {
		if (commit == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("commit"));
	}

	private void validate(Project project) {
		if (project == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("project"));
	}

	private List<CommittedFile> listFiles(Repository repository) throws IOException, GitAPIException {
		GitCommitedFiles commitedFiles = new GitCommitedFiles(repository, commit);
		return commitedFiles.list();
	}

	private String readFile(RevCommit commit, String filePath, Repository repository) throws IOException {
		ServiceGitCommit service = new ServiceGitCommit(repository);
		return service.readFileFromCommit(commit, filePath);
	}
}
