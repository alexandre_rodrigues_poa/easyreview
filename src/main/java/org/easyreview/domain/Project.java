package org.easyreview.domain;

import static javax.persistence.GenerationType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

import javax.persistence.*;

import static org.easyreview.ExceptionsMessages.*;
import static java.util.Comparator.comparing;

import org.apache.commons.lang3.StringUtils;
import org.easyreview.ExceptionsMessages;
import org.easyreview.factorys.ContainerFactory;
import org.easyreview.persistence.FileConverter;
import org.easyreview.persistence.LocalDateConverter;

@Entity
public class Project {

	@Version
	private int version;
	
	@Id @GeneratedValue(strategy=AUTO)
	private Long id = 0L;
	
	private String name;
	
	@ManyToMany @JoinTable
	private Set<SystemUser> users;
	
	@Column(length=2048) @Convert(converter=FileConverter.class)
	private File repositoryPath;
	
	private boolean removed;

	@Convert(converter=LocalDateConverter.class)
	private LocalDate commitsFromDate;

	private Integer newCommits;

	@Enumerated(EnumType.STRING)
	private RepositoryType repositoryType;

	public static Project newInstance(String name, File repositoryPath, RepositoryType repositoryType, Set<SystemUser> users, LocalDate commitsFromDate) {
		Project newProject = new Project();
		newProject.setName(name);
		newProject.setRepositoryPath(repositoryPath);
		newProject.setRepositoryType(repositoryType);
		newProject.setUsers(users);
		newProject.setCommitsFromDate(commitsFromDate);
		newProject.persist();
		return newProject;
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		validateState();
		validate(name);
		this.name = name;
	}

	public File getRepositoryPath() {
		return repositoryPath;
	}

	public void setRepositoryPath(File repositoryPath) {
		validateState();
		validate(repositoryPath);
		this.repositoryPath = repositoryPath;
	}

	public RepositoryType getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryType(RepositoryType repositoryType) {
		validateState();
		validate(repositoryType);
		this.repositoryType = repositoryType;
	}

	public Set<SystemUser> getUsers() {
		return Collections.unmodifiableSet(users);
	}

	public void setUsers(Set<SystemUser> users) {
		validateState();
		validate(users);
		this.users = new HashSet<>(users); 
	}

	public LocalDate getCommitsFromDate() {
		return commitsFromDate;
	}

	public void setCommitsFromDate(LocalDate commitsFromDate) {
		validate(commitsFromDate);
		this.commitsFromDate = commitsFromDate;
	}

	public GitRepository getGitRepository() {
		return new GitRepository(this);
	}

	public boolean isProjectMembership(SystemUser user) {
		return users.contains(user);
	}

	public int getNumberOfCommits() {
		return getGitRepository().getNumberOfCommits();
	}

	public int getNewCommits() {
		return newCommits != null ? newCommits : 0;
	}

	public void setNewCommits(int commits) {
		if(commits <= 0)
			throw new IllegalArgumentException(theNumberOfNewCommitsCanNotLessThanOrEqualToZero());
		newCommits = commits;
	}

	public void remove() {
		removed = true;
	}

	public static List<Project> listAllOrderedByName() {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		List<Project> projects = repository.listAllActiveProjects();
		projects.sort(comparing(Project::getName));
		return projects;
	}

	public static List<Project> listUser(SystemUser user) {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		return repository.listAllActiveUserProjects(user);
	}

	public static Project findBy(Long id) {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		return repository.findProjectBy(id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Project))
			return false;

		Project other = (Project) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return String.format("Project [id=%s, version=%s, name=%s, users=%s, repositoryPath=%s, removed=%s]", id, version, name, users, repositoryPath, removed);
	}

	private void persist() {
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		entityManager.persist(this);
	}

	private void validate(String name) {
		if (StringUtils.isBlank(name))
			throw new IllegalArgumentException(theParameterCanNotBeBlank("name"));
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = new RuleProjectNameCanNotBeInUseByAnotherProject(name, id);
		
		if (rule.notInAccord())
			throw new IllegalArgumentException(theNameIsInUseByAnotherProject(name));
	}

	private void validate(File repositoryPath) {
		if (repositoryPath == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("repositoryPath"));
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = new RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(repositoryPath, id);

		if (rule.notInAccord()) {
			throw new IllegalArgumentException(theRepositoryPathIsInUseByAnotherProject(repositoryPath));
		}
	}

	private void validate(RepositoryType repositoryType) {
		if (repositoryType == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("type"));
	}

	private void validate(Set<SystemUser> users) {
		for (SystemUser user : users) {
			if (user == null)
				throw new IllegalArgumentException(usersParameterCanNotContainANullUser());
			if (user.removed())
				throw new IllegalArgumentException(usersParameterCanNotContainANullUser());
		}
	}
	
	private void validateState() {
		if (removed)
			throw new IllegalStateException(ExceptionsMessages.canNotEditARemovedProject());
	}

	private void validate(LocalDate commitsFromDate) {
		if (commitsFromDate == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("commitsFromDate"));
	}
}
