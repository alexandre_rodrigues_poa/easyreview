package org.easyreview.domain;


import static javax.persistence.GenerationType.*;
import static java.util.Comparator.comparing;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.*;

import org.easyreview.ExceptionsMessages;
import org.easyreview.factorys.ContainerFactory;

@Entity
public class SystemUser {

	@Version
	private int version;
	
	@Id @GeneratedValue(strategy=AUTO)
	private Long id = 0L;
	
	@Embedded
	private Password password;
	
	@Embedded
	private Email email;
	
	private String userName;
	private boolean isAdmin;
	private boolean removed;
	
	@ManyToMany(mappedBy="users")
	private Set<Project> projects = new HashSet<>();

	public static SystemUser newInstance(String name, Email email, Password password, boolean isAdmin) {
		SystemUser user = new SystemUser();
		
		user.setName(name);
		user.setEmail(email);
		user.setPassword(password);
		user.setAdmin(isAdmin);
		
		insert(user);
		
		return user;
	}
	
	public static List<SystemUser> listAllActiveOrderedByName() {
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		List<SystemUser> users = repository.listAllActive();
		users.sort(comparing(user -> user.getName()));
		return users;
	}

	public static SystemUser findBy(Long userId) {
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		return repository.findUserBy(userId);
	}
	
	public Long getId() {
		return id;
	}

	public String getName() {
		return userName;
	}

	public void setName(String name) {
		validateState();
		validate(name);
		this.userName = name;
	}

	public Email getEmail() {
		return email;
	}

	public void setEmail(Email email) {
		validateState();
		validate(email);		
		this.email = email;
	}

	public Password getPassword() {
		return password;
	}

	public void setPassword(Password password) {
		validateState();
		validate(password);
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}
	
	public void setAdmin(boolean isAdmin) {
		validateState();
		this.isAdmin = isAdmin;
	}

	public String getEmailAddress() {
		return email.getAddress();
	}

	public String getPasswordHash() {
		return password.getHash();
	}

	public Set<Project> getProjects() {
		return Collections.unmodifiableSet(projects);
	}

	public void remove() {
		removed = true;
		projects.clear();
	}

	public boolean removed() {
		return removed;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof SystemUser))
			return false;
		
		SystemUser other = (SystemUser) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return String.format("SystemUser [id=%s, password=%s, email=%s, userName=%s, isAdmin=%s]", id, password, email, userName, isAdmin);
	}

	private static void insert(SystemUser user) {
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		entityManager.persist(user);
	}

	private void validate(String userName) {
		if (userName == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("userName"));
		if (userName.trim().isEmpty())
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeBlank("userName"));
	}

	private void validate(Email email) {
		if (email == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("email"));
		
		RuleEmailCanNotBeInUseByAnotherUser rule = new RuleEmailCanNotBeInUseByAnotherUser(email, id);
		
		if (rule.notInAccord())
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeInUse("email"));
	}

	private void validate(Password password) {
		if (password == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("password"));
	}
	
	private void validateState() {
		if (removed)
			throw new IllegalStateException(ExceptionsMessages.canNotEditARemovedUser());
	}
}
