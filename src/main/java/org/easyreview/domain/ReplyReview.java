package org.easyreview.domain;

import static javax.persistence.GenerationType.AUTO;
import static org.easyreview.ExceptionsMessages.*;

import java.time.LocalDateTime;

import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.OneToOne;
import javax.persistence.Version;

import org.apache.commons.lang3.StringUtils;
import org.easyreview.factorys.ContainerFactory;
import org.easyreview.persistence.LocalDateTimeConverter;

@Entity
public class ReplyReview {

	@Id @GeneratedValue(strategy=AUTO)
	private Long id = 0L;
	@Version
	private Integer version;
	
	@OneToOne @JoinColumn
	private SystemUser author;
	@Lob
	private String text;
	
	@Convert(converter=LocalDateTimeConverter.class)
	private LocalDateTime dateTime;
	@Convert(converter=LocalDateTimeConverter.class)
	private LocalDateTime lastEdition;

	public Long getId() {
		return id;
	}

	public SystemUser getAuthor() {
		return author;
	}

	public String getText() {
		return text;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public LocalDateTime getLastEdition() {
		return lastEdition;
	}

	public void setText(String text, SystemUser author) {
		validate(text);
		validateIsTheOriginalAuthor(author);
		
		if (notIsFirstAccessToSetText())
			lastEdition = LocalDateTime.now();
		
		this.text = text;
	}

	public boolean edited() {
		return lastEdition != null;
	}

	public static ReplyReview newInstance(SystemUser author, String text) {
		validate(author);
		ReplyReview review = createReply(author, text);
		return review;
	}

	public static ReplyReview findBy(Long id) {
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		return entityManager.find(ReplyReview.class, id);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ReplyReview))
			return false;
		
		ReplyReview other = (ReplyReview) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return String.format("ReplyReview [id=%s, version=%s, author=%s, text=%s, dateTime=%s, lastEdition=%s]", id, version, author, text, dateTime, lastEdition);
	}

	private static void validate(SystemUser author) {
		if (author == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("author"));
	}

	private static ReplyReview createReply(SystemUser author, String text) {
		ReplyReview review = new ReplyReview();
		review.author = author;
		review.setText(text, author);
		review.dateTime = LocalDateTime.now();
		return review;
	}

	private void validate(String text) {
		if (StringUtils.isBlank(text))
			throw new IllegalArgumentException(theParameterCanNotBeNull("text"));
	}

	private void validateIsTheOriginalAuthor(SystemUser author) {
		if (!this.author.equals(author))
			throw new IllegalArgumentException(onlyTheOriginalAuthorCanModifyTheReply(this.author, author));
	}

	private boolean notIsFirstAccessToSetText() {
		return this.text != null;
	}
}
