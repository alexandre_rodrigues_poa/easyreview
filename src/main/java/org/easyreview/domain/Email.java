package org.easyreview.domain;

import javax.persistence.Embeddable;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.EmailValidator;
import org.easyreview.ExceptionsMessages;
import org.easyreview.factorys.ContainerFactory;

@Embeddable
public class Email {

	private String emailAddress;
	
	public static Email newInstance(String emailAddress) {
		return newInstance(emailAddress, true);
	}

	public static Email newInstance(String emailAddress, boolean validateAddress) {
		if (validateAddress) 
			validateAddres(emailAddress);
		else
			validateIsBlank(emailAddress);
		
		Email email = new Email();
		email.emailAddress = emailAddress;
		return email;
	}

	public String getAddress() {
		return emailAddress;
	}

	public boolean inUse() {
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		return repository.emailInUse(this);
	}

	public boolean isOfTheUserWith(Long id) {
		if (id == null)
			return false;
		
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		Long idFound = repository.findIdUserBy(this);
		
		return id.equals(idFound);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + emailAddress.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Email))
			return false;
		
		Email other = (Email) obj;
		return emailAddress.equals(other.emailAddress);
	}

	@Override
	public String toString() {
		return emailAddress;
	}

	private static void validateIsBlank(String emailAddress) {
		if (StringUtils.isBlank(emailAddress))
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeBlank(emailAddress));
	}

	private static void validateAddres(String emailAddress) {
		EmailValidator emailValidator = EmailValidator.getInstance();
		if (!emailValidator.isValid(emailAddress))
			throw new IllegalArgumentException(ExceptionsMessages.emailAddressIsInvalid(emailAddress));
	}
}
