package org.easyreview.domain;

import java.io.File;

import javax.transaction.Transactional;

import org.easyreview.repositories.git.GitUpdateCommand;
import org.easyreview.repositories.git.GitUpdateCommandBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ProjectUpdateSevice {
	
	private Logger looger = LoggerFactory.getLogger(ProjectUpdateSevice.class);

	@Transactional
	public void update(Project project) {
		final int originalNumberOfCommits = project.getNumberOfCommits();
		
		try {
			GitUpdateCommand command = getCommand(project);
			String resultMessage = command.execute();
			updateNewCommits(project, originalNumberOfCommits);
			looger.debug("UPDATE PROJECT - 'git svn fetch' command executed with sucess:", resultMessage);
		} catch (Exception exception) {
			looger.error("UPDATE PROJECT ERROR:", exception);
		}

	}
	
	private GitUpdateCommand getCommand(Project project) {
		File repositoryPath = project.getRepositoryPath();
		RepositoryType repositoryType = project.getRepositoryType();
		GitUpdateCommandBuilder builder = repositoryType.getGitUpdateCommandBuilder();
		return builder.build(repositoryPath);
	}

	private void updateNewCommits(Project project, final int originalNumberOfCommits) {
		final int actualNumberDeCommits = project.getNumberOfCommits();
		int newCommits = actualNumberDeCommits - originalNumberOfCommits;
		if (newCommits > 0)
			project.setNewCommits(newCommits);
		else
			project.setNewCommits(0);
	}
}
