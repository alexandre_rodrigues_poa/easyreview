package org.easyreview.domain;

import javax.persistence.*;

import org.springframework.stereotype.Repository;

@Repository
public class ReviewsMetricsRepository {

	@PersistenceContext
	private EntityManager entityManager;

	public long numberOfReviewsFrom(Project project) {
		TypedQuery<Long> query = createQueryCountNumberOfReviewsFrom(project);
		return query.getSingleResult();
	}

	public long numberOfReviewsWithOkStatusFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfReviewsWithStatusFrom(project, CodeAnalysisStatus.OK);
		return query.getSingleResult();
	}

	public long numberOfReviewsWithCautionStatusFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfReviewsWithStatusFrom(project, CodeAnalysisStatus.CAUTION);
		return query.getSingleResult();
	}

	public long numberOfReviewsWithErrorStatusFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfReviewsWithStatusFrom(project, CodeAnalysisStatus.ERROR);
		return query.getSingleResult();
	}

	public long numberOfFilesRevisedFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfFilesRevisedFrom(project);
		return query.getSingleResult();
	}

	public long numberOfFilesWithoutProblemsAndErrorsFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfFilesWithoutProblemsAndErrorsFrom(project);
		return query.getSingleResult();
	}

	public long numberOfFilesWithProblemsFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfFilesWithStatusFrom(project, CodeAnalysisStatus.CAUTION);
		return query.getSingleResult();
	}

	public long numberOfFilesWithErrorsFrom(Project project) {
		TypedQuery<Long> query = createQueryNumberOfFilesWithStatusFrom(project, CodeAnalysisStatus.ERROR);
		return query.getSingleResult();
	}

	private TypedQuery<Long> createQueryCountNumberOfReviewsFrom(Project project) {
		String jpql = "SELECT COUNT (review) FROM Review review WHERE review.project = :project";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("project", project);
		return query;
	}

	private TypedQuery<Long> createQueryNumberOfReviewsWithStatusFrom(Project project, CodeAnalysisStatus status) {
		String jpql = "SELECT COUNT(review) FROM Review review WHERE review.project = :project and review.analysisStatus = :status";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("project", project);
		query.setParameter("status", status);
		return query;
	}

	private TypedQuery<Long> createQueryNumberOfFilesRevisedFrom(Project project) {
		String jpql = "SELECT COUNT(DISTINCT review.file) FROM Review review WHERE review.project = :project";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("project", project);
		return query;
	}

	private TypedQuery<Long> createQueryNumberOfFilesWithoutProblemsAndErrorsFrom(Project project) {
		String jpql = "SELECT " +
					  	"COUNT(DISTINCT review.file) " + 
					  "FROM " + 
					  	"Review review " + 
					  "WHERE " + 
					  	"review.project = :project " + 
					    "AND review.analysisStatus = org.easyreview.domain.CodeAnalysisStatus.OK " + 
					  	"AND review.file not in(SELECT " + 
						  	                 	  "DISTINCT review.file " + 
						  	                 	"FROM " +
						  	                 	  "Review review " +
						  	                 	"WHERE " +
						  	                 	  "review.project = :project " +
						  	                 	  "AND (review.analysisStatus = org.easyreview.domain.CodeAnalysisStatus.CAUTION " +
						  	                 	        "OR review.analysisStatus = org.easyreview.domain.CodeAnalysisStatus.ERROR))";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("project", project);
		return query;
	}

	private TypedQuery<Long> createQueryNumberOfFilesWithStatusFrom(Project project, CodeAnalysisStatus status) {
		String jpql = "SELECT COUNT(DISTINCT review.file) FROM Review review WHERE review.project = :project and review.analysisStatus = :status";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("project", project);
		query.setParameter("status", status);
		return query;
	}
}
