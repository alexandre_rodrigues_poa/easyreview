package org.easyreview.domain;

import static javax.persistence.GenerationType.AUTO;
import static javax.persistence.CascadeType.ALL;
import static org.easyreview.ExceptionsMessages.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.easyreview.factorys.ContainerFactory;
import org.easyreview.persistence.FileConverter;
import org.easyreview.persistence.LocalDateTimeConverter;
import org.hibernate.validator.constraints.NotBlank;

@Entity
public class Review {

	@Id @GeneratedValue(strategy=AUTO)
	private Long id = 0L;
	@Version
	private Integer version;
	@OneToOne @JoinColumn
	private SystemUser author;
	@OneToOne @JoinColumn
	private Project project;
	private String commitId;
	@Column(length=2048) @Convert(converter=FileConverter.class)
	private File file;
	@Convert(converter=LocalDateTimeConverter.class)
	private LocalDateTime dateTime;
	@Convert(converter=LocalDateTimeConverter.class)
	private LocalDateTime lastEdition;
	@OneToMany(cascade=ALL) @JoinColumn
	private List<ReplyReview> replies = new ArrayList<>();

	@NotNull
	private CodeAnalysisStatus analysisStatus;

	@Lob
	@NotBlank
	private String comment;

	@Transient
	private String pageUrl;

	public Long getId() {
		return id;
	}

	public SystemUser getAuthor() {
		return author;
	}

	public Project getProject() {
		return project;
	}

	public String getCommitId() {
		return commitId;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public File getFile() {
		return file;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment, SystemUser author) {
		validate(comment);
		validateIsTheOriginalAuthor(author);

		this.comment = comment;
		
		reviewWasEdited();
	}

	public CodeAnalysisStatus getAnalysisStatus() {
		return analysisStatus;
	}

	public void setAnalysisStatus(CodeAnalysisStatus analysisStatus, SystemUser author) {
		validate(analysisStatus);
		validateIsTheOriginalAuthor(author);

		this.analysisStatus = analysisStatus;
		
		reviewWasEdited();
	}

	private void reviewWasEdited() {
		if (this.id != 0L) 
			lastEdition = LocalDateTime.now();
	}

	public boolean edited() {
		return lastEdition != null;
	}

	public LocalDateTime lastEdition() {
		return lastEdition;
	}

	public List<ReplyReview> listReplies() {
		return Collections.unmodifiableList(replies);
	}

	public void addIfNotExist(ReplyReview reply) {
		if (!replies.contains(reply))
			replies.add(reply);		
	}

	public static Review newInstance(SystemUser author, Project project, GitCommit commit, File file, String comment, CodeAnalysisStatus analysisStatus) {
		validateParameters(author, project, commit, file);
		Review review = createReview(author, project, commit, file, comment, analysisStatus);
		
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		entityManager.persist(review);
		
		return review;
	}

	public static Review findBy(Long id) {
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		Review review = entityManager.find(Review.class, id); 
		return review;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof Review))
			return false;
		
		Review other = (Review) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return String.format("FileReview [id=%s, version=%s, author=%s, project=%s, commitId=%s, file=%s, dateTime=%s, comment=%s, lastEdition=%s, replies=%s, analysisStatus=%s]",
				id, version, author, project, commitId, file, dateTime, comment, lastEdition, replies, analysisStatus);
	}

	private static void validateParameters(SystemUser author, Project project, GitCommit commit, File file) {
		if (project == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("project"));
		if (theAuthorOfTheCommitIsTheAuthorOfTheReview(author, commit)) {
			String emailAddress = author.getEmail().getAddress();
			throw new IllegalArgumentException(theAuthorOfTheCommitCanNotBeTheAuthorOfTheReview(emailAddress));
		}
		if (file == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("file"));
	}

	private static boolean theAuthorOfTheCommitIsTheAuthorOfTheReview(SystemUser author, GitCommit commit) {
		return author.getEmail().equals(commit.getCommitterEmail());
	}

	private static Review createReview(SystemUser author, Project project, GitCommit commit, File file, String comment, CodeAnalysisStatus analysisStatus) {
		Review review = new Review();
		review.author = author;
		review.project = project;
		review.commitId = commit.getId();
		review.file = file;
		review.dateTime = LocalDateTime .now();
		review.setComment(comment, author);
		review.setAnalysisStatus(analysisStatus, author);
		return review;
	}

	private void validate(String text) {
		if (StringUtils.isBlank(text))
			throw new IllegalArgumentException(theParameterCanNotBeBlank("text"));
	}

	private void validate(CodeAnalysisStatus newAnalysisStatus) {
		if (newAnalysisStatus == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("analysisStatus"));
	}

	private void validateIsTheOriginalAuthor(SystemUser author) {
		if (!this.author.equals(author))
			throw new IllegalArgumentException(onlyTheOriginalAuthorCanModifyTheReview(this.author, author));
	}
}