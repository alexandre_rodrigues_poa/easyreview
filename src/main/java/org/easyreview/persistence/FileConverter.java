package org.easyreview.persistence;

import java.io.File;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class FileConverter implements AttributeConverter<File, String> {

	@Override
	public String convertToDatabaseColumn(File file) {
		return file != null ? file.getPath() : null;
	}

	@Override
	public File convertToEntityAttribute(String path) {
		return path != null ? new File(path) : null;
	}
}
