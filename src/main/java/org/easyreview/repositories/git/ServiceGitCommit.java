package org.easyreview.repositories.git;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easyreview.ExceptionsMessages;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;

public class ServiceGitCommit {

	private Repository gitRepository;

	public ServiceGitCommit(Repository gitRepository) {
		if(gitRepository == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("repository"));
		this.gitRepository = gitRepository;
	}

	public List<RevCommit> listCommitsFrom(Date date) throws NoHeadException, GitAPIException, IOException {
		List<RevCommit> listCommit = new ArrayList<>();
		Git git = new Git(gitRepository);
		
		try {
			LogCommand log = git.log().all();
			Iterable<RevCommit> commits = log.call();
			for(RevCommit commit : commits) {
				if (commitDateIsIsGreaterThanOrEqual(commit, date))
					listCommit.add(commit);
				else
					break;
			}
		} finally {
			git.close();
		}
		return listCommit;
	}

	public int countNumberOfCommitsFrom(Date date) throws NoHeadException, GitAPIException, IOException{
		int count = 0;
		Git git = new Git(gitRepository);
		try {
			LogCommand log = git.log().all();
			Iterable<RevCommit> commits = log.call();
			for(RevCommit commit : commits) {
				if (commitDateIsIsGreaterThanOrEqual(commit, date))
					count++;
				else
					break;
			}
				
		} finally {
			git.close();
		}
		return count;
	}

	public RevCommit findCommitBy(String commitId) throws NoHeadException, GitAPIException, IOException {
		RevWalk revWalk = new RevWalk(gitRepository);
		
		try {
			ObjectId id = gitRepository.resolve(commitId);
			return revWalk.parseCommit(id);
		} finally {
			revWalk.close();
		}
	}
	
	public RevCommit findCommitBy(ObjectId commitId) throws NoHeadException, GitAPIException, IOException {
		RevWalk revWalk = new RevWalk(gitRepository);
		
		try {
			return revWalk.parseCommit(commitId);
		} finally {
			revWalk.close();
		}
	}

	public String readFileFromCommit(RevCommit commit, String filePath) throws IOException {
		try (TreeWalk treeWalk = new TreeWalk(gitRepository)) {
			treeWalk.addTree(commit.getTree());
			treeWalk.setRecursive(true);
			treeWalk.setFilter(PathFilter.create(filePath));
			
			if (!treeWalk.next()) {
				String message = String.format("File not found: ", filePath);
				throw new IllegalArgumentException( message);
			}
			ObjectId fileId = treeWalk.getObjectId(0); 
			return readFile(fileId);
		}	
	}

	private String readFile(ObjectId fileId) throws MissingObjectException, IOException {
		ObjectLoader loader = gitRepository.open(fileId);
		ByteArrayOutputStream file = new ByteArrayOutputStream(); 
		loader.copyTo(file);
		
		return file.toString();
	}
	
	private boolean commitDateIsIsGreaterThanOrEqual(RevCommit commit, Date date) {
		PersonIdent committer = commit.getAuthorIdent();
		return committer.getWhen().compareTo(date) >= 0;
	}
}
