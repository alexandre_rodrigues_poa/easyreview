package org.easyreview.repositories.git;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.easyreview.ExceptionsMessages;
import org.easyreview.domain.GitException;

public abstract class GitUpdateCommand {
	private File dir;
	
	public GitUpdateCommand(File dir) {
		if (dir == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeNull("dir"));
		this.dir = dir;
	}

	public String execute() throws IOException, InterruptedException {
		Runtime run = Runtime.getRuntime();
		Process process = run.exec(getCommand(), null, dir);
		process.waitFor(10, TimeUnit.MINUTES);

		if (executedWithSucess(process))
			return getSucessMessageFrom(process);
		
		String message = getErrorMessageFrom(process);
		throw new GitException(message);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + dir.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;

		GitUpdateCommand other = (GitUpdateCommand) obj;
		return dir.equals(other.dir);
	}

	@Override
	public String toString() {
		return String.format("%s [dir= %s]", getClass().getSimpleName(), dir);
	}

	protected abstract String getCommand();

	private String getSucessMessageFrom(Process process) {
		InputStream stream = process.getInputStream();
		return convertInputStreamToString(stream);
	}

	private String getErrorMessageFrom(Process process) {
		InputStream stream = process.getErrorStream();
		String message = convertInputStreamToString(stream);
		return message;
	}

	private boolean executedWithSucess(Process process) {
		return process.exitValue() == 0;
	}
	
	private String convertInputStreamToString(InputStream stream) {
		
		StringBuilder message = new StringBuilder();
		try (Scanner scanner = new Scanner(stream)) {
			while(scanner.hasNext())
				message.append(scanner.nextLine());
				message.append("\n");
		}
		return message.toString();
	}
}

	