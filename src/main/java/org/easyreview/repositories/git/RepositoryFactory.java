package org.easyreview.repositories.git;

import java.io.File;
import java.io.IOException;

import org.easyreview.domain.GitException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

public class RepositoryFactory {
	
	private RepositoryFactory() {}
	
	public static Repository createFrom(File directory) {
		try {
			FileRepositoryBuilder builder = new FileRepositoryBuilder();
			return builder.findGitDir(directory).build();
		} catch (IOException e) {
			throw new GitException(e);
		}
	}
}