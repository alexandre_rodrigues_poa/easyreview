package org.easyreview.repositories.git;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;

public class ModifiedFile {

	private String currentFile;
	private String previousFile;

	public ModifiedFile(Repository repository, RevCommit commit, String filePath) throws IOException, GitAPIException {
		ServiceGitCommit service = new ServiceGitCommit(repository);
		readCurrentFile(commit, filePath, service);
		readPreviousFile(commit, filePath, service);
	}

	public String getCurrentFile() {
		return currentFile;
	}

	public String getPreviousFile() {
		return previousFile;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + currentFile.hashCode();
		result = prime * result + previousFile.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ModifiedFile))
			return false;
		
		ModifiedFile other = (ModifiedFile) obj;
		
		return currentFile.equals(other.currentFile) && previousFile.equals(other.previousFile);
	}

	private void readCurrentFile(RevCommit commit, String filePath, ServiceGitCommit service) throws IOException {
		currentFile = service.readFileFromCommit(commit, filePath);
	}

	private void readPreviousFile(RevCommit commit, String filePath, ServiceGitCommit service) throws NoHeadException, GitAPIException, IOException {
		ObjectId parentId = commit.getParent(0).getId();
		RevCommit parent = service.findCommitBy(parentId);
		
		previousFile = service.readFileFromCommit(parent, filePath);
	}
}
