package org.easyreview.repositories.git;

import java.io.File;

public class GitPullCommand extends GitUpdateCommand {

	public GitPullCommand(File dir) {
		super(dir);
	}

	@Override
	protected String getCommand() {
		return "git pull";
	}
}

	