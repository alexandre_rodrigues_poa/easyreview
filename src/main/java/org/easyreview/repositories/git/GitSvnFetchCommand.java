package org.easyreview.repositories.git;

import java.io.File;

public class GitSvnFetchCommand extends GitUpdateCommand {

	public GitSvnFetchCommand(File dir) {
		super(dir);
	}

	@Override
	protected String getCommand() {
		return "git svn fetch";
	}
}

	