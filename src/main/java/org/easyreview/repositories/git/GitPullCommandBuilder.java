package org.easyreview.repositories.git;

import java.io.File;

public class GitPullCommandBuilder implements GitUpdateCommandBuilder {

	@Override
	public GitUpdateCommand build(File dir) {
		return new GitPullCommand(dir);
	}

}
