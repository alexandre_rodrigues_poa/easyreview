package org.easyreview.repositories.git;

import java.io.IOException;
import java.util.*;

import org.easyreview.repositories.CommittedFile;
import org.easyreview.repositories.CommittedFileStatus;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.MissingObjectException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.util.io.DisabledOutputStream;

public class GitCommitedFiles {

	private List<CommittedFile> committedFiles = new ArrayList<>();

	public GitCommitedFiles(Repository repository, RevCommit commit) throws IOException, GitAPIException {
		DiffFormatter formatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
		try {
			listFiles(repository, commit, formatter);
		} finally {
			formatter.close();
		}
	}

	public List<CommittedFile> list() {
		return Collections.unmodifiableList(committedFiles);
	}

	private void listFiles(Repository repository, RevCommit commit, DiffFormatter formatter) throws IOException, GitAPIException {
		if (commit.getParentCount() == 0)
			listFilesFromCommit(repository, commit);
		else
			listFilesBetweenCommitAndPreviousCommit(repository, commit, formatter);
	}

	private void listFilesFromCommit(Repository repository, RevCommit commit) throws MissingObjectException, IncorrectObjectTypeException, CorruptObjectException, IOException {
		RevTree tree = commit.getTree();
		TreeWalk treeWalk = new TreeWalk(repository);
		try {
			treeWalk.addTree(tree);
			treeWalk.setRecursive(false);
			populateList(treeWalk);
		} finally {
			treeWalk.close();
		}
	}

	private void populateList(TreeWalk treeWalk) throws IOException {
		while (treeWalk.next()) {
		    if (treeWalk.isSubtree())
		        treeWalk.enterSubtree();
		    else
		    	committedFiles.add(new CommittedFile(treeWalk.getPathString(), CommittedFileStatus.ADDED));
		}
	}

	private void listFilesBetweenCommitAndPreviousCommit(Repository repository, RevCommit commit, DiffFormatter formatter) throws GitAPIException, IOException {
		ServiceGitCommit service = new ServiceGitCommit(repository);
		RevCommit parent = service.findCommitBy(commit.getParent(0));

		formatter.setRepository(repository);
		formatter.setDiffComparator(RawTextComparator.DEFAULT);
		formatter.setDetectRenames(true);
		
		List<DiffEntry> diffs = formatter.scan(parent.getTree(), commit.getTree());
		populateList(diffs);
	}

	@SuppressWarnings("incomplete-switch")
	private void populateList(List<DiffEntry> diffs) throws IOException {
		for (DiffEntry diff : diffs) {
			switch (diff.getChangeType()) {
			case ADD :
				committedFiles.add(new CommittedFile(diff.getNewPath(), CommittedFileStatus.ADDED));
				break;
			case MODIFY :
				committedFiles.add(new CommittedFile(diff.getNewPath(), CommittedFileStatus.CHANGED));
				break;
			case DELETE:
				committedFiles.add(new CommittedFile(diff.getOldPath(), CommittedFileStatus.REMOVED));
				break;
			}
		}
	}
}
