package org.easyreview.repositories.git;

import java.io.File;

public interface GitUpdateCommandBuilder {
	GitUpdateCommand build(File dir);
}
