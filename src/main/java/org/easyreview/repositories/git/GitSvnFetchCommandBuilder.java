package org.easyreview.repositories.git;

import java.io.File;

public class GitSvnFetchCommandBuilder implements GitUpdateCommandBuilder {

	@Override
	public GitUpdateCommand build(File dir) {
		return new GitSvnFetchCommand(dir);
	}

}
