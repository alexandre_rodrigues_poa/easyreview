package org.easyreview.repositories;

public enum CommittedFileStatus {
	CHANGED, ADDED, REMOVED;
}
