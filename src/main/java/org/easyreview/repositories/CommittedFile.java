package org.easyreview.repositories;

import org.apache.commons.lang3.StringUtils;
import org.easyreview.ExceptionsMessages;

public class CommittedFile {

	private String name;
	private CommittedFileStatus status;

	public CommittedFile(String name, CommittedFileStatus status) {
		validate(name);
		validate(status);

		this.name = name;
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public CommittedFileStatus getStatus() {
		return status;
	}

	private void validate(CommittedFileStatus status) {
		if (status == null)
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeBlank("status"));
	}

	private void validate(String name) {
		if (StringUtils.isBlank(name))
			throw new IllegalArgumentException(ExceptionsMessages.theParameterCanNotBeBlank("name"));
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + name.hashCode();
		result = prime * result + status.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof CommittedFile))
			return false;
		
		CommittedFile other = (CommittedFile) obj;
		return name.equals(other.name) && status.equals(other.status);
	}

	@Override
	public String toString() {
		return String.format("CommittedFile [name=%s, status=%s]", name, status);
	}
}
