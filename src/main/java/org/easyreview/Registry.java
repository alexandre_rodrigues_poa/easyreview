package org.easyreview;

import java.text.Format;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;

public class Registry {
	private static FormatStyle style = FormatStyle.MEDIUM;
	private static Registry instance = new Registry();

	private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(style);
	
	public static DateTimeFormatter getDateTimeFormatter() {
		return getInstance().dateTimeFormatter;
	}
	public static String getDateFormatString() {
		Format format = getInstance().dateTimeFormatter.toFormat();
		return format.toString();
	}

	private static Registry getInstance() {
		return instance;
	}
}
