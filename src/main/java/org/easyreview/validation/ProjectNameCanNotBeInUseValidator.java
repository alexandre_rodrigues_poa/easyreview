package org.easyreview.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.domain.ProjectRepository;
import org.easyreview.factorys.ContainerFactory;

public class ProjectNameCanNotBeInUseValidator implements ConstraintValidator<ProjectNameCanNotBeInUse, String> {

	@Override
	public void initialize(ProjectNameCanNotBeInUse constraintAnnotation) {
	}

	@Override
	public boolean isValid(String name, ConstraintValidatorContext context) {
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		return !repository.nameInUse(name);
	}
}
