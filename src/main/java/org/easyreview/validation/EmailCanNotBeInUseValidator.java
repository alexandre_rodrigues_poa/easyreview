package org.easyreview.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.domain.Email;
import org.easyreview.domain.SystemUserRepository;
import org.easyreview.factorys.ContainerFactory;

public class EmailCanNotBeInUseValidator implements ConstraintValidator<EmailCanNotBeInUse, String> {

	@Override
	public void initialize(EmailCanNotBeInUse constraintAnnotation) {
	}

	@Override
	public boolean isValid(String emailAddress, ConstraintValidatorContext context) {
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		try {
			Email email = Email.newInstance(emailAddress);
			return !repository.emailInUse(email);
		} catch (IllegalArgumentException e) {
			return true;
		}
	}

}
