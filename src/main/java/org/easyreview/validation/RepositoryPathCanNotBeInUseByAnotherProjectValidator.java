package org.easyreview.validation;

import java.io.File;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.controllers.projects.ProjectFormConfig;
import org.easyreview.domain.RuleProjectRepositoryPathCanNotBeInUseByAnotherProject;

public class RepositoryPathCanNotBeInUseByAnotherProjectValidator implements ConstraintValidator<RepositoryPathCanNotBeInUseByAnotherProject, ProjectFormConfig> {

	@Override
	public void initialize(RepositoryPathCanNotBeInUseByAnotherProject constraintAnnotation) {
	}

	@Override
	public boolean isValid(ProjectFormConfig form, ConstraintValidatorContext context) {
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = createRule(form);
		return rule.inAccord();
	}

	private RuleProjectRepositoryPathCanNotBeInUseByAnotherProject createRule(ProjectFormConfig form) {
		Long projectId = form.getId();
		File repositoryPath = new File(form.getRepositoryPath());
		
		return new RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(repositoryPath, projectId);
	}

}
