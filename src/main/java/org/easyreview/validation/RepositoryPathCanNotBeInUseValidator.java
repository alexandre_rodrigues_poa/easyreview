package org.easyreview.validation;

import java.io.File;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.domain.ProjectRepository;
import org.easyreview.factorys.ContainerFactory;

public class RepositoryPathCanNotBeInUseValidator implements ConstraintValidator<RepositoryPathCanNotBeInUse, String> {

	@Override
	public void initialize(RepositoryPathCanNotBeInUse constraintAnnotation) {
	}

	@Override
	public boolean isValid(String repositoryPath, ConstraintValidatorContext context) {
		if (repositoryPath == null)
			return true;
		
		ProjectRepository repository = ContainerFactory.get(ProjectRepository.class);
		File path = new File(repositoryPath);
		
		return !repository.repositoryPathInUse(path);
	}
}
