package org.easyreview.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.controllers.users.UserFormPassword;

public class PasswordMacthValidator implements ConstraintValidator<PasswordMatch, UserFormPassword> {

	@Override
	public void initialize(PasswordMatch constraintAnnotation) {
	}

	@Override
	public boolean isValid(UserFormPassword userPassword, ConstraintValidatorContext context) {
		if(((userPassword.getPassword() == null) || (userPassword.getConfirmPassword() == null)))
			return true;
		return userPassword.getPassword().equals(userPassword.getConfirmPassword());
	}
}
