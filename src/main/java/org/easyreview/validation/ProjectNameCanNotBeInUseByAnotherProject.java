package org.easyreview.validation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy=ProjectNameCanNotBeInUseByAnotherProjectValidator.class)
@Target(TYPE)
@Retention(RUNTIME)
public @interface ProjectNameCanNotBeInUseByAnotherProject {
	  String message() default "";
	  Class<?>[] groups() default { };
	  Class<? extends Payload>[] payload() default { };
}
