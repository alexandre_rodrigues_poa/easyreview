package org.easyreview.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.controllers.projects.ProjectFormConfig;
import org.easyreview.domain.RuleProjectNameCanNotBeInUseByAnotherProject;

public class ProjectNameCanNotBeInUseByAnotherProjectValidator implements ConstraintValidator<ProjectNameCanNotBeInUseByAnotherProject, ProjectFormConfig> {

	@Override
	public void initialize(ProjectNameCanNotBeInUseByAnotherProject constraintAnnotation) {
	}

	@Override
	public boolean isValid(ProjectFormConfig project, ConstraintValidatorContext context) {
		RuleProjectNameCanNotBeInUseByAnotherProject rule = createRule(project);
		return rule.inAccord();
	}

	private RuleProjectNameCanNotBeInUseByAnotherProject createRule(ProjectFormConfig project) {
		String projectName = project.getName();
		Long projectId = project.getId();
		return new RuleProjectNameCanNotBeInUseByAnotherProject(projectName, projectId);
	}
}
