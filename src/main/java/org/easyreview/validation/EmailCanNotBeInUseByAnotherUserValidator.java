package org.easyreview.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.easyreview.controllers.users.UserFormEdit;
import org.easyreview.domain.Email;
import org.easyreview.domain.RuleEmailCanNotBeInUseByAnotherUser;

public class EmailCanNotBeInUseByAnotherUserValidator implements ConstraintValidator<EmailCanNotBeInUseByAnotherUser, UserFormEdit> {

	@Override
	public void initialize(EmailCanNotBeInUseByAnotherUser constraintAnnotation) {
	}

	@Override
	public boolean isValid(UserFormEdit user, ConstraintValidatorContext context) {
		Email email;

		try {
			email = Email.newInstance(user.getEmailAddress());
		} catch (IllegalArgumentException e) {
			return true;
		}
		
		long userId = user.getId();
		
		RuleEmailCanNotBeInUseByAnotherUser rule = new RuleEmailCanNotBeInUseByAnotherUser(email, userId);
		
		return rule.inAccord();
	}

}
