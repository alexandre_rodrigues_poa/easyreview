package org.easyreview.validation;

import static java.lang.annotation.RetentionPolicy.*;
import static java.lang.annotation.ElementType.*;

import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Constraint(validatedBy=ProjectNameCanNotBeInUseValidator.class)
@Target(FIELD)
@Retention(RUNTIME)
public @interface ProjectNameCanNotBeInUse {
	  String message() default "";
	  Class<?>[] groups() default { };
	  Class<? extends Payload>[] payload() default { };
}
