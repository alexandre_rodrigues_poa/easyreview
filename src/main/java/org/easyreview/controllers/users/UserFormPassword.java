package org.easyreview.controllers.users;

import org.easyreview.validation.PasswordMatch;
import org.hibernate.validator.constraints.NotBlank;

@PasswordMatch
public class UserFormPassword {
	
	@NotBlank
	private String password;
	
	@NotBlank
	private String confirmPassword;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String repeatPassword) {
		this.confirmPassword = repeatPassword;
	}

	@Override
	public String toString() {
		return String.format("password=%s, confirmPassword=%s", password, confirmPassword);
	}
	
	
	
}
