package org.easyreview.controllers.users;

import org.easyreview.domain.SystemUser;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.easyreview.validation.EmailCanNotBeInUseByAnotherUser;

@EmailCanNotBeInUseByAnotherUser
public class UserFormEdit {

	private Long id;
	
	@NotBlank
	private String name;
	
	@NotBlank @Email
	private String emailAddress;

	private boolean isAdmin;

	public UserFormEdit() {}

	public UserFormEdit(SystemUser systemUser) {
		id = systemUser.getId();
		name = systemUser.getName();
		emailAddress = systemUser.getEmailAddress();
		isAdmin = systemUser.isAdmin();
	}

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public boolean isAdmin() {
		return isAdmin;
	}
	
	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}
}
