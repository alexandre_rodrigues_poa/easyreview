package org.easyreview.controllers.users;

import org.easyreview.domain.SystemUser;
import org.easyreview.domain.UserManipulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class EditUser {
	
	@Autowired
	private UserManipulationService service;
	
	@RequestMapping(value="/edit/{userId}", method=GET)
    public ModelAndView getUser(@PathVariable Long userId){
		SystemUser systemUser = SystemUser.findBy(userId);
		UserFormEdit user = new UserFormEdit(systemUser);
		
		ModelAndView modelAndView = new ModelAndView("users/edit");
		modelAndView.addObject("userFormEdit", user);
		
		return modelAndView;
    }

	@RequestMapping(value="/edit", method=GET)
    public ModelAndView formEdit(@ModelAttribute UserFormEdit user){
		ModelAndView modelAndView = new ModelAndView("users/edit");
		return modelAndView;
    }

	@RequestMapping(value="/edit", method=PUT)
    public ModelAndView save(@Valid @ModelAttribute UserFormEdit user, BindingResult validateResult, RedirectAttributes redirectAttributes) {
		if (validateResult.hasErrors())
			return formEdit(user);
		
		change(user);
		
		redirectAttributes.addFlashAttribute("userEdited", user.getName());
		redirectAttributes.addFlashAttribute("userId", user.getId());
		
        return new ModelAndView("redirect:listAll");
    }

	private void change(UserFormEdit user) {
		Long userId = user.getId();
		String name = user.getName();
		String emailAddress = user.getEmailAddress();
		Boolean isAdmin = user.isAdmin();
		
		service.editUser(userId, name, emailAddress, isAdmin);
	}
}