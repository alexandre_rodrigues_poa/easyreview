package org.easyreview.controllers.users;

import org.easyreview.domain.SystemUser;
import org.easyreview.domain.UserManipulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.validation.Valid;

@Controller
@RequestMapping("/users")
public class InsertNewUser {
	
	@Autowired
	private UserManipulationService service;
	
	@RequestMapping(value="/insertNew", method=GET)
    public ModelAndView formInsertNewUser(@ModelAttribute UserFormInsert user){
		ModelAndView modelAndView = new ModelAndView("users/insertNew");
		return modelAndView;
    }

	@RequestMapping(value="/insertNew", method=POST)
    public ModelAndView insertNewUser(@Valid @ModelAttribute UserFormInsert user, BindingResult validateResult, RedirectAttributes redirectAttributes) {
		if (validateResult.hasErrors())
			return formInsertNewUser(user);
		
		SystemUser systemUser = insertNew(user);
		redirectAttributes.addFlashAttribute("newUserInserted", systemUser.getName());
		redirectAttributes.addFlashAttribute("userId", systemUser.getId());
		
        return new ModelAndView("redirect:listAll");
    }

	private SystemUser insertNew(UserFormInsert user) {
		String name = user.getName();
		String emailAddress = user.getEmailAddress();
		String password = user.getPassword();
		Boolean isAdmin = user.isAdmin();
		
		return service.insertNewUser(name, emailAddress, password, isAdmin);
	}
}