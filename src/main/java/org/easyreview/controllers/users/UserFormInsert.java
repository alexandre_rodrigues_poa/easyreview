package org.easyreview.controllers.users;

import javax.validation.Valid;

import org.easyreview.validation.EmailCanNotBeInUse;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

public class UserFormInsert {
	
	@NotBlank
	private String name;
	
	@NotBlank @Email @EmailCanNotBeInUse
	private String emailAddress;
	
	private boolean isAdmin;
	
	@Valid
	private UserFormPassword userPassword = new UserFormPassword();
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

	public UserFormPassword getUserPassword() {
		return userPassword;
	}

	@Override
	public String toString() {
		return String.format("User [name=%s, email=%s, isAdmin=%s, %s]", name, emailAddress, isAdmin, userPassword);
	}

	public String getPassword() {
		return userPassword.getPassword();
	}	
}
