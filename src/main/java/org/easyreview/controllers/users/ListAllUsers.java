package org.easyreview.controllers.users;

import org.easyreview.domain.SystemUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/users")
public class ListAllUsers {
	
	@RequestMapping(value="/listAll", method=GET)
    public ModelAndView listAll(){
		List<UserFormEdit> users = listUsers();
		
		ModelAndView modelAndView = new ModelAndView("users/listAll");
		modelAndView.addObject("users", users);
        
		return modelAndView;
    }

	private List<UserFormEdit> listUsers() {
		List<SystemUser> systemUsers = SystemUser.listAllActiveOrderedByName();
		List<UserFormEdit> users = new ArrayList<>();
		
		for(SystemUser systemUser : systemUsers)
			users.add(new UserFormEdit(systemUser));
		
		return users;
	}
}