package org.easyreview.controllers.users;

import org.easyreview.domain.SystemUser;
import org.easyreview.domain.UserManipulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static org.springframework.web.bind.annotation.RequestMethod.*;

@Controller
@RequestMapping("/users")
public class RemoveUser {
	
	@Autowired
	private UserManipulationService service;
	
	@RequestMapping(value="/remove/{userId}", method=GET)
    public ModelAndView remove(@PathVariable Long userId, RedirectAttributes redirectAttributes){
		SystemUser  user = service.removeUser(userId);
		redirectAttributes.addFlashAttribute("userRemoved", user.getName());
		return new ModelAndView("redirect:/users/listAll");
    }
}