package org.easyreview.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AccessDeniedController {

	@RequestMapping(value="/accessDenied", method=GET) 
	public String getAccessDenied() {
		return "accessDenied"; 
	}
}
