package org.easyreview.controllers.help;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/help")
public class HelpController {

	@RequestMapping(value="/textFormatting", method=GET)
	public String textFormatting() {
		return "help/textFormatting";
	}
}
