package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.ArrayList;
import java.util.List;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.ReviewRepository;
import org.easyreview.domain.GitCommit;
import org.easyreview.domain.GitRepository;
import org.easyreview.domain.Project;
import org.easyreview.domain.SystemUser;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class ListCommits {

	@Autowired
	private ReviewRepository reviewRepository;
	
	@Autowired
	private LoggedUser loggedUser;

	@RequestMapping(value="/listCommits/{projectId}", method=GET)
	public ModelAndView list(@PathVariable Long projectId) {
		
		Project project = Project.findBy(projectId);
		
		List<CommitView> commits = listCommits(project);
		
		ModelAndView modelAndView = new ModelAndView("projects/commits");
		
		modelAndView.addObject("projectName", project.getName());
		modelAndView.addObject("commits", commits);
		
		return modelAndView;
	}

	private List<CommitView> listCommits(Project project) {
		GitRepository repository = project.getGitRepository();
		List<CommitView> commits = new ArrayList<>();
		
		for (GitCommit commit : repository.listCommits())
			commits.add(createView(commit));
		
		return commits;
	}

	private CommitView createView(GitCommit commit) {
		long filesReviewed = filesReviewed(commit);
		boolean loggedUserReviewed = loggedUserReviewed(commit);
		List<CodeAnalysisStatus> analysisStatusList = listAnalysisStatus(commit);
		return new CommitView(commit, filesReviewed, loggedUserReviewed, analysisStatusList);
	}

	private List<CodeAnalysisStatus> listAnalysisStatus(GitCommit commit) {
		return reviewRepository.listCodeAnalysisStatusFrom(commit.getId());
	}

	private long filesReviewed(GitCommit commit) {
		return reviewRepository.countFilesWithReviewFrom(commit.getId());
	}

	private boolean loggedUserReviewed(GitCommit commit) {
		String commitId = commit.getId();
		
		long userId = loggedUser.getUserId();
		SystemUser author = SystemUser.findBy(userId);
		
		return reviewRepository.userReviewedTheCommit(commitId, author);
	}
}
