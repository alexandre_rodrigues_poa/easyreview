package org.easyreview.controllers.projects;

import javax.validation.constraints.NotNull;

import org.easyreview.domain.SystemUser;

public class UserData {

	@NotNull
	private Long id;
	private String userName;
	private String userEmail;
	private boolean isProjectMembership;

	public UserData() {
	}

	public UserData(boolean isProjectMembership, SystemUser systemUser) {
		setId(systemUser.getId());
		setName(systemUser.getName());
		setEmail(systemUser.getEmailAddress());
		setIsProjectMembership(isProjectMembership);
	}

	public boolean getIsProjectMembership() {
		return isProjectMembership;
	}

	public void setIsProjectMembership(Boolean IsProjectMembership) {
		this.isProjectMembership = IsProjectMembership;
	}
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return userName;
	}
	public void setName(String name) {
		this.userName = name;
	}

	public String getEmail() {
		return userEmail;
	}
	
	public void setEmail(String email) {
		this.userEmail = email;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof UserData))
			return false;
		UserData other = (UserData) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
