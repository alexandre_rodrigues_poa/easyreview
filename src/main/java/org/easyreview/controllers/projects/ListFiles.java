package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.io.File;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.ReviewRepository;
import org.easyreview.domain.GitCommit;
import org.easyreview.domain.GitRepository;
import org.easyreview.domain.Project;
import org.easyreview.domain.SystemUser;
import org.easyreview.repositories.CommittedFile;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class ListFiles {

	@Autowired
	private ReviewRepository repository;

	@Autowired
	private LoggedUser loggedUser;
	
	@RequestMapping(value="/listFiles/{projectId}/{commitId}", method=GET)
	public ModelAndView list(@PathVariable Long projectId, @PathVariable String commitId) {
		
		Project project = Project.findBy(projectId);
		GitCommit commit = getCommit(commitId, project);
		
		List<CommittedFileView> committedFiles = new ArrayList<>();
		
		for(CommittedFile committedFile : commit.listFiles()) {
			CommittedFileView view = createView(commit, committedFile);
			committedFiles.add(view);
		}
		
		sortByFileName(committedFiles);
		
		ModelAndView modelAndView = new ModelAndView("projects/files");
		
		modelAndView.addObject("projectName", project.getName());
		modelAndView.addObject("commitMessage", commit.getMessage());
		modelAndView.addObject("files", committedFiles);
		
		return modelAndView;
	}

	private GitCommit getCommit(String commitId, Project project) {
		GitRepository gitRepository = project.getGitRepository();
		GitCommit commit = gitRepository.findCommitBy(commitId);
		return commit;
	}

	private CommittedFileView createView(GitCommit commit, CommittedFile committedFile) {
		String commitId = commit.getId();
		File file = new File(committedFile.getName());
		
		long countReviews = repository.countReviewsFrom(commitId, file);
		
		long userId = loggedUser.getUserId();
		SystemUser user = SystemUser.findBy(userId);
		
		boolean loggedUserReviewedTheFile = repository.userReviewedTheFile(commitId, file, user);
		List<CodeAnalysisStatus> analysisStatusList = repository.listCodeAnalysisStatusFrom(commitId, file);
		
		return new CommittedFileView(commit, committedFile, countReviews, loggedUserReviewedTheFile, analysisStatusList);
	}

	private void sortByFileName(List<CommittedFileView> committedFiles) {
		committedFiles.sort(new Comparator<CommittedFileView>() {
			@Override
			public int compare(CommittedFileView firstFile, CommittedFileView secondFile) {
				return firstFile.getFileName().compareTo(secondFile.getFileName());
			}
		});
	}
}
