package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.easyreview.domain.ProjectManipulationService;
import org.easyreview.domain.RepositoryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class InsertNewProjectController {

	@Autowired
	private ProjectManipulationService service;

	@RequestMapping(value="/insertNew", method=GET)
    public ModelAndView form(@ModelAttribute ProjectFormInsert project){
		ModelAndView modelAndView = new ModelAndView("projects/insertNew");
		return modelAndView;
    }

	@RequestMapping(value="/insertNew", method=POST)
    public ModelAndView insert(@Valid @ModelAttribute ProjectFormInsert project, BindingResult validateResult) {
		if (validateResult.hasErrors())
			return form(project);
		
		insertProject(project);

		return new ModelAndView("redirect:/projects/list");
    }

	private void insertProject(ProjectFormInsert projectForm) {
		String name = projectForm.getName();
		String repositoryPath = projectForm.getRepositoryPath();
		Set<Long> userIDs = listAllProjectMemberships(projectForm);
		LocalDate commitsFromDate = projectForm.getCommitsFromDate();
		RepositoryType repositoryType = projectForm.getRepositoryType();
		
		service.insertNewProject(name, repositoryPath, userIDs, commitsFromDate, repositoryType);
	}

	private Set<Long> listAllProjectMemberships(ProjectFormInsert project) {
		Set<Long> userIDs = new HashSet<>();
		
		for(UserData userData : project.getUsers())
			if (userData.getIsProjectMembership())
				userIDs.add(userData.getId());
		
		return userIDs;
	}
}
