package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.ArrayList;
import java.util.List;

import org.easyreview.domain.Project;
import org.easyreview.domain.SystemUser;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class ListProjects {

	@Autowired
	private LoggedUser loggedUser;
	
	@RequestMapping(value="/list", method=GET)
	public ModelAndView list() {
		ModelAndView result = new ModelAndView("projects/list");
		result.addObject("projects", listProjects());
		return result;
	}
	
	private List<ProjectView> listProjects() {
		if (loggedUser.getIsDefaultUser())
			return new ArrayList<>();
		if (loggedUser.getIsAdmin())
			return listAllProjects();
		else {
			Long userId = loggedUser.getUserId();
			SystemUser user = SystemUser.findBy(userId);
			return listUserProjects(user);
		}
	}

	private List<ProjectView> listAllProjects() {
		List<Project> projects = Project.listAllOrderedByName();
		return projecsToProjectViews(projects);
	}

	private List<ProjectView> listUserProjects(SystemUser user) {
		List<Project> projects = Project.listUser(user);
		return projecsToProjectViews(projects);
	}

	private List<ProjectView> projecsToProjectViews(List<Project> projects) {
		List<ProjectView> result = new ArrayList<>();
		projects.forEach(project -> result.add(new ProjectView(project)));	
		return result;
	}
}
