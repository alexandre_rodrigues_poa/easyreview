package org.easyreview.controllers.projects;

import org.easyreview.domain.Project;

public class ProjectView {

	private long id;
	private String name;
	private int numberOfCommits;
	private boolean hasError;
	private int newCommits;

	public ProjectView(Project project) {
		this.id = project.getId();
		this.name = project.getName();
		try {
			this.numberOfCommits = project.getNumberOfCommits();
		} catch (Exception e) {
			hasError = true;
		}
		this.newCommits = project.getNewCommits();
	}

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public int getNumberOfCommits() {
		return numberOfCommits;
	}

	public boolean getHasError() {
		return hasError;
	}

	public int getNewCommits() {
		return newCommits;
	}
}
