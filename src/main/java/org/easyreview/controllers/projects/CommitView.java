package org.easyreview.controllers.projects;

import static org.easyreview.domain.CodeAnalysisStatus.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.easyreview.Registry;
import org.easyreview.domain.Email;
import org.easyreview.domain.GitCommit;
import org.easyreview.domain.CodeAnalysisStatus;

public class CommitView {
	private String commitId;
	private Long projectId;
	private String message;
	private String committerName;
	private Email committerEmail;
	private LocalDateTime dateTime;
	private int numberOfFiles;
	private long filesReviewed;
	private boolean loggedUserReviewed;
	private boolean hasAnOkReview;
	private boolean hasAnCautionReview;
	private boolean hasAnErrorReview;

	public CommitView(GitCommit gitCommit, long filesReviewed, boolean loggedUserReviewed, List<CodeAnalysisStatus> analysisStatusList) {
		this.commitId = gitCommit.getId();
		this.projectId = gitCommit.getProjectId();
		this.message = gitCommit.getMessage();
		this.committerName = gitCommit.getCommitterName();
		this.committerEmail = gitCommit.getCommitterEmail();
		this.dateTime = gitCommit.getDateTime();
		this.numberOfFiles = gitCommit.getNumberOfFiles();
		this.filesReviewed = filesReviewed;
		this.loggedUserReviewed = loggedUserReviewed;
		this.hasAnOkReview = analysisStatusList.contains(OK);
		this.hasAnCautionReview = analysisStatusList.contains(CAUTION);
		this.hasAnErrorReview = analysisStatusList.contains(ERROR);
	}

	public String getCommitId() {
		return commitId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public String getMessage() {
		return message;
	}

	public String getCommitterName() {
		return committerName;
	}

	public String getCommitterEmail() {
		return committerEmail.getAddress();
	}

	public String getDate() {
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		return dateTime.format(formatter);
	}
	
	public int compareDate(CommitView other) {
		if (other.dateTime.isBefore(this.dateTime))
			return -1;
		if (other.dateTime.isAfter(this.dateTime))
			return 1;
		return 0;
	}

	public int getNumberOfFiles() {
		return numberOfFiles;
	}

	public long getFilesReviewed() {
		return filesReviewed;
	}

	public boolean getLoggedUserReviewed() {
		return loggedUserReviewed;
	}

	public boolean getHasAnOkReview() {
		return hasAnOkReview;
	}

	public boolean getHasAnCautionReview() {
		return hasAnCautionReview;
	}

	public boolean getHasAnErrorReview() {
		return hasAnErrorReview;
	}
}
