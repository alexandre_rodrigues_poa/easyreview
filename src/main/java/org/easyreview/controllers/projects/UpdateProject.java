package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.easyreview.domain.Project;
import org.easyreview.domain.ProjectUpdateSevice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class UpdateProject {

	@Autowired
	private ProjectUpdateSevice updateSevice;
	
	@RequestMapping(value="/update/{projectId}", method=GET)
	public ModelAndView update(@PathVariable Long projectId) {
		Project project = Project.findBy(projectId);
		updateSevice.update(project);
		return new ModelAndView("redirect:../list");
	}
}
