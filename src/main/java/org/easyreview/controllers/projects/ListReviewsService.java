package org.easyreview.controllers.projects;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.easyreview.controllers.projects.reviews.ReviewView;
import org.easyreview.domain.Review;
import org.easyreview.domain.ReviewRepository;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ListReviewsService {
	
	@Autowired
	private ReviewRepository fileReviewRepository;
	
	@Autowired
	private LoggedUser loggedUser;

	public List<ReviewView> list(String commitId, String fileName) {
		
		File file = new File(fileName);
		List<Review> reviews = fileReviewRepository.listReviewsFrom(commitId, file);
		
		List<ReviewView> views = new ArrayList<>();
		for(Review review : reviews)
			views.add(new ReviewView(review, loggedUser));
		
		return views;
	}
}
