package org.easyreview.controllers.projects;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FilenameUtils;

public class FileViewMode {

	private Map<String, String> modes = new HashMap<>();
	
	public FileViewMode() {
		modes.put("java", "text/x-java");
		modes.put("xml", "application/xml");
		modes.put("css", "text/css");
		modes.put("xhtml", "text/html");
		modes.put("html", "text/html");
		modes.put("htm", "text/html");
		modes.put("js", "text/javascript");
		modes.put("json", "application/json");
		modes.put("jsp", "application/x-jsp");
		modes.put("sql", "text/x-sql");
		modes.put("pas", "text/x-pascal");
		modes.put("php", "application/x-httpd-php");
		modes.put("rb", "text/x-ruby");
		modes.put("scala", "text/x-scala");
	}
	
	public String get(String fileName) {
		String extension = FilenameUtils.getExtension(fileName);
		String mode = modes.get(extension);
		return mode != null ? mode : "text";
	}
}
