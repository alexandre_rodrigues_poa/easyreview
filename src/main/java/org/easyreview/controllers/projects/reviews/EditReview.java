package org.easyreview.controllers.projects.reviews;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.List;

import javax.validation.Valid;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.Review;
import org.easyreview.domain.ReviewManipulationService;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects/review")
public class EditReview {

	@Autowired
	private ReviewManipulationService userService;
	
	@Autowired
	private LoggedUser loggedUser;
	
	@RequestMapping(value="/edit/{reviewId}", method=GET)
	public ModelAndView getReview(@PathVariable Long reviewId) {
		Review review = Review.findBy(reviewId);
		ModelAndView modelAndView = new ModelAndView("fragments/editReview :: editReview");
		modelAndView.addObject("reviewForm", new ReviewForm(review));
		return modelAndView;
	}

	@RequestMapping(value="/edit", method=GET)
    public ModelAndView getForm(@ModelAttribute ReviewForm form) {
		ModelAndView modelAndView = new ModelAndView("fragments/editReview :: editReview");
		return modelAndView;
	}
	
	@RequestMapping(value="/edit", method=POST)
    public ModelAndView editReply(@Valid @ModelAttribute ReviewForm form, BindingResult validateResult) {
		if (validateResult.hasErrors())
			return getForm(form);

		Review review = saveChanges(form);
		
		ModelAndView modelAndView = loadViewFrom(review);
		return modelAndView;
	}

	@RequestMapping(value="/cancelEdit/{reviewId}", method=GET)
	public ModelAndView cancel(@PathVariable Long reviewId) {
		Review review = Review.findBy(reviewId);
		ModelAndView modelAndView = loadViewFrom(review);
		return modelAndView;
	}

	@ModelAttribute("allCodeAnalysisStatus")
	public List<CodeAnalysisStatusView> listAllCodeAnalysisStatus() {
		return CodeAnalysisStatusView.listAll();
	}
	
	private Review saveChanges(ReviewForm form) {
		Long userId = loggedUser.getUserId();
		Long reviewId = form.getId();
		CodeAnalysisStatus analysisStatus = form.getAnalysisStatus();
		String comment = form.getComment();
		String url = form.getPageUrl();
		
		Review review = userService.editReview(userId, reviewId, analysisStatus, comment, url);
		
		return review;
	}

	private ModelAndView loadViewFrom(Review review) {
		ModelAndView modelAndView = new ModelAndView("fragments/review :: review");
		modelAndView.addObject("review", new ReviewView(review, loggedUser));
		return modelAndView;
	}
}
