package org.easyreview.controllers.projects.reviews;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.easyreview.Registry;
import org.easyreview.domain.ReplyReview;
import org.easyreview.security.LoggedUser;
import org.pegdown.PegDownProcessor;

public class ReplyReviewView {

	private Long id;
	private String text;
	private LocalDateTime date;
	private String authorName;
	private boolean edited;
	private LocalDateTime lastEdition;
	private boolean loggedUserIsAuthor;

	public ReplyReviewView(ReplyReview reply, LoggedUser loggedUser) {
		this.id = reply.getId();
		this.text = reply.getText();
		this.date = reply.getDateTime();
		this.authorName = reply.getAuthor().getName();
		this.edited = reply.edited();
		this.lastEdition = reply.getLastEdition();
		this.loggedUserIsAuthor = loggedUser.isAuthorOf(reply);
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		PegDownProcessor processor = new PegDownProcessor();
        return processor.markdownToHtml(text);		
	}

	public String getDate() {
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		return formatter.format(date);
	}

	public String getAuthorName() {
		return authorName;
	}

	public boolean getEdited() {
		return edited;
	}

	public String getLastEdition() {
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		return formatter.format(lastEdition);
	}

	public boolean getLoggedUserIsAuthor() {
		return loggedUserIsAuthor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ReplyReviewView))
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		ReplyReviewView other = (ReplyReviewView) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return "ReplyReviewView [id=" + id + ", date=" + date + ", authorName=" + authorName + ", edited=" + edited
				+ ", lastEdition=" + lastEdition + ", loggedUserIsAuthor=" + loggedUserIsAuthor + "]";
	}
}
