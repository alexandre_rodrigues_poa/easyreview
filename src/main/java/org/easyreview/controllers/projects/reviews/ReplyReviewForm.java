package org.easyreview.controllers.projects.reviews;

import org.easyreview.domain.ReplyReview;
import org.hibernate.validator.constraints.NotBlank;

public class ReplyReviewForm {

	private Long id;
	private Long reviewId;
	@NotBlank
	private String text;
	private String PageUrl;

	public ReplyReviewForm() {
	}

	public ReplyReviewForm(ReplyReview reply) {
		id = reply.getId();
		text = reply.getText();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getReviewId() {
		return reviewId;
	}

	public void setReviewId(Long reviewId) {
		this.reviewId = reviewId;
	}

	public String getText() {
		return  text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getPageUrl() {
		return PageUrl;
	}

	public void setPageUrl(String urlPage) {
		this.PageUrl = urlPage;
	}
}
