package org.easyreview.controllers.projects.reviews;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.validation.Valid;

import org.easyreview.domain.ReplyReview;
import org.easyreview.domain.ReplyReviewManipulationService;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects/review/reply")
public class EditReply {

	@Autowired
	private ReplyReviewManipulationService service;
	
	@Autowired
	private LoggedUser loggedUser;
	
	@RequestMapping(value="/edit/{replyId}", method=GET)
	public ModelAndView getReply(@PathVariable Long replyId) {
		ReplyReview reply = ReplyReview.findBy(replyId);
		ModelAndView modelAndView = new ModelAndView("fragments/formReplyReview :: formReplyReview");
		modelAndView.addObject("replyReviewForm", new ReplyReviewForm(reply));
		return modelAndView;
	}

	@RequestMapping(value="/edit", method=GET)
    public ModelAndView getForm(@ModelAttribute ReplyReviewForm replyReview) {
		ModelAndView modelAndView = new ModelAndView("fragments/formReplyReview :: formReplyReview");
		return modelAndView;
	}

	@RequestMapping(value="/edit", method=POST)
    public ModelAndView editReply(@Valid @ModelAttribute ReplyReviewForm form, BindingResult validateResult) {
		if (validateResult.hasErrors())
			return getForm(form);

		ReplyReview reply = saveChanges(form);

		ModelAndView modelAndView = loadViewFrom(reply);
		return modelAndView;
	}

	private ReplyReview saveChanges(ReplyReviewForm form) {
		Long userId = loggedUser.getUserId();
		Long replyId = form.getId();
		String text = form.getText();
		String pageUrl = form.getPageUrl();
		
		ReplyReview reply = service.editReply(userId, replyId, text, pageUrl);
		
		return reply;
	}

	private ModelAndView loadViewFrom(ReplyReview reply) {
		ModelAndView modelAndView = new ModelAndView("fragments/reply :: reply");
		modelAndView.addObject("reply", new ReplyReviewView(reply, loggedUser));
		return modelAndView;
	}
}
