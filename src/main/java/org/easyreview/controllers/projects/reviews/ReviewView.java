package org.easyreview.controllers.projects.reviews;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.easyreview.Registry;
import org.easyreview.domain.*;
import org.easyreview.security.LoggedUser;
import org.pegdown.PegDownProcessor;

public class ReviewView {
	private Long id;
	private CodeAnalysisStatusView codeAnalysisStatusView;
	private String comment;
	private LocalDateTime date;
	private String authorName;
	private boolean loggedUserIsAuthorOfReview;
	private boolean edited;
	private LocalDateTime lastEdition;
	private List<ReplyReviewView> replies = new ArrayList<>();
	
	public ReviewView(Review review, LoggedUser loggedUser) {
		this.id = review.getId();
		this.codeAnalysisStatusView = new CodeAnalysisStatusView(review.getAnalysisStatus());
		this.comment = review.getComment();
		this.date = review.getDateTime();
		this.authorName = review.getAuthor().getName();
		this.edited = review.edited();
		this.lastEdition = review.lastEdition();
		this.loggedUserIsAuthorOfReview = loggedUser.isAuthorOf(review);
		setReplies(review, loggedUser);
	}

	public Long getId() {
		return id;
	}

	public CodeAnalysisStatusView getAnalysisStatus() {
		return codeAnalysisStatusView;
	}

	public String getComment() {
		PegDownProcessor processor = new PegDownProcessor();
		return processor.markdownToHtml(comment);
	}

	public String getDate() {
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		return formatter.format(date);
	}

	public String getAuthorName() {
		return authorName;
	}

	public boolean getLoggedUserIsAuthor() {
		return loggedUserIsAuthorOfReview;
	}

	public boolean getEdited() {
		return edited;
	}

	public String getLastEdition() {
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		return formatter.format(lastEdition);
	}

	public List<ReplyReviewView> getReplies() {
		return replies ;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof ReviewView))
			return false;
		
		ReviewView other = (ReviewView) obj;
		return id.equals(other.id);
	}

	@Override
	public String toString() {
		return String.format(
				"FileReviewView [id=%s, codeAnalysisStatusView=%s, comment=%s, date=%s, authorName=%s, loggedUserIsAuthorOfReview=%s, edited=%s, lastEdition=%s, replies=%s]",
				id, codeAnalysisStatusView, comment, date, authorName, loggedUserIsAuthorOfReview, edited, lastEdition,
				replies);
	}

	private void setReplies(Review review, LoggedUser loggedUser) {
		for (ReplyReview reply : review.listReplies())
			replies.add(new ReplyReviewView(reply, loggedUser));
	}

	public boolean getSomeoneHasReplied() {
		return !replies.isEmpty();
	}
}
