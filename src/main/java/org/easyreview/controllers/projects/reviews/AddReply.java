package org.easyreview.controllers.projects.reviews;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import javax.validation.Valid;

import org.easyreview.domain.Review;
import org.easyreview.domain.ReplyReviewManipulationService;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects/review/reply")
public class AddReply {

	@Autowired
	private ReplyReviewManipulationService service;
	@Autowired
	private LoggedUser loggedUser; 

	@RequestMapping(value="/add", method=GET)
    public ModelAndView getForm(@ModelAttribute ReplyReviewForm form) {
		ModelAndView modelAndView = new ModelAndView("fragments/formReplyReview :: formReplyReview");
		return modelAndView;
	}
	
	@RequestMapping(value="/add", method=POST)
    public ModelAndView addReply(@Valid @ModelAttribute ReplyReviewForm form, BindingResult validateResult) {
		if (validateResult.hasErrors())
			return getForm(form);
		
		insertReply(form);
		
		ModelAndView modelAndView = new ModelAndView("fragments/review :: review");
		
		ReviewView review = getReview(form);
		modelAndView.addObject("review", review);
		
		return modelAndView;
	}

	private ReviewView getReview(ReplyReviewForm replyReview) {
		Long reviewId = replyReview.getReviewId();
		Review review = Review.findBy(reviewId);
		ReviewView view = new ReviewView(review, loggedUser);
		return view;
	}

	private void insertReply(ReplyReviewForm form) {
		Long userId = loggedUser.getUserId();
	    Long reviewId = form.getReviewId();
		String text = form.getText();
		String pageUrl = form.getPageUrl();
		
		service.insertNewReply(userId, reviewId, text, pageUrl);
	}
}
