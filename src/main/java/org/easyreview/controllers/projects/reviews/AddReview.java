package org.easyreview.controllers.projects.reviews;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.easyreview.controllers.projects.ListReviewsService;
import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.ReviewManipulationService;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects/review")
public class AddReview {
	
	@Autowired
	private LoggedUser loggedUser;
	@Autowired
	private ReviewManipulationService service;
	@Autowired
	private ListReviewsService fileReviews;
	
	@RequestMapping(value="/add", method=GET)
    public ModelAndView getForm(@ModelAttribute ReviewForm fileReview) {
		ModelAndView modelAndView = new ModelAndView("fragments/formAddReview :: formAddReview");
		return modelAndView;
	}
	
	@RequestMapping(value="/add", method=POST)
    public ModelAndView addReview(@Valid @ModelAttribute ReviewForm form, BindingResult validateResult, HttpServletRequest request) {
		if (validateResult.hasErrors())
			return getForm(form);

		insertReview(form);
		
		ModelAndView modelAndView = new ModelAndView("fragments/reviews :: reviews");
		modelAndView.addObject("reviews", listReviews(form));
		
		return modelAndView;
	}

	@ModelAttribute("allCodeAnalysisStatus")
	public List<CodeAnalysisStatusView> listAllCodeAnalysisStatus() {
		return CodeAnalysisStatusView.listAll();
	}

	private void insertReview(ReviewForm form) {
		Long userId = loggedUser.getUserId();
		Long projectId = form.getProjectId();
		String commitId = form.getCommitId(); 
		String fileName = form.getFile();
		CodeAnalysisStatus analysisStatus = form.getAnalysisStatus();
		String comment = form.getComment();
		String url = form.getPageUrl();
		
		service.insertNewReview(userId, projectId, commitId, fileName, analysisStatus, comment, url);
	}
	
	private List<ReviewView> listReviews(ReviewForm fileReview) {
		String commitId = fileReview.getCommitId(); 
		String fileName = fileReview.getFile();
		
		return fileReviews.list(commitId, fileName);
	}
}
