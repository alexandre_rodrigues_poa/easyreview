package org.easyreview.controllers.projects.reviews;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.*;

import org.easyreview.controllers.projects.FileViewMode;
import org.easyreview.controllers.projects.ListReviewsService;
import org.easyreview.domain.Email;
import org.easyreview.domain.GitCommit;
import org.easyreview.domain.GitRepository;
import org.easyreview.domain.Project;
import org.easyreview.domain.SystemUser;
import org.easyreview.repositories.git.ModifiedFile;
import org.easyreview.security.LoggedUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects/review")
public class ReviewController  {
	
	@Autowired
	private LoggedUser loggedUser;
	
	@Autowired
	private ListReviewsService fileReviews;
	
	@RequestMapping(value="/modifiedFile/{projectId}/{commitId}", params="fileName", method=GET)
	public ModelAndView loadModifiedFile(@PathVariable Long projectId, @PathVariable String commitId, @RequestParam("fileName") String fileName) {
		Project project = Project.findBy(projectId);
		GitRepository repository = project.getGitRepository();
		GitCommit commit = repository.findCommitBy(commitId);
		
		ModifiedFile modifiedFile = commit.readModifiedFile(fileName);
		
		Map<String, Object> attributes = createCommonAttributes(fileName, project, commit);
		attributes.put("files", modifiedFile);
		
		ModelAndView modelAndView = new ModelAndView("projects/review/modifiedFiles", attributes);
		return modelAndView;
	}

	@RequestMapping(value="/addedFile/{projectId}/{commitId}", params="fileName", method=GET)
	public ModelAndView loadAddedFile(@PathVariable Long projectId, @PathVariable String commitId, @RequestParam("fileName") String fileName) {
		Project project = Project.findBy(projectId);
		GitRepository repository = project.getGitRepository();
		GitCommit commit = repository.findCommitBy(commitId);
		
		String file = commit.readFile(fileName);
		
		Map<String, Object> attributes = createCommonAttributes(fileName, project, commit);
		attributes.put("file", file);

		return new ModelAndView("projects/review/addedFile", attributes);
	}

	@RequestMapping(value="/removedFile/{projectId}/{commitId}", params="fileName", method=GET)
	public ModelAndView loadRemovedFile(@PathVariable Long projectId, @PathVariable String commitId, @RequestParam("fileName") String fileName) {
		Project project = Project.findBy(projectId);
		GitRepository repository = project.getGitRepository();
		GitCommit commit = repository.findCommitBy(commitId);
		
		String file = commit.readRemovedFile(fileName);
		
		Map<String, Object> attributes = createCommonAttributes(fileName, project, commit);
		attributes.put("file", file);

		return new ModelAndView("projects/review/removedFile", attributes);
	}

	@ModelAttribute("allCodeAnalysisStatus")
	public List<CodeAnalysisStatusView> listAllCodeAnalysisStatus() {
		return CodeAnalysisStatusView.listAll();
	}

	private Map<String, Object> createCommonAttributes(String fileName, Project project, GitCommit commit) {
		String mode = getMode(fileName);
		
		Map<String, Object> attributes = new HashMap<>();
		attributes.put("projectName", project.getName());
		attributes.put("commitMessage", commit.getMessage());
		attributes.put("fileName", fileName);
		attributes.put("mode", mode);
		attributes.put("reviewForm", createFileReview(fileName, project, commit));
		attributes.put("replyReviewForm", new ReplyReviewForm());
		attributes.put("loggedUserCanAddReview", loggedUserCanAddReview(commit));
		attributes.put("reviews", listReviews(commit, fileName));
		
		return attributes;
	}

	private List<ReviewView> listReviews(GitCommit commit, String fileName) {
		String commitId = commit.getId();
		return fileReviews.list(commitId, fileName);
	}

	private ReviewForm createFileReview(String fileName, Project project, GitCommit commit) {
		ReviewForm reviewForm = new ReviewForm();
		reviewForm.setFile(fileName);
		reviewForm.setCommitId(commit.getId());
		reviewForm.setProjectId(project.getId());
		
		return reviewForm;
	}

	private String getMode(String fileName) {
		FileViewMode fileViewMode = new FileViewMode();
		String mode = fileViewMode.get(fileName);
		return mode;
	}

	private boolean loggedUserCanAddReview(GitCommit commit) {
		Email committerEmail = commit.getCommitterEmail();
		Email loggedUserEmail = getLoggedUserEmail();
		return !loggedUserEmail.equals(committerEmail);
	}

	private Email getLoggedUserEmail() {
		Long userId = loggedUser.getUserId();
		SystemUser user = SystemUser.findBy(userId);
		Email loggedUserEmail = user.getEmail();
		return loggedUserEmail;
	}
}
