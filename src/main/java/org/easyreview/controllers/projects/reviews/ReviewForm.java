package org.easyreview.controllers.projects.reviews;

import javax.validation.constraints.NotNull;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.Review;
import org.hibernate.validator.constraints.NotBlank;

public class ReviewForm {

	private Long id;
	private String commitId;
	private String file;
	
	@NotNull
	private CodeAnalysisStatus analysisStatus;
	@NotBlank
	private String comment;
	private Long projectId;
	
	private String pageUrl;

	public ReviewForm() {
	}

	public ReviewForm(Review review) {
		this.id = review.getId();
		this.commitId = review.getCommitId();
		this.file = review.getFile().getPath();
		this.analysisStatus = review.getAnalysisStatus();
		this.comment = review.getComment();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCommitId() {
		return commitId;
	}

	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}

	public String getFile() {
		return file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public CodeAnalysisStatus getAnalysisStatus() {
		return analysisStatus;
	}

	public void setAnalysisStatus(CodeAnalysisStatus analysisStatus) {
		this.analysisStatus = analysisStatus;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public void setProjectId(Long projectId) {
		this.projectId = projectId;
	}

	public Long getProjectId() {
		return projectId;
	}

	public String getPageUrl() {
		return pageUrl;
	}

	public void setPageUrl(String pageUrl) {
		this.pageUrl = pageUrl;
	}
}
