package org.easyreview.controllers.projects.reviews;

import static org.easyreview.ExceptionsMessages.theParameterCanNotBeNull;

import java.util.ArrayList;
import java.util.List;

import org.easyreview.domain.CodeAnalysisStatus;

public class CodeAnalysisStatusView {

	private CodeAnalysisStatus status;

	public CodeAnalysisStatusView(CodeAnalysisStatus status) {
		validate(status);
		this.status = status;
	}

	public CodeAnalysisStatus getCodeAnalysisStatus() {
		return status;
	}

	public String getInternationalizedDescription() {
		String stringStatus = status.toString();
		return "review.code.analysis.status." + stringStatus .toLowerCase();
	}

	public String getStringStatus() {
		return status.toString();
	}
	
	public static List<CodeAnalysisStatusView> listAll() {
		List<CodeAnalysisStatusView> list = new ArrayList<>();
		for(CodeAnalysisStatus status : CodeAnalysisStatus.values())
			list.add(new CodeAnalysisStatusView(status));
		return list;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + status.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof CodeAnalysisStatusView))
			return false;
		CodeAnalysisStatusView other = (CodeAnalysisStatusView) obj;
		return status.equals(other.status);
	}

	@Override
	public String toString() {
		return String.format("CodeAnalysisStatusView [status=%s]", status);
	}

	private void validate(CodeAnalysisStatus status) {
		if (status == null)
			throw new IllegalArgumentException(theParameterCanNotBeNull("status"));
	}
}
