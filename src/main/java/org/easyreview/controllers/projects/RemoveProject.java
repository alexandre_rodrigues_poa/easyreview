package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.easyreview.domain.ProjectManipulationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class RemoveProject {

	@Autowired
	private ProjectManipulationService service;
	
	@RequestMapping(value="/remove/{projectId}", method=GET)
	public ModelAndView getProject(@PathVariable Long projectId) {
		service.removeProject(projectId);
		
		ModelAndView modelAndView = new ModelAndView("redirect:/projects/list");
		return modelAndView;
	}
}
