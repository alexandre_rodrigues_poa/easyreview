package org.easyreview.controllers.projects;

import static org.easyreview.repositories.CommittedFileStatus.*;
import static org.easyreview.domain.CodeAnalysisStatus.*;

import java.util.List;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.GitCommit;
import org.easyreview.repositories.CommittedFile;
import org.easyreview.repositories.CommittedFileStatus;

public class CommittedFileView {

	private Long projectId;
	private String commitId;
	private String fileName;
	private CommittedFileStatus status;
	private long numberOfReviews;
	private boolean loggedUserReviewedTheFile;
	private boolean hasAnOkReview;
	private boolean hasAnCautionReview;
	private boolean hasAnErrorReview;

	public CommittedFileView(GitCommit gitCommit, CommittedFile committedFile, long numberOfReviews, boolean loggedUserReviewedTheFile, List<CodeAnalysisStatus> analysisStatusList) {
		this.projectId = gitCommit.getProjectId();
		this.commitId = gitCommit.getId();
		this.fileName = committedFile.getName();
		this.status = committedFile.getStatus();
		this.numberOfReviews = numberOfReviews;
		this.loggedUserReviewedTheFile = loggedUserReviewedTheFile;
		this.hasAnOkReview = analysisStatusList.contains(OK);
		this.hasAnCautionReview = analysisStatusList.contains(CAUTION);
		this.hasAnErrorReview = analysisStatusList.contains(ERROR);
	}

	public Long getProjectId() {
		return projectId;
	}

	public String getCommitId() {
		return commitId;
	}

	public String getFileName() {
		return fileName;
	}

	public String getFileStatus() {
		return "project.file.status." + status.toString().toLowerCase();
	}

	public long getNumberOfReviews() {
		return numberOfReviews;
	}

	public boolean getHasAnOkReview() {
		return hasAnOkReview;
	}

	public boolean getHasAnCautionReview() {
		return hasAnCautionReview;
	}

	public boolean getHasAnErrorReview() {
		return hasAnErrorReview;
	}

	public String getRevisonPath() {
		switch (status) {
		case ADDED:
			return String.format("review/addedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		case CHANGED:
			return String.format("review/modifiedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		case REMOVED:
			return String.format("review/removedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		default:
			throw new IllegalStateException("Can't review removed file");
		}
	}

	public boolean getAdded() {
		return status.equals(ADDED);
	}

	public boolean getLoggedUserReviewed() {
		return loggedUserReviewedTheFile;
	}
}
