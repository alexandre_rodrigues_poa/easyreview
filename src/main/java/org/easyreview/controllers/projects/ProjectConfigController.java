package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.validation.Valid;

import org.easyreview.domain.Project;
import org.easyreview.domain.ProjectManipulationService;
import org.easyreview.domain.RepositoryType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class ProjectConfigController {

	@Autowired
	private ProjectManipulationService service;
	
	@RequestMapping(value="/config/{projectId}", method=GET)
	public ModelAndView getProject(@PathVariable Long projectId) {
		Project project = Project.findBy(projectId);
		
		ModelAndView modelAndView = new ModelAndView("projects/config");
		modelAndView.addObject("projectFormConfig", new ProjectFormConfig(project));
		return modelAndView;
	}
	
	@RequestMapping(value="/config", method=GET)
	public ModelAndView form(@ModelAttribute ProjectFormConfig projectFormConfig) {
		ModelAndView modelAndView = new ModelAndView("projects/config");
		return modelAndView;
	}
	
	@RequestMapping(value="/config", method=PUT)
	public ModelAndView save(@Valid @ModelAttribute ProjectFormConfig projectForm, BindingResult validateResult) {
		if (validateResult.hasErrors())
			return form(projectForm);
		
		saveProject(projectForm);
		
		ModelAndView modelAndView = new ModelAndView("redirect:list");
		return modelAndView;
	}
	
	private void saveProject(ProjectFormConfig projectForm) {
		Long projectId = projectForm.getId();
		String name = projectForm.getName();
		String repositoryPath = projectForm.getRepositoryPath();
		RepositoryType repositoryType = projectForm.getRepositoryType();
		Set<Long> userIDs = listAllProjectMemberships(projectForm);
		LocalDate commitsFromDate = projectForm.getCommitsFromDate();

		service.editProject(projectId, name, repositoryPath, repositoryType, userIDs, commitsFromDate);
	}

	private Set<Long> listAllProjectMemberships(ProjectFormConfig project) {
		Set<Long> userIDs = new HashSet<>();
		
		for(UserData userData : project.getUsers())
			if (userData.getIsProjectMembership())
				userIDs.add(userData.getId());
		
		return userIDs;
	}
}
