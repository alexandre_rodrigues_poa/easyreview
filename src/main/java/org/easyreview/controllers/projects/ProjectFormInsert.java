package org.easyreview.controllers.projects;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.easyreview.domain.RepositoryType;
import org.easyreview.domain.SystemUser;
import org.easyreview.validation.ProjectNameCanNotBeInUse;
import org.easyreview.validation.RepositoryPathCanNotBeInUse;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

public class ProjectFormInsert {

	@NotBlank @ProjectNameCanNotBeInUse
	private String name;
	@NotBlank @RepositoryPathCanNotBeInUse
	private String repositoryPath;
	private List<UserData> users = new ArrayList<>();
	@DateTimeFormat(iso=ISO.DATE) @NotNull
	private LocalDate commitsFromDate;
	@NotNull
	private RepositoryType repositoryType;

	public ProjectFormInsert() {
		populateUsers();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRepositoryPath() {
		return repositoryPath;
	}

	public void setRepositoryPath(String repositoryPath) {
		this.repositoryPath = repositoryPath;
	}

	public List<UserData> getUsers() {
		return users;
	}

	public LocalDate getCommitsFromDate() {
		return commitsFromDate;
	}

	public void setCommitsFromDate(LocalDate date) {
		this.commitsFromDate = date;
	}

	public void setRepositoryType(RepositoryType type) {
		this.repositoryType = type;
	}

	public RepositoryType getRepositoryType() {
		return repositoryType;
	}

	private void populateUsers() {
		List<SystemUser> systemUsers = SystemUser.listAllActiveOrderedByName();
		for(SystemUser user : systemUsers)
			users.add(new UserData(false, user));
	}
}
