package org.easyreview.controllers.projects;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.easyreview.domain.Project;
import org.easyreview.domain.ReviewsMetrics;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/projects")
public class Metrics {

	@RequestMapping(value="/metrics/{projectId}", method=GET)
	public ModelAndView generate(@PathVariable long projectId) {
		Project project = Project.findBy(projectId);
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		MetricsView metricsView = new MetricsView(metrics);
		
		ModelAndView modelAndView = new ModelAndView("projects/metrics");
		modelAndView.addObject("project", new ProjectView(project));
		modelAndView.addObject("metrics", metricsView);
		
		return modelAndView;
	}
}
