package org.easyreview.controllers.projects;

import org.easyreview.domain.ReviewsMetrics;

public class MetricsView {

	private long numberOfFilesRevised;
	private long numberOfFilesWithErrors;
	private long numberOfFilesWithoutProblemsAndErrors;
	private long numberOfFilesWithProblems;
	private long numberOfReviews;
	private long numberOfReviewsWithCautionStatus;
	private long numberOfReviewsWithErrorStatus;
	private long numberOfReviewsWithOkStatus;
	private double percentageOfFilesWithErrors;
	private double percentageOfFilesWithoutProblemsAndErrors;
	private double percentageOfFilesWithProblems;
	private double percentageOfReviewsWithCautionStatus;
	private double percentageOfReviewsWithErrorStatus;
	private double percentageOfReviewsWithOkStatus;

	public MetricsView(ReviewsMetrics metrics) {
		this.numberOfFilesRevised = metrics.numberOfFilesRevised();
		this.numberOfFilesWithErrors = metrics.numberOfFilesWithErrors();
		this.numberOfFilesWithoutProblemsAndErrors = metrics.numberOfFilesWithoutProblemsAndErrors();
		this.numberOfFilesWithProblems = metrics.numberOfFilesWithProblems();

		this.numberOfReviews = metrics.numberOfReviews();
		this.numberOfReviewsWithCautionStatus = metrics.numberOfReviewsWithCautionStatus();
		this.numberOfReviewsWithErrorStatus = metrics.numberOfReviewsWithErrorStatus();
		this.numberOfReviewsWithOkStatus = metrics.numberOfReviewsWithOkStatus();
		
		this.percentageOfFilesWithErrors = metrics.percentageOfFilesWithErrors();
		this.percentageOfFilesWithoutProblemsAndErrors = metrics.percentageOfFilesWithoutProblemsAndErrors();
		this.percentageOfFilesWithProblems = metrics.percentageOfFilesWithProblems();
		
		this.percentageOfReviewsWithCautionStatus = metrics.percentageOfReviewsWithCautionStatus();
		this.percentageOfReviewsWithErrorStatus = metrics.percentageOfReviewsWithErrorStatus();
		this.percentageOfReviewsWithOkStatus = metrics.percentageOfReviewsWithOkStatus();
	}

	public long getNumberOfFilesRevised() {
		return numberOfFilesRevised;
	}

	public long getNumberOfFilesWithErrors() {
		return numberOfFilesWithErrors;
	}

	public long getNumberOfFilesWithoutProblemsAndErrors() {
		return numberOfFilesWithoutProblemsAndErrors;
	}

	public long getNumberOfFilesWithProblems() {
		return numberOfFilesWithProblems;
	}

	public long getNumberOfReviews() {
		return numberOfReviews;
	}

	public long getNumberOfReviewsWithCautionStatus() {
		return numberOfReviewsWithCautionStatus;
	}

	public long getNumberOfReviewsWithErrorStatus() {
		return numberOfReviewsWithErrorStatus;
	}

	public long getNumberOfReviewsWithOkStatus() {
		return numberOfReviewsWithOkStatus;
	}

	public long getPercentageOfFilesWithErrorsRounded() {
		return Math.round(percentageOfFilesWithErrors);
	}

	public long getPercentageOfFilesWithoutProblemsAndErrorsRounded() {
		return Math.round(percentageOfFilesWithoutProblemsAndErrors);
	}

	public long getPercentageOfFilesWithProblemsRounded() {
		return Math.round(percentageOfFilesWithProblems);
	}

	public long getPercentageOfReviewsWithCautionStatusRounded() {
		return Math.round(percentageOfReviewsWithCautionStatus);
	}

	public long getPercentageOfReviewsWithErrorStatusRounded() {
		return Math.round(percentageOfReviewsWithErrorStatus);
	}

	public long getPercentageOfReviewsWithOkStatusRounded() {
		return Math.round(percentageOfReviewsWithOkStatus);
	}
}
