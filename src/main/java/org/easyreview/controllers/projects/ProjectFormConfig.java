package org.easyreview.controllers.projects;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.easyreview.domain.Project;
import org.easyreview.domain.RepositoryType;
import org.easyreview.domain.SystemUser;
import org.easyreview.validation.ProjectNameCanNotBeInUseByAnotherProject;
import org.easyreview.validation.RepositoryPathCanNotBeInUseByAnotherProject;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

@ProjectNameCanNotBeInUseByAnotherProject
@RepositoryPathCanNotBeInUseByAnotherProject
public class ProjectFormConfig {

	@NotNull
	private Long id;
	@NotBlank
	private String name;
	@NotBlank
	private String repositoryPath;
	private List<UserData> users = new ArrayList<>();
	@DateTimeFormat(iso=ISO.DATE) @NotNull
	private LocalDate commitsFromDate;
	@NotNull
	private RepositoryType repositoryType;

	public ProjectFormConfig() {
	}

	public ProjectFormConfig(Project project) {
		setId(project.getId());
		setName(project.getName());
		setRepositoryPath(project.getRepositoryPath().getPath());
		setCommitsFromDate(project.getCommitsFromDate());
		setRepositoryType(project.getRepositoryType());
		populateUsers(project);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getRepositoryPath() {
		return repositoryPath;
	}

	public void setRepositoryType(RepositoryType repositoryType) {
		this.repositoryType = repositoryType;
	}

	public RepositoryType getRepositoryType() {
		return repositoryType;
	}

	public void setRepositoryPath(String repositoryPath) {
		this.repositoryPath = repositoryPath;
	}

	public void setCommitsFromDate(LocalDate date) {
		this.commitsFromDate = date;
	}

	public LocalDate getCommitsFromDate() {
		return commitsFromDate;
	}

	public List<UserData> getUsers() {
		return users;
	}

	private void populateUsers(Project project) {
		for(SystemUser user : SystemUser.listAllActiveOrderedByName()) {
			boolean isMembership = project.isProjectMembership(user);
			users.add(new UserData(isMembership , user));
		}
	}
}
