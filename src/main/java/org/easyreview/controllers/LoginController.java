package org.easyreview.controllers;

import static org.springframework.web.bind.annotation.RequestMethod.*;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
	
	 @RequestMapping(value="/login", method=GET)
	 public String getLogin() {
		 return "login";
	 }
	 
	 @RequestMapping(value="/loginError", method=GET)
	 public String getLoginError(Model model) {
		 model.addAttribute("loginError", true);
		 return "login";
	 }
}
