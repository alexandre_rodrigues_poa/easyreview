package org.easyreview.scheduling;

import java.util.List;

import org.easyreview.domain.Project;
import org.easyreview.domain.ProjectUpdateSevice;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ProjectsUpdateSchedule {
	
	@Value("${scheduling.projectsUpdate.update:true}")
	private boolean update = true;
	
	@Scheduled(cron="${scheduling.projectsUpdate.cron:0 0 3 1/1 * ?}")
	public void update() {
		if (update) {
			List<Project> projects = Project.listAllOrderedByName();
			ProjectUpdateSevice service = new ProjectUpdateSevice();
			for(Project project : projects)
				service.update(project);
		}
	}
}
