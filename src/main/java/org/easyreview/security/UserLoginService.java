package org.easyreview.security;

import org.easyreview.domain.Email;
import org.easyreview.domain.SystemUser;
import org.easyreview.domain.SystemUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserLoginService implements UserDetailsService{

	@Autowired
	private SystemUserRepository repository;
	
	@Override
	public UserDetails loadUserByUsername(String emailAddress) throws UsernameNotFoundException {
		Email email = Email.newInstance(emailAddress);
		SystemUser user = repository.findUserBy(email);
		
		if (user != null)
			return new LoginUser(user);
		else if (userIsDefaultAdmin(emailAddress))
			return createDefaultUser(emailAddress);
		
		throw new UsernameNotFoundException(emailAddress);
	}

	private boolean userIsDefaultAdmin(String emailAddress) {
		return emailAddress.equals("sysadmin@easyreview.org") && repository.notHasAdminUser();
	}

	private UserDetails createDefaultUser(String emailAddress) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String hash = encoder.encode("sysadmin");
		return new LoginUser("SysAdmin", emailAddress, hash);
	}
}
