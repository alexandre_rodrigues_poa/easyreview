package org.easyreview.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(
		prePostEnabled = true,
		securedEnabled = true,
		jsr250Enabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService users;
	
	@Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
//		httpSecurity.authorizeRequests().antMatchers("/").permitAll();
		
        httpSecurity.authorizeRequests().
    		antMatchers("/favicon.ico").permitAll().
        	antMatchers("/webjars/**").permitAll().
        	antMatchers("/images/**").permitAll().
        	antMatchers("/font-awesome/**").permitAll().
        	antMatchers("/css/**").permitAll().
        	antMatchers("/help/**").permitAll().
        	antMatchers("/users/**").hasAnyRole("DEFAULT_USER", "ADMIN").
        	antMatchers("/projects/insertNew").hasRole("ADMIN").
        	antMatchers("/projects/config/**").hasRole("ADMIN").
        	anyRequest().authenticated().
        and().
        	formLogin().
        		loginPage("/login").
        		failureUrl("/loginError").
        		usernameParameter("email").
        		passwordParameter("password").
        		permitAll().
        and().
        	logout().
        	logoutSuccessUrl("/login").
        	deleteCookies("rememberMeId").
        	permitAll().
        and().
        	exceptionHandling().accessDeniedPage("/accessDenied").
        and().
        	rememberMe().rememberMeParameter("rememberMeId");
    }
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(users).passwordEncoder(new BCryptPasswordEncoder());
    }
}
