package org.easyreview.security;

import java.util.Collection;
import java.util.HashSet;

import org.easyreview.domain.SystemUser;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

class LoginUser implements UserDetails {

	private static final long serialVersionUID = 1L;
	private String password;
	private String email;
	private String name;
	private Long userId;
	private boolean isAdmin;
	private boolean isDefaultUser;
	
	public LoginUser(SystemUser user) {
		userId = user.getId();
		name = user.getName();
		password = user.getPasswordHash();
		email = user.getEmailAddress();
		isAdmin = user.isAdmin();
	}
	
	public LoginUser(String name, String userName, String password) {
		this.name = name;
		this.email = userName;
		this.password = password;
		this.isDefaultUser = true;
	}

	public boolean isAdmin() {
		return isAdmin;
	}
	
	@Override
	public Collection<GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new HashSet<>();
		
		if (isAdmin)
			authorities.add(new AdminAuthority());
		if (isDefaultUser)
			authorities.add(new DefaultUserAuthority());
		
		return authorities;
	}

	public Long getUserId() {
		return userId;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return email;
	}

	public String getName() {
		return name;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean isDefaultUser() {
		return isDefaultUser;
	}
}
