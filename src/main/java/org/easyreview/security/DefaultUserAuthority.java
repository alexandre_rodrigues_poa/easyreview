package org.easyreview.security;

import org.springframework.security.core.GrantedAuthority;

public class DefaultUserAuthority implements GrantedAuthority {

	private static final long serialVersionUID = 1L;
	private final String role = "ROLE_DEFAULT_USER";

	@Override
	public String getAuthority() {
		return role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + role.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof DefaultUserAuthority))
			return false;
		DefaultUserAuthority other = (DefaultUserAuthority) obj;
		return role.equals(other.role);
	}
}
