package org.easyreview.security;

import org.springframework.security.core.GrantedAuthority;

class AdminAuthority implements GrantedAuthority {
	
	private static final long serialVersionUID = 1L;
	private final String role = "ROLE_ADMIN";

	@Override
	public String getAuthority() {
		return role;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + role.hashCode();
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!(obj instanceof AdminAuthority))
			return false;
		AdminAuthority other = (AdminAuthority) obj;
		return role.equals(other.role);
	}
}
