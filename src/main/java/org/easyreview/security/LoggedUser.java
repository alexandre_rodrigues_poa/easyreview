package org.easyreview.security;

import org.easyreview.domain.Review;
import org.easyreview.domain.ReplyReview;
import org.easyreview.domain.SystemUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class LoggedUser {

	public boolean getIsAdmin() {
		return getUserIsLogged() && getUser().isAdmin();
	}
	
	public boolean getUserIsLogged() {
		return getUser() != null;
	}
	
	public String getName() {
		return getUserIsLogged() ? getUser().getName() : null;
	}

	private LoginUser getUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if ((authentication != null) && (authentication.getPrincipal() instanceof LoginUser))
			return (LoginUser)authentication.getPrincipal();
		return null;
	}

	public Long getUserId() {
		return  getUserIsLogged() ? getUser().getUserId() : null;
	}

	public boolean getIsDefaultUser() {
		return  getUserIsLogged() ? getUser().isDefaultUser() : false;
	}

	public boolean getCanManageUsers() {
		if (getUserIsLogged())
			return getUser().isDefaultUser() || getUser().isAdmin();
		
		return false;
	}

	public boolean isAuthorOf(Review review) {
		SystemUser author = review.getAuthor();
		return loggedUserEquals(author);
	}

	public boolean isAuthorOf(ReplyReview replyReview) {
		SystemUser author = replyReview.getAuthor();
		return loggedUserEquals(author);
	}

	private boolean loggedUserEquals(SystemUser user) {
		Long authorId = user.getId();
		boolean loggedUserId = authorId.equals(getUserId());
		
		return loggedUserId;
	}	
}
