package org.easyreview.mail;

import javax.mail.internet.MimeMessage;

import org.easyreview.domain.NotificationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class MailSendingService {
	
	private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);

	@Value("${mail.from:notreply@easyreview.org}")
	private String from;
	
	@Value("${mail.configured:false}")
	private boolean mailConfigured;

	@Autowired
	private JavaMailSender mailSender;
	
	@Async
	public void send(String[] to, String subject, String message, boolean htmlMessage) {
		if (mailConfigured && (to.length > 0)) {
			MimeMessage mimeMsg = mailSender.createMimeMessage();
			MimeMessageHelper helper = new MimeMessageHelper(mimeMsg);
			try {
				helper.setSubject(subject);
				helper.setText(message, htmlMessage);
				helper.setTo(to);
				helper.setFrom(from);
				mailSender.send(mimeMsg);
			} catch (Exception e) {
				logger.error("Send e-mail error", e);
			}
		}
	}
}
