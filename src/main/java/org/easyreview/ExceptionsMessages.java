package org.easyreview;

import java.io.File;

import org.easyreview.domain.SystemUser;

public class ExceptionsMessages {
	public static String theParameterCanNotBeNull(String parameter) {
		return String.format("The parameter '%s' can't be null", parameter);
	} 
	public static String theParameterCanNotBeBlank(String parameter) {
		return String.format("The parameter '%s' can't be blank", parameter);
	}
	public static String theParameterCanNotBeInUse(String parameter) {
		return String.format("The parameter '%s' can't be in use", parameter);
	}
	public static String emailAddressIsInvalid(String emailAddress) {
		 return String.format("Email address '%s' is invalid", emailAddress);
	}
	public static String canNotEditARemovedUser() {
		return "Can't edit a removed user!";
	}
	public static String canNotEditARemovedProject() {
		return "Can't edit a removed project!";
	}
	public static String theNameIsInUseByAnotherProject(String name) {
		return String.format("The name '%s' is in use by another project", name);
	}
	public static String theRepositoryPathIsInUseByAnotherProject(File repositoryPath) {
		return String.format("The repository path '%s' is in use by another project", repositoryPath);
	}
	public static String usersParameterCanNotContainANullUser() {
		return "Users parameter can't contain a null user!";
	}
	public static String usersParameterCanNotContainARemovedUser() {
		return "Users parameter can't contain a removed user!";
	}
	public static String theAuthorOfTheCommitCanNotBeTheAuthorOfTheReview(String email) {
		return "The author of the commit can not be the author of the review: " + email;
	}
	public static String onlyTheOriginalAuthorCanModifyTheReview(SystemUser originalAuthor, SystemUser currentAuthor) {
		return String.format("Only the orginal author can modify the review! Original author %s! Current author %s", originalAuthor, currentAuthor);
	}
	public static String onlyTheOriginalAuthorCanModifyTheReply(SystemUser originalAuthor, SystemUser currentAuthor) {
		return String.format("Only the orginal author can modify the reply! Original author %s! Current author %s", originalAuthor, currentAuthor);
	}

	public static String theNumberOfNewCommitsCanNotLessThanOrEqualToZero() {
		return "The number of new commits can not less than or equal to zero";
	}
}
