package org.easyreview.persistence;

import static org.junit.Assert.*;

import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;

import org.easyreview.utils.DateConverter;
import org.junit.Test;

public class LocalDateConverterTest {

	@Test
	public void testConvertToDatabaseColumn() {
		LocalDate localDate = LocalDate.now();
		Date date = DateConverter.localDateToDate(localDate);
		
		LocalDateConverter converter = new LocalDateConverter();
		assertEquals(date, converter.convertToDatabaseColumn(localDate));
	}

	@Test
	public void testConvertToDatabaseColumnWithNullParameter() {
		LocalDateConverter converter = new LocalDateConverter();
		assertNull(converter.convertToDatabaseColumn(null));
	}
	
	@Test
	public void testConvertToEntityAttribute() {
		Date date = new Date();
		LocalDate localDate =  date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		
		LocalDateConverter converter = new LocalDateConverter();
		assertEquals(localDate, converter.convertToEntityAttribute(date));
	}

	@Test
	public void testConvertToEntityAttributeWithNullParameter() {
		LocalDateConverter converter = new LocalDateConverter();
		assertNull(converter.convertToEntityAttribute(null));
	}
}
