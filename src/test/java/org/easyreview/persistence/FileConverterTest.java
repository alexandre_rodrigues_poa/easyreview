package org.easyreview.persistence;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class FileConverterTest {

	@Test
	public void testConvertToDatabaseColumn() {
		File file = new File("/test/testFile.test");
		FileConverter converter = new FileConverter();
		
		assertEquals(file.getPath(), converter.convertToDatabaseColumn(file));
	}

	@Test
	public void testConvertToDatabaseColumnWithNullParameter() {
		FileConverter converter = new FileConverter();
		assertNull(converter.convertToDatabaseColumn(null));
	}

	@Test
	public void testConvertToEntityAttribute() {
		File file = new File("/test");
		FileConverter converter = new FileConverter();
		
		assertEquals(file, converter.convertToEntityAttribute(file.getPath()));
	}

	@Test
	public void testConvertToEntityAttributeWithNullParameter() {
		FileConverter converter = new FileConverter();
		assertNull(converter.convertToEntityAttribute(null));
	}
}
