package org.easyreview.persistence;

import static org.junit.Assert.*;

import java.util.Date;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;

import org.junit.Test;

public class LocalDateTimeConverterTest {

	@Test
	public void testConvertToDatabaseColumn() {
		LocalDateTime localDateTime = LocalDateTime.now();
		Date date = Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
		
		LocalDateTimeConverter converter = new LocalDateTimeConverter();
		assertEquals(date, converter.convertToDatabaseColumn(localDateTime));
	}

	@Test
	public void testConvertToDatabaseColumnWithNullParameter() {
		LocalDateTimeConverter converter = new LocalDateTimeConverter();
		assertNull(converter.convertToDatabaseColumn(null));
	}
	
	@Test
	public void testConvertToEntityAttribute() {
		Date date = new Date();
		
		Instant instant = Instant.ofEpochMilli(date.getTime());
		LocalDateTime localDateTime = LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
		
		LocalDateTimeConverter converter = new LocalDateTimeConverter();
		assertEquals(localDateTime, converter.convertToEntityAttribute(date));
	}

	@Test
	public void testConvertToEntityAttributeWithNullParameter() {
		LocalDateTimeConverter converter = new LocalDateTimeConverter();
		assertNull(converter.convertToEntityAttribute(null));
	}
}
