package org.easyreview.factorys;

import static org.junit.Assert.*;

import javax.persistence.EntityManager;

import org.easyreview.EasyreviewApplication;
import org.easyreview.domain.SystemUserRepository;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ContainerFactoryTest {

	@Test
	public void testFactoryEntityManager() {
		EntityManager entityManager = ContainerFactory.get(EntityManager.class);
		assertNotNull(entityManager);
		
	}
	
	@Test
	public void testSystemUserRepository() {
		SystemUserRepository repository = ContainerFactory.get(SystemUserRepository.class);
		assertNotNull(repository);
	}
}
