package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.factorys.ContainerFactory;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(ContainerFactory.class)
public class RuleProjectNameCanNotBeInUseByAnotherProjectTest {
	
	private final long projectId = 432L;
	@Mock private ProjectRepository repository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
	}
	
	@Test
	public void testRuleWithNameNotInUse() {
		String projectName = "Project Name";
		when(repository.nameInUse(projectName)).thenReturn(false);
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = new RuleProjectNameCanNotBeInUseByAnotherProject(projectName, projectId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleWithProjectNameInUseByInformedProject() {
		String projectName = "Project Name";
		
		when(repository.nameInUse(projectName)).thenReturn(true);
		when(repository.findActiveProjectIdBy(projectName)).thenReturn(projectId);
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = new RuleProjectNameCanNotBeInUseByAnotherProject(projectName, projectId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleNotInAccord() {
		String projectName = "Project Name";
		
		when(repository.nameInUse(projectName)).thenReturn(true);
		when(repository.findActiveProjectIdBy(projectName)).thenReturn(234L);
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = new RuleProjectNameCanNotBeInUseByAnotherProject(projectName, projectId);
		assertFalse(rule.inAccord());
		assertTrue(rule.notInAccord());
	}
}
