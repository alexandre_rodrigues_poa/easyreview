package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.easyreview.domain.CodeAnalysisStatus.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.easyreview.EasyreviewApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ReviewRepositoryTest {

	@Autowired
	private ReviewRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Test @Transactional
	public void testListReviewsFromCommitAndFile() {
		Project project = Project.newInstance("Test List Reviews", new File("/test/list"), GIT, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review review01 = Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review review02 = Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		
		List<Review> reviews = repository.listReviewsFrom(commitId, fileName);
		assertFalse(reviews.isEmpty());
		
		assertTrue(reviews.contains(review01));
		assertTrue(reviews.contains(review02));	
	}

	@Test @Transactional
	public void testCountReviewsFromCommitAndFile() {
		Project project = Project.newInstance("Test Count Reviews", new File("/test/list"), SVN, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		
		long count = repository.countReviewsFrom(commitId, fileName);
		assertEquals(count, 2);
	}

	@Test @Transactional
	public void testFindReviewByReply() {
		Project project = Project.newInstance("Test Fing Project By Reply Review", new File("/test/find/by/reply"), SVN, new HashSet<>(), LocalDate.now());
		
		SystemUser author = getAuthor();
		File file = new File("/test/persit/file.test");
		String text = "Review test";
		CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.OK;
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(Email.newInstance("committer@easyreview.org"));		
		
		Review review = Review.newInstance(author, project, commit, file, text, analysisStatus);
		
		text = "Reply review";
		ReplyReview reply = ReplyReview.newInstance(author, text);
		review.addIfNotExist(reply);
		
		entityManager.flush();
		entityManager.clear();
		
		assertEquals(review, repository.findReviewBy(reply));
	}
	
	@Test @Transactional
	public void testCountFilesWithReviewFromCommit() {
		Project project = Project.newInstance("Test Count Files with Reviews", new File("/test/count"), GIT, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		
		long count = repository.countFilesWithReviewFrom(commitId);
		assertEquals(1, count);
	}
	
	@Test @Transactional
	public void testUserReviewedTheCommit() {
		Project project = Project.newInstance("Test User Reviewed The Commit", new File("/test/reviewed/commit"), SVN, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		
		assertTrue(repository.userReviewedTheCommit(commitId, author));
	}

	@Test @Transactional
	public void testUserReviewedTheFile() {
		Project project = Project.newInstance("Test User Reviewed The File", new File("/test/reviewed/file"), SVN, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser user = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(user, project, commit, fileName, "Review", ERROR);
		
		assertTrue(repository.userReviewedTheFile(commitId, fileName, user));
	}

	@Test @Transactional
	public void testListCodeAnalysisStatusFromCommit() {
		Project project = Project.newInstance("Test Code Analysis Status From Commit", new File("/test/status/commit"), SVN, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		Review.newInstance(author, project, commit, fileName, "Third review", OK);
		
		List<CodeAnalysisStatus> statusList = repository.listCodeAnalysisStatusFrom(commitId);
		
		assertTrue(statusList.contains(OK));
		assertTrue(statusList.contains(CAUTION));
		assertTrue(statusList.contains(ERROR));
	}

	@Test @Transactional
	public void testListCodeAnalysisStatusFromFile() {
		Project project = Project.newInstance("Test Code Analysis Status From File", new File("/test/status/commit"), GIT, new HashSet<>(), LocalDate.now());
		
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		File fileName = new File("/test/repository/file");
		
		SystemUser author = getAuthor();

		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit.getId()).thenReturn(commitId);
		
		Review.newInstance(author, project, commit, fileName, "First review", CAUTION);
		Review.newInstance(author, project, commit, fileName, "Second review", ERROR);
		Review.newInstance(author, project, commit, fileName, "Third review", OK);
		
		List<CodeAnalysisStatus> statusList = repository.listCodeAnalysisStatusFrom(commitId, fileName);
		
		assertTrue(statusList.contains(OK));
		assertTrue(statusList.contains(CAUTION));
		assertTrue(statusList.contains(ERROR));
	}

	private SystemUser getAuthor() {
		String name = "Review Author";
		Email email = Email.newInstance("author@easyreview.org");
		Password password = Password.newInstance("password");
		boolean isAdmin = false;
		
		SystemUser author = SystemUser.newInstance(name, email, password, isAdmin);
		return author;
	}
}
