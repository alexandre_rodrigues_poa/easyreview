package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.easyreview.domain.CodeAnalysisStatus.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.easyreview.EasyreviewApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ReviewsMetricsRepositoryTest {

	@Autowired
	private ReviewsMetricsRepository repository;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private String commitId01 = "a23286d546d038fc01dbc237a0bcab3247268e94";
	private File fileName01 = new File("/test/repository/file01");
	
	private String commitId02 = "a23286d546d038fc01dbc237a0bcab3247268e95";
	private File fileName02 = new File("/test/repository/file02");
	private Project project;
	private SystemUser author;
	
	
	@Mock private GitCommit commit01;
	@Mock private GitCommit commit02;
	
	@Before @Transactional
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		project = Project.newInstance("Test Reviews Metrics Repository", new File("/test/status/commit"), GIT, new HashSet<>(), LocalDate.now());
		author = getAuthor();
		
		Email commiterEmail = Email.newInstance("committer@easyreview.org");
		
		when(commit01.getCommitterEmail()).thenReturn(commiterEmail);
		when(commit02.getCommitterEmail()).thenReturn(commiterEmail);
		
		when(commit01.getId()).thenReturn(commitId01);
		when(commit02.getId()).thenReturn(commitId02);
	}
	
	@Test @Transactional
	public void testGetNumberOfReviewsFromProject() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", CAUTION);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", ERROR);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfReviews = repository.numberOfReviewsFrom(project);
		
		assertEquals(3, numberOfReviews);
	}

	@Test @Transactional
	public void testGetNumberOfReviewsWithOkStatus() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", CAUTION);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", OK);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfReviewsWithOkStatus = repository.numberOfReviewsWithOkStatusFrom(project);
		
		assertEquals(2, numberOfReviewsWithOkStatus);
	}
	
	@Test @Transactional
	public void testGetNumberReviewsWithCautionStatus() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", CAUTION);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", OK);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfReviewsWithCautionStatus = repository.numberOfReviewsWithCautionStatusFrom(project);
		
		assertEquals(1, numberOfReviewsWithCautionStatus);
	}
	
	@Test @Transactional
	public void testGetNumberReviewsWithErrorStatus() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", ERROR);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", ERROR);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", ERROR);
		
		long numberOfReviewsWithErrorStatus = repository.numberOfReviewsWithErrorStatusFrom(project);
		
		assertEquals(3, numberOfReviewsWithErrorStatus);
	}

	@Test @Transactional
	public void testGetNumberOfFilesRevised() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", ERROR);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", OK);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfFilesRevised = repository.numberOfFilesRevisedFrom(project);
		
		assertEquals(2, numberOfFilesRevised);
	}

	@Test @Transactional
	public void testGetNumberOfFilesWithoutProblemsAndErrors() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", ERROR);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", OK);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfFilesWithoutProblems = repository.numberOfFilesWithoutProblemsAndErrorsFrom(project);
		
		assertEquals(1, numberOfFilesWithoutProblems);
	}

	@Test @Transactional
	public void testGetNumberOfFilesWithProblems() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", CAUTION);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", CAUTION);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", CAUTION);
		
		long numberOfFilesWithProblems = repository.numberOfFilesWithProblemsFrom(project);
		
		assertEquals(2, numberOfFilesWithProblems);
	}


	@Test @Transactional
	public void testGetNumberOfFilesWithErrors() {
		Review.newInstance(author, project, commit01, fileName01, "First review file 01", ERROR);
		Review.newInstance(author, project, commit01, fileName01, "Second review file 02", ERROR);
		Review.newInstance(author, project, commit02, fileName02, "First review 01", OK);
		
		long numberOfFilesWithErrors = repository.numberOfFilesWithErrorsFrom(project);
		
		assertEquals(1, numberOfFilesWithErrors);
	}
	private SystemUser getAuthor() {
		String name = "Review Author";
		Email email = Email.newInstance("author@easyreview.org");
		Password password = Password.newInstance("password");
		boolean isAdmin = false;
		
		SystemUser author = SystemUser.newInstance(name, email, password, isAdmin);
		return author;
	}
}
