package org.easyreview.domain;

import static org.mockito.Mockito.*;

import java.io.File;

import org.easyreview.repositories.git.GitSvnFetchCommand;
import org.easyreview.repositories.git.GitSvnFetchCommandBuilder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.slf4j.Logger;

@RunWith(PowerMockRunner.class)
@PrepareForTest({GitSvnFetchCommandBuilder.class})
public class ProjectUpdateSeviceTest {
	
	@Test
	public void testUpdateProjectOk() throws Exception {
		File repositoryPath = new File("/test/update/project");
		
		Project project = mock(Project.class);
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getRepositoryType()).thenReturn(RepositoryType.SVN);
		
		GitSvnFetchCommand command = mock(GitSvnFetchCommand.class);
		PowerMockito.whenNew(GitSvnFetchCommand.class).withArguments(repositoryPath).thenReturn(command);
		
		String message = "OK";
		when(command.execute()).thenReturn(message);
		
		Logger looger = mock(Logger.class);
		ProjectUpdateSevice service = new ProjectUpdateSevice();
		Whitebox.setInternalState(service, "looger", looger);
		
		service.update(project);
		
		verify(looger, times(1)).debug("UPDATE PROJECT - 'git svn fetch' command executed with sucess:", message);
	}

	@Test
	public void testFailToUpdateTheProject() throws Exception {
		File repositoryPath = new File("/test/update/project");
		
		Project project = mock(Project.class);
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getRepositoryType()).thenReturn(RepositoryType.SVN);
		
		GitSvnFetchCommand command = mock(GitSvnFetchCommand.class);
		PowerMockito.whenNew(GitSvnFetchCommand.class).withArguments(repositoryPath).thenReturn(command);
		
		RuntimeException exception = new RuntimeException("Fail to execute the command");
		when(command.execute()).thenThrow(exception);
		
		Logger looger = mock(Logger.class);
		ProjectUpdateSevice service = new ProjectUpdateSevice();
		Whitebox.setInternalState(service, "looger", looger);
		
		service.update(project);
		
		verify(looger, times(1)).error("UPDATE PROJECT ERROR:", exception);
	}

	@Test
	public void testHasNewCommits() throws Exception {
		final File repositoryPath = new File("/test/update/project");
		final int originalNumberOfCommits = 10;
		final int finalNumberOfCommits = 12;
		final int newCommits = finalNumberOfCommits - originalNumberOfCommits;
		
		Project project = mock(Project.class);
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getNumberOfCommits()).thenReturn(originalNumberOfCommits).thenReturn(finalNumberOfCommits);
		when(project.getRepositoryType()).thenReturn(RepositoryType.SVN);
		
		GitSvnFetchCommand command = mock(GitSvnFetchCommand.class);
		PowerMockito.whenNew(GitSvnFetchCommand.class).withArguments(repositoryPath).thenReturn(command);
		
		String message = "OK";
		when(command.execute()).thenReturn(message);
		
		Logger looger = mock(Logger.class);
		ProjectUpdateSevice service = new ProjectUpdateSevice();
		Whitebox.setInternalState(service, "looger", looger);
		
		service.update(project);
		
		verify(project, times(1)).setNewCommits(newCommits);
	}

	@Test
	public void testNotHasNewCommits() throws Exception {
		final File repositoryPath = new File("/test/update/project");
		final int originalNumberOfCommits = 15;
		final int finalNumberOfCommits = 12;
		final int newCommits = 0;
		
		Project project = mock(Project.class);
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getNumberOfCommits()).thenReturn(originalNumberOfCommits).thenReturn(finalNumberOfCommits);
		when(project.getRepositoryType()).thenReturn(RepositoryType.SVN);
		
		GitSvnFetchCommand command = mock(GitSvnFetchCommand.class);
		PowerMockito.whenNew(GitSvnFetchCommand.class).withArguments(repositoryPath).thenReturn(command);
		
		String message = "OK";
		when(command.execute()).thenReturn(message);
		
		Logger looger = mock(Logger.class);
		ProjectUpdateSevice service = new ProjectUpdateSevice();
		Whitebox.setInternalState(service, "looger", looger);
		
		service.update(project);
		
		verify(project, times(1)).setNewCommits(newCommits);
	}
}
