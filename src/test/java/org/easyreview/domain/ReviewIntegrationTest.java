package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.HashSet;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.easyreview.EasyreviewApplication;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ReviewIntegrationTest {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Test
	@Transactional
	public void testPersist() {
		Project project = Project.newInstance("Test Persist", new File("/test/persist"), GIT, new HashSet<>(), LocalDate.now());
		
		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(Email.newInstance("committer@easyreview.org"));
				
		SystemUser author = SystemUser.newInstance("User Name", Email.newInstance("user@easyreview.org"), Password.newInstance("test"), false);
		File file = new File("/test/persit/file.test");
		String text = "Review test";
		CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.OK;
		 
		Review review = Review.newInstance(author, project, commit, file, text, analysisStatus);
		
		assertTrue(review.getId() != 0);
	}

	@Test
	@Transactional
	public void testReplyPersist() {
		Project project = Project.newInstance("Test Reply Persist", new File("/test/reply/persist"), SVN, new HashSet<>(), LocalDate.now());

		GitCommit commit = mock(GitCommit.class);
		when(commit.getCommitterEmail()).thenReturn(Email.newInstance("committer@easyreview.org"));
				
		SystemUser author = SystemUser.newInstance("User Name", Email.newInstance("user@easyreview.org"), Password.newInstance("test"), false);
		File file = new File("/test/persit/file.test");
		String text = "Review test";
		CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.OK;
		
		Review review = Review.newInstance(author, project, commit, file, text, analysisStatus);
		
		text = "Reply review";
		ReplyReview reply = ReplyReview.newInstance(author, text);
		review.addIfNotExist(reply);

		entityManager.flush();

		assertTrue(reply.getId() > 0);
	}
}
