package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.reflect.Method;

import javax.transaction.Transactional;

import org.junit.*;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SystemUser.class, Password.class})
public class UserManipulationServiceTest {

	private UserManipulationService service = new UserManipulationService(); 
	
	@Test
	public void testInsertNewUser() {
		String name = "User Name";
		String email = "user@email.org";
		String password = "password";
		boolean isAdmin = true;
		
		Password userPassword = mock(Password.class);
		PowerMockito.mockStatic(Password.class);
		when(Password.newInstance(password)).thenReturn(userPassword);

		SystemUser user = mock(SystemUser.class);
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.newInstance(name, Email.newInstance(email), userPassword, isAdmin)).thenReturn(user);
		
		assertEquals(user, service.insertNewUser(name, email, password, isAdmin));
	}

	@Test
	public void testMethodInsertNewUserHasTransactionAnnotation() throws NoSuchMethodException {
		Class<UserManipulationService> objectClass = UserManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("insertNewUser", String.class, String.class, String.class, boolean.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testEditUser() {
		Long id = 1L;
		String name = "User Name";
		String emailAddress = "user@email.org";
		boolean isAdmin = true;
		
		SystemUser user = mock(SystemUser.class);
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(id)).thenReturn(user);
		
		service.editUser(id, name, emailAddress, isAdmin);
		
		verify(user).setName(name);
		verify(user).setEmail(Email.newInstance(emailAddress));
		verify(user).setAdmin(isAdmin);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testNotFoudUserForEdit() {
		Long id = 1L;
		String name = "User Name";
		String emailAddress = "user@email.org";
		boolean isAdmin = true;
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(id)).thenReturn(null);
		
		service.editUser(id, name, emailAddress, isAdmin);
	}

	@Test
	public void testMethodEditUserHasTransactionAnnotation() throws NoSuchMethodException {
		Class<UserManipulationService> objectClass = UserManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("editUser", Long.class, String.class, String.class, boolean.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testRemoveUser() {
		Long id = 1L;
		
		SystemUser user = mock(SystemUser.class);
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(id)).thenReturn(user);
		
		service.removeUser(id);
		
		verify(user, times(1)).remove();
	}
	
	@Test
	public void testMethodRemoveUserHasTransactionAnnotation() throws NoSuchMethodException {
		Class<UserManipulationService> objectClass = UserManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("removeUser", Long.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}
}
