package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.lang.reflect.Method;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.transaction.Transactional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Project.class, SystemUser.class})
public class ProjectManipulationServiceTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testInsertNewProject() {
		Long userId01 = 1L;
		Long userId02 = 2L;
		
		Set<Long> userIDs = new HashSet<>(Arrays.asList(userId01, userId02));
		
		SystemUser user01 = mock(SystemUser.class);
		SystemUser user02 = mock(SystemUser.class);
		
		Set<SystemUser> users = new HashSet<>(Arrays.asList(user01, user02));
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId01)).thenReturn(user01);
		when(SystemUser.findBy(userId02)).thenReturn(user02);
		
		String name = "Insert Project";
		String repositoryPath = "/test/insert/new/project";
		LocalDate fromDate = LocalDate.now();
		
		Project project = mock(Project.class);
		PowerMockito.mockStatic(Project.class);
		when(Project.newInstance(name, new File(repositoryPath), GIT, users, fromDate)).thenReturn(project);
		
		ProjectManipulationService service = new ProjectManipulationService();
		assertEquals(project, service.insertNewProject(name, repositoryPath, userIDs, fromDate, GIT));
	}

	@Test
	public void testMethodInsertNewProjectHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ProjectManipulationService> objectClass = ProjectManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("insertNewProject", String.class, String.class, Set.class, LocalDate.class, RepositoryType.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testEditProject() {
		Long userId01 = 3L;
		Long userId02 = 4L;
		
		Set<Long> userIDs = new HashSet<>(Arrays.asList(userId01, userId02));
		
		SystemUser user01 = mock(SystemUser.class);
		SystemUser user02 = mock(SystemUser.class);
		
		Set<SystemUser> users = new HashSet<>(Arrays.asList(user01, user02));
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId01)).thenReturn(user01);
		when(SystemUser.findBy(userId02)).thenReturn(user02);
		
		String name = "Edit Project";
		String repositoryPath = "/test/edit/new/project";
		RepositoryType repositoryType = GIT;
		LocalDate fromDate = LocalDate.now();
		
		Long projectId = 1L;
		Project project = mock(Project.class);
		PowerMockito.mockStatic(Project.class);
		when(Project.findBy(projectId)).thenReturn(project);
		
		ProjectManipulationService service = new ProjectManipulationService();
		assertEquals(project, service.editProject(projectId, name, repositoryPath, repositoryType, userIDs,  fromDate));
		
		verify(project, times(1)).setName(name);
		verify(project, times(1)).setRepositoryPath(new File(repositoryPath));
		verify(project, times(1)).setUsers(users);
	}

	@Test
	public void testMethodEditNewProjectHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ProjectManipulationService> objectClass = ProjectManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("editProject", Long.class, String.class, String.class, RepositoryType.class, Set.class, LocalDate.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testRemoveProject() {
		Long projectId = 1L;
		Project project = mock(Project.class);
		
		PowerMockito.mockStatic(Project.class);
		when(Project.findBy(projectId)).thenReturn(project);
		
		ProjectManipulationService service = new ProjectManipulationService();
		assertEquals(project, service.removeProject(projectId));
		
		verify(project, times(1)).remove();
	}

	@Test
	public void testMethodRemoveProjectHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ProjectManipulationService> objectClass = ProjectManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("removeProject", Long.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}
}
