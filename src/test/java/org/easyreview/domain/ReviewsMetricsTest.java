package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@PrepareForTest({ContainerFactory.class})
@RunWith(PowerMockRunner.class)
public class ReviewsMetricsTest {

	@Mock
	private ReviewsMetricsRepository repository;
	
	@Mock
	private Project project;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		when(ContainerFactory.get(ReviewsMetricsRepository.class)).thenReturn(repository);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testProjectParamCanNotBeNull() {
		new ReviewsMetrics(null);
	}

	@Test
	public void testNumberOfReviews() {
		final long numberOfReviews = 34L;
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfReviews, metrics.numberOfReviews());
	}

	@Test
	public void testNumberOfReviewsWithOkStatusFrom() {
		final long numberOfReviewsWithOkStatus = 32L;
		when(repository.numberOfReviewsWithOkStatusFrom(project)).thenReturn(numberOfReviewsWithOkStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfReviewsWithOkStatus, metrics.numberOfReviewsWithOkStatus());
	}

	@Test
	public void testNumberOfReviewsWithCautionStatus() {
		final long numberOfReviewsWithCautionStatus = 13L;
		when(repository.numberOfReviewsWithCautionStatusFrom(project)).thenReturn(numberOfReviewsWithCautionStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfReviewsWithCautionStatus, metrics.numberOfReviewsWithCautionStatus());
	}

	@Test
	public void testNumberOfReviewsWithErrorStatus() {
		final long numberOfReviewsWithErrorStatus = 21L;
		when(repository.numberOfReviewsWithErrorStatusFrom(project)).thenReturn(numberOfReviewsWithErrorStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfReviewsWithErrorStatus, metrics.numberOfReviewsWithErrorStatus());
	}

	@Test
	public void testNumberOfFilesRevised() {
		final long numberOfFilesRevised = 20L;
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfFilesRevised, metrics.numberOfFilesRevised());
	}

	@Test
	public void testNumberOfFilesWithoutProblemsAndErrors() {
		final long numberOfFilesWithoutProblemsAndErrors = 9L;
		when(repository.numberOfFilesWithoutProblemsAndErrorsFrom(project)).thenReturn(numberOfFilesWithoutProblemsAndErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfFilesWithoutProblemsAndErrors, metrics.numberOfFilesWithoutProblemsAndErrors());
	}

	@Test
	public void testNumberOfFilesWithProblems() {
		final long numberOfFilesWithProblems = 18L;
		when(repository.numberOfFilesWithProblemsFrom(project)).thenReturn(numberOfFilesWithProblems);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfFilesWithProblems, metrics.numberOfFilesWithProblems());
	}

	@Test
	public void testNumberOfFilesWithErrors() {
		final long numberOfFilesWithErrors = 36L;
		when(repository.numberOfFilesWithErrorsFrom(project)).thenReturn(numberOfFilesWithErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(numberOfFilesWithErrors, metrics.numberOfFilesWithErrors());
	}

	@Test
	public void testPercentageOfReviewsWithOkStatus() {
		final long numberOfReviews = 100L;
		final long numberOfReviewsWithOkStatus = 32L;
		
		final double percentagerOfReviewsWithOkStatus = ((double)numberOfReviewsWithOkStatus / (double)numberOfReviews) * 100.0;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithOkStatusFrom(project)).thenReturn(numberOfReviewsWithOkStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentagerOfReviewsWithOkStatus, metrics.percentageOfReviewsWithOkStatus(), 0);
	}
	
	@Test
	public void testPercentageOfReviewsWithOkStatusWhenNumberOfReviewsIsEqualToZero() {
		final long numberOfReviews = 0L;
		final long numberOfReviewsWithOkStatus = 32L;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithOkStatusFrom(project)).thenReturn(numberOfReviewsWithOkStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfReviewsWithOkStatus(), 0);
	}

	@Test
	public void testPercentageOfReviewsWithCautionStatus() {
		final long numberOfReviews = 100L;
		final long numberOfReviewsWithCautionStatus = 51L;
		
		final double percentagerOfReviewsWithCautionStatus = ((double)numberOfReviewsWithCautionStatus / (double)numberOfReviews) * 100.0;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithCautionStatusFrom(project)).thenReturn(numberOfReviewsWithCautionStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentagerOfReviewsWithCautionStatus, metrics.percentageOfReviewsWithCautionStatus(), 0);
	}

	@Test
	public void testPercentageOfReviewsWithCautionStatusWhenNumberOfReviewsIsEqualToZero() {
		final long numberOfReviews = 0L;
		final long numberOfReviewsWithCautionStatus = 51L;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithCautionStatusFrom(project)).thenReturn(numberOfReviewsWithCautionStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfReviewsWithCautionStatus(), 0);
	}

	@Test
	public void testPercentageOfReviewsWithErrorStatus() {
		final long numberOfReviews = 100L;
		final long numberOfReviewsWithErrorStatus = 25L;
		
		final double percentagerOfReviewsWithErrorStatus = ((double)numberOfReviewsWithErrorStatus / (double)numberOfReviews) * 100.0;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithErrorStatusFrom(project)).thenReturn(numberOfReviewsWithErrorStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentagerOfReviewsWithErrorStatus, metrics.percentageOfReviewsWithErrorStatus(), 0);
	}

	@Test
	public void testPercentageOfReviewsWithErrorStatusWhenNumberOfReviewsIsEqualToZero() {
		final long numberOfReviews = 0L;
		final long numberOfReviewsWithErrorStatus = 25L;
		
		when(repository.numberOfReviewsFrom(project)).thenReturn(numberOfReviews);
		when(repository.numberOfReviewsWithErrorStatusFrom(project)).thenReturn(numberOfReviewsWithErrorStatus);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfReviewsWithErrorStatus(), 0);
	}

	@Test
	public void testPercentageOfFilesWithoutProblemsAndErrors() {
		final long numberOfFilesRevised = 200L;
		final long numberOfFilesWithoutProblemsAndErrors = 50L;
		
		final double percentageOfFilesWithoutProblemsAndErrors = ((double)numberOfFilesWithoutProblemsAndErrors / (double)numberOfFilesRevised) * 100.0;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithoutProblemsAndErrorsFrom(project)).thenReturn(numberOfFilesWithoutProblemsAndErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentageOfFilesWithoutProblemsAndErrors, metrics.percentageOfFilesWithoutProblemsAndErrors(), 0);
	}

	@Test
	public void testPercentageOfFilesWithoutProblemsAndErrorsWhenNumberOfFilesIsEqualToZero() {
		final long numberOfFilesRevised = 0L;
		final long numberOfFilesWithoutProblemsAndErrors = 50L;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithoutProblemsAndErrorsFrom(project)).thenReturn(numberOfFilesWithoutProblemsAndErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfFilesWithoutProblemsAndErrors(), 0);
	}

	@Test
	public void testPercentageOfFilesWithProblems() {
		final long numberOfFilesRevised = 200L;
		final long numberOfFilesWithProblems = 75L;
		
		final double percentageOfFilesWithProblems = ((double)numberOfFilesWithProblems / (double)numberOfFilesRevised) * 100.0;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithProblemsFrom(project)).thenReturn(numberOfFilesWithProblems);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentageOfFilesWithProblems, metrics.percentageOfFilesWithProblems(), 0);
	}

	@Test
	public void testPercentageOfFilesWWithProblemsWhenNumberOfFilesIsEqualToZero() {
		final long numberOfFilesRevised = 0L;
		final long numberOfFilesWithProblems = 75L;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithErrorsFrom(project)).thenReturn(numberOfFilesWithProblems);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfFilesWithProblems(), 0);
	}

	@Test
	public void testPercentageOfFilesWithErrors() {
		final long numberOfFilesRevised = 200L;
		final long numberOfFilesWithErrors = 30L;
		
		final double percentageOfFilesWithErrors = ((double)numberOfFilesWithErrors / (double)numberOfFilesRevised) * 100.0;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithErrorsFrom(project)).thenReturn(numberOfFilesWithErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(percentageOfFilesWithErrors, metrics.percentageOfFilesWithErrors(), 0);
	}

	@Test
	public void testPercentageOfFilesWWithErrorsWhenNumberOfFilesIsEqualToZero() {
		final long numberOfFilesRevised = 0L;
		final long numberOfFilesWithErrors = 30L;
		
		when(repository.numberOfFilesRevisedFrom(project)).thenReturn(numberOfFilesRevised);
		when(repository.numberOfFilesWithErrorsFrom(project)).thenReturn(numberOfFilesWithErrors);
		
		ReviewsMetrics metrics = new ReviewsMetrics(project);
		assertEquals(0, metrics.percentageOfFilesWithErrors(), 0);
	}
}
