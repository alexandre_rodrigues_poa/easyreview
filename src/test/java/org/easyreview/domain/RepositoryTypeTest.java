package org.easyreview.domain;

import static org.junit.Assert.*;

import org.easyreview.repositories.git.GitPullCommandBuilder;
import org.easyreview.repositories.git.GitSvnFetchCommandBuilder;
import org.easyreview.repositories.git.GitUpdateCommandBuilder;
import org.junit.Test;

public class RepositoryTypeTest {

	@Test
	public void testGetGitDescription() {
		RepositoryType gitType = RepositoryType.GIT;
		assertEquals("Git", gitType.getDescription());				
	}
	
	@Test
	public void testGetGitUpdateCommandBuilderFromGitRepository() {
		RepositoryType gitType = RepositoryType.GIT;
		
		GitUpdateCommandBuilder builder = gitType.getGitUpdateCommandBuilder();
		assertTrue(builder instanceof GitPullCommandBuilder);
	}

	@Test
	public void testGetSvnDescription() {
		RepositoryType svnType = RepositoryType.SVN;
		assertEquals("Svn", svnType.getDescription());				
	}

	@Test
	public void testGetGitUpdateCommandBuilderFromSvnRepository() {
		RepositoryType svnType = RepositoryType.SVN;
		
		GitUpdateCommandBuilder builder = svnType.getGitUpdateCommandBuilder();
		assertTrue(builder instanceof GitSvnFetchCommandBuilder);
	}
}
