package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.easyreview.repositories.git.RepositoryFactory;
import org.easyreview.repositories.git.ServiceGitCommit;
import org.easyreview.utils.DateConverter;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({RepositoryFactory.class, GitRepository.class, DateConverter.class})
public class GitRepositoryTest {
	
	private final File repositoryPath =  new File("/Backup Dell Inspiron/Desenvolvimento/Java/Quantis");
	
	@Mock private Project project;
	@Mock private Repository repository;
	@Mock private ServiceGitCommit serviceGitCommit;
	private final LocalDate commitsFromDate = LocalDate.of(2000, 01, 01);
	@SuppressWarnings("deprecation")
	private final Date fromDate = new Date(2000, 01, 01); 

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getCommitsFromDate()).thenReturn(commitsFromDate);
		
		PowerMockito.mockStatic(DateConverter.class);
		PowerMockito.when(DateConverter.localDateToDate(commitsFromDate)).thenReturn(fromDate);
		
		PowerMockito.mockStatic(RepositoryFactory.class);
		PowerMockito.when(RepositoryFactory.createFrom(repositoryPath)).thenReturn(repository);
		
		PowerMockito.whenNew(ServiceGitCommit.class).withArguments(repository).thenReturn(serviceGitCommit);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheProjectParameterCanNotBeNull() {
		new GitRepository(null);
	}

	@Test
	public void testGetNumberOfCommits() throws NoHeadException, GitAPIException, IOException {
		int numberOfCommits = 43;

		when(serviceGitCommit.countNumberOfCommitsFrom(fromDate)).thenReturn(numberOfCommits);
		
		GitRepository repository = new GitRepository(project);
		assertEquals(numberOfCommits, repository.getNumberOfCommits());
	}
	
	@Test
	public void testListCommits() throws Exception {
		RevCommit commit = mock(RevCommit.class);
		
		List<RevCommit> commits = new ArrayList<>();
		commits.add(commit);
		
		when(serviceGitCommit.listCommitsFrom(fromDate)).thenReturn(commits);
		
		GitRepository repository = new GitRepository(project);
		
		List<GitCommit> gitCommits = new ArrayList<>();
		gitCommits.add(new GitCommit(project, commit));
		
		assertEquals(gitCommits, repository.listCommits());
	}
	
	@Test
	public void testFindCommitById() throws NoHeadException, GitAPIException, IOException {
		String commitId = "19031cc731bbc2aabf94b1db81030cf349a2b63c";
		RevCommit commit = mock(RevCommit.class);
		
		when(serviceGitCommit.findCommitBy(commitId)).thenReturn(commit);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		GitRepository repository = new GitRepository(project);
		
		assertEquals(gitCommit, repository.findCommitBy(commitId));
	}
}