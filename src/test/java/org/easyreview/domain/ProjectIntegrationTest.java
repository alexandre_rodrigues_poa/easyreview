package org.easyreview.domain;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.easyreview.EasyreviewApplication;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ProjectIntegrationTest {

	@PersistenceContext
	private EntityManager entityManager;
	
	private File repositoryPath;
	private RepositoryType repositoryType;
	private Email email;
	private String name;
	private Password password;
	private boolean isAdmin;
	private Set<SystemUser> users;
	private SystemUser user;
	private LocalDate commitsFromDate;
	
	@Before
	public void setUp() throws Exception {
		repositoryType = RepositoryType.SVN;
		repositoryPath = new File("/");
		email = Email.newInstance("test@project.com");
		name = "Test Project";
		password = Password.newInstance("password");
		isAdmin = true;
		user = SystemUser.newInstance(name, email, password, isAdmin);

		users = new HashSet<>();
		users.add(user);
		
		commitsFromDate = LocalDate.now();
	}

	@Test @Transactional
	public void testPersist() {
		
		File repositoryPath = new File("/");

		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertTrue(project.getId() > 0);
	}

	@Test @Transactional
	public void testPersistUsersToPersitProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		
		entityManager.flush();
		entityManager.clear();
		
		project = entityManager.find(Project.class, project.getId());
		assertFalse(project.getUsers().isEmpty());
	}
}
