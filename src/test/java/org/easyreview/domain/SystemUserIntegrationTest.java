package org.easyreview.domain;

import static org.junit.Assert.*;

import org.easyreview.EasyreviewApplication;
import org.easyreview.domain.Email;
import org.easyreview.domain.Password;
import org.easyreview.domain.SystemUser;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class SystemUserIntegrationTest {
	private Password password = Password.newInstance("password");
	private Email email = Email.newInstance("user@domain.com");
	private String userName = "User Name";
	private boolean isAdmin = false;
	
	
	@Test @Transactional
	public void testPersit() {
		SystemUser systemUser = SystemUser.newInstance(userName, email, password, isAdmin);
		assertNotNull(systemUser.getId());
	}
	
	@Test @Transactional
	public void testFindUserById() {
		assertNull(SystemUser.findBy(0L));
		
		SystemUser systemUser = SystemUser.newInstance(userName, email, password, isAdmin);
		Long id = systemUser.getId();
		
		assertNotNull(SystemUser.findBy(id));
	}
}
