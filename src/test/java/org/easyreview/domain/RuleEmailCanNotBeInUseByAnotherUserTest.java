package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class RuleEmailCanNotBeInUseByAnotherUserTest {
	
	@Mock private Email email;
	private final long userId = 2L; 

	@Test
	public void testRuleWithEmailNotInUse() {
		when(email.inUse()).thenReturn(false);
		
		RuleEmailCanNotBeInUseByAnotherUser rule = new RuleEmailCanNotBeInUseByAnotherUser(email, userId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleWithEmailInUseByInformedUser() {
		when(email.inUse()).thenReturn(true);
		when(email.isOfTheUserWith(userId)).thenReturn(true);
		
		RuleEmailCanNotBeInUseByAnotherUser rule = new RuleEmailCanNotBeInUseByAnotherUser(email, userId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleNotInAccord() {
		when(email.inUse()).thenReturn(true);
		when(email.isOfTheUserWith(userId)).thenReturn(false);
		
		RuleEmailCanNotBeInUseByAnotherUser rule = new RuleEmailCanNotBeInUseByAnotherUser(email, userId);
		assertTrue(rule.notInAccord());
		assertFalse(rule.inAccord());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testEmailCanNotBeNull() {
		new RuleEmailCanNotBeInUseByAnotherUser(null, userId);
	}
}
