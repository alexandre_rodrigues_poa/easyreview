package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.EntityManager;

import org.easyreview.factorys.ContainerFactory;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class, LocalDateTime.class, Review.class})
public class ReviewTest {
	
	@Mock private EntityManager entityManager;
	@Mock private SystemUser author;
	@Mock private Project project;
	@Mock private GitCommit commit;
	
	private final File file = new File("/test/review/file.test");
	private final String comment = "Review text!";
	private final CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.CAUTION;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		PowerMockito.mockStatic(ContainerFactory.class);
		when(ContainerFactory.get(EntityManager.class)).thenReturn(entityManager);
		
		when(author.getEmail()).thenReturn(Email.newInstance("author@easyreview.org"));
		when(commit.getCommitterEmail()).thenReturn(Email.newInstance("committer@easyreview.org"));
	}

	@Test
	public void testPersist() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		verify(entityManager, times(1)).persist(review);
	}

	@Test
	public void testGetAuthor() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(author, review.getAuthor());
	}

	@Test
	public void testGetProject() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(project, review.getProject());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testProjectCanNotBeNull() {
		Review review = Review.newInstance(author, null, commit, file, comment, analysisStatus); 
		assertEquals(project, review.getProject());
	}
	
	@Test
	public void testGetCommitId() {
		String commitId = "aaad445b902fe4f537138fb94b4af2ae30e84607";
		when(commit.getId()).thenReturn(commitId );
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(commitId, review.getCommitId());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testAuthorOfTheCommitCanNotBeTheAuthorOfTheReview() {
		Email email = Email.newInstance("author@easyreview.org");
		
		when(author.getEmail()).thenReturn(email);
		when(commit.getCommitterEmail()).thenReturn(email);
		
		Review.newInstance(author, project, commit, file, comment, analysisStatus);
	}

	@Test
	public void testGetFile() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(file, review.getFile());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFileParameterCanNobBeNull() {
		Review.newInstance(author, project, commit, null, comment, analysisStatus); 
	}

	@Test
	public void testGetDateTime() {
		LocalDateTime dateTime = LocalDateTime.now();
		
		PowerMockito.mockStatic(LocalDateTime.class);
		when(LocalDateTime.now()).thenReturn(dateTime);
		
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(dateTime, review.getDateTime());
	}
	
	@Test
	public void testGetComment() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		assertEquals(comment, review.getComment());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParameterTextCanNotBeBlank() {
		Review.newInstance(author, project, commit, file, " ", analysisStatus); 
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParameterTextCanNotBeNull() {
		Review.newInstance(author, project, commit, file, null, analysisStatus); 
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testParameterAnalysisStatusCanNotBeNull() {
		Review.newInstance(author, project, commit, file, comment, null); 
	}
	
	@Test
	public void testSetText() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		
		String newText = "New text review";
		review.setComment(newText,  author);
		
		assertEquals(newText, review.getComment());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testParameterTextCanNotBeBlankOnSetText() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		review.setComment(" ", author);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testParameterTextCanNotBeNullOnSetText() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		review.setComment(null, author);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOnlyTheAuthorCanModifyTheText() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 

		SystemUser newAuthor = mock(SystemUser.class); 

		String newText = "New text review";
		review.setComment(newText, newAuthor);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOnlyTheOriginalAuthorCanModifyTheComment() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 

		String newText = "New text review";
		review.setComment(newText, null);
	}
	
	@Test
	public void testGetAnalysisStatus() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);		
		assertEquals(analysisStatus, review.getAnalysisStatus());
	}

	@Test
	public void testSetAnalysisStatus() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		
		CodeAnalysisStatus newAnalysisStatus = CodeAnalysisStatus.OK;
		review.setAnalysisStatus(newAnalysisStatus, author);
		
		assertEquals(newAnalysisStatus, review.getAnalysisStatus());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testOnlyTheOrinalAuthorCanModifyTheAnalysisStatus() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 

		SystemUser newAuthor = mock(SystemUser.class); 
		
		CodeAnalysisStatus newAnalysisStatus = CodeAnalysisStatus.ERROR;

		review.setAnalysisStatus(newAnalysisStatus, newAuthor);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testParameterAnalysisStatusCanNotBeNullOnSetAnalysisStatus() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		review.setAnalysisStatus(null, author);
	}

	@Test
	public void testEditedReviewOnSetText() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		Whitebox.setInternalState(review, "id", 1L);
		
		assertFalse(review.edited());
		
		String newText = "New text review";
		review.setComment(newText, author);
		
		assertTrue(review.edited());
	}

	@Test
	public void testEditedReviewOnSetAnalysisStatus() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		Whitebox.setInternalState(review, "id", 1L);
		
		assertFalse(review.edited());
		
		CodeAnalysisStatus newAnalysisStatus = CodeAnalysisStatus.OK;
		
		review.setAnalysisStatus(newAnalysisStatus, author);
		
		assertTrue(review.edited());
	}
	
	@Test
	public void testLastEdition() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus); 
		Whitebox.setInternalState(review, "id", 1L);

		LocalDateTime dateTime = LocalDateTime.now();
		PowerMockito.mockStatic(LocalDateTime.class);
		when(LocalDateTime.now()).thenReturn(dateTime);

		String newText = "New text review";
		review.setComment(newText, author);
		
		assertEquals(dateTime, review.lastEdition());
	}

	@Test
	public void testAddReply() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		
		List<ReplyReview> replies = review.listReplies();
		assertTrue(replies.isEmpty());
		
		ReplyReview reply = ReplyReview.newInstance(author, comment);
		review.addIfNotExist(reply);
		
		replies = review.listReplies();
		assertFalse(replies.isEmpty());
		assertTrue(replies.contains(reply));
	}
	
	@Test
	public void testAddReplyIfNotExist() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		ReplyReview reply = ReplyReview.newInstance(author, comment);

		review.addIfNotExist(reply);
		review.addIfNotExist(reply);
		
		List<ReplyReview> replies = review.listReplies();
		assertEquals(1, replies.size());
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testCanNotModifiyRepliesList() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		List<ReplyReview> replies = review.listReplies();
		replies.clear();
	}

	@Test
	public void testReplaceReply() {
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		ReplyReview reply = ReplyReview.newInstance(author, comment);
		review.addIfNotExist(reply);
		
		String text = "New reply";
		reply.setText(text, author);
		
		review.addIfNotExist(reply);
		
		List<ReplyReview> replies = review.listReplies();
		reply = replies.get(0);
		
		assertEquals(text, reply.getText());
	}

	@Test
	public void testFindReviewById() {
		Long id = 127L;
		
		Review review = Review.newInstance(author, project, commit, file, comment, analysisStatus);
		Whitebox.setInternalState(review, "id", id);
		
		when(entityManager.find(Review.class, id)).thenReturn(review);
		
		assertEquals(review, Review.findBy(id));
	}
}