package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.easyreview.EasyreviewApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class ProjectRepositoryTest {

	private File repositoryPath = new File("/");
	private String projectName = "Test Project"; 
	private Set<SystemUser> users = new HashSet<>();
	private LocalDate commitsFromDate = LocalDate.now();
	
	@Autowired
	private ProjectRepository repository;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Test @Transactional
	public void testNameInUse() {
		assertFalse(repository.nameInUse(projectName));

		Project project = Project.newInstance(projectName, repositoryPath, SVN, users, commitsFromDate);
		assertTrue(repository.nameInUse(projectName));
		
		project.remove();
		assertFalse(repository.nameInUse(projectName));
	}

	@Test @Transactional
	public void testFindActiveProjectIdByProjectName() {
		Project project = Project.newInstance(projectName, repositoryPath, SVN, users, commitsFromDate);
		
		Long id = repository.findActiveProjectIdBy(projectName);
		assertEquals(project.getId(), id);
		
		project.remove();
		assertNull(repository.findActiveProjectIdBy(projectName));
	}

	@Test @Transactional
	public void testRepositoryPathInUseName() {
		assertFalse(repository.repositoryPathInUse(repositoryPath));
		
		Project project = Project.newInstance(projectName, repositoryPath, SVN, users, commitsFromDate);
		assertTrue(repository.repositoryPathInUse(repositoryPath));
		
		project.remove();
		assertFalse(repository.repositoryPathInUse(repositoryPath));
	}

	@Test @Transactional
	public void testFindActiveProjectIdByRepositoryPath() {
		Project project = Project.newInstance(projectName, repositoryPath, SVN, users, commitsFromDate);
		
		Long id = repository.findActiveProjectIdBy(repositoryPath);
		assertEquals(project.getId(), id);
		
		project.remove();
		assertNull(repository.findActiveProjectIdBy(repositoryPath));
	}

	@Test @Transactional
	public void testListAllActiveProjects() {
		Project project01 = Project.newInstance("Project 01", new File("/Repository01"), SVN, users, commitsFromDate);
		Project project02 = Project.newInstance("Project 02", new File("/Repository02"), GIT, users, commitsFromDate);
		
		entityManager.clear();
		
		List<Project> projects = repository.listAllActiveProjects();
		assertEquals(2, projects.size());
		assertTrue(projects.contains(project01));
		assertTrue(projects.contains(project02));
		
		project02 = entityManager.merge(project02);
		project02.remove();
		
		entityManager.flush();
		entityManager.clear();
		
		projects = repository.listAllActiveProjects();
		System.out.println(projects);
		
		assertEquals(1, projects.size());
		assertFalse(projects.contains(project02));
	}

	@Test @Transactional
	public void testFindProjectByProjectById() {
		Project project = Project.newInstance(projectName, repositoryPath, SVN, users, commitsFromDate);
		entityManager.clear();
		
		Long id = project.getId();
		project = repository.findProjectBy(id);
		
		assertNotNull(project);
	}
	
	@Test @Transactional
	public void testGetRepositoryPathByProjectId() {
		Project project = Project.newInstance(projectName, repositoryPath, GIT, users, commitsFromDate);
		entityManager.clear();
		
		Long projectId = project.getId();
		assertEquals(repositoryPath, repository.getRepositoryPathBy(projectId));
	}
	
	@Test @Transactional
	public void testSetProjectUsers() {
		String name = "User Name";
		Email email =  Email.newInstance("testFindProjectbyUser@email.org");
		Password password = Password.newInstance("password");
		boolean isAdmin = false;
		
		SystemUser user = SystemUser.newInstance(name, email, password, isAdmin);
		users.add(user);
		
		Project project01 = Project.newInstance("Project 01", new File("/repository/project01"), SVN, users, commitsFromDate);
		Project project02 = Project.newInstance("Project 02", new File("/repository/project02"), GIT, users, commitsFromDate);
		
		entityManager.flush();
		entityManager.clear();
		
		user = SystemUser.findBy(user.getId());
		System.out.println(user.getProjects());
		
		Set<Project> projects = user.getProjects();
		assertEquals(2, projects.size());
		
		assertTrue(projects.contains(project01));
		assertTrue(projects.contains(project02));

	}
	@Test @Transactional
	public void testListAllActiveUserProjects() {
		SystemUser user = SystemUser.newInstance("User Name", Email.newInstance("listAllActiveUserProjects@eazyreviw.org"), Password.newInstance("password"), false);
		
		users.clear();
		Project.newInstance("Project 01", new File("/Repository01"), GIT, users, commitsFromDate);
		
		users.add(user);
		Project project = Project.newInstance("Project 02", new File("/Repository02"), SVN, users, commitsFromDate);
		
		entityManager.flush();
		entityManager.clear();
		
		List<Project> projects = repository.listAllActiveUserProjects(user);
		assertEquals(1, projects.size());
		assertTrue(projects.contains(project));
		
		project = entityManager.merge(project);
		project.remove();
		
		entityManager.flush();
		entityManager.clear();
		
		projects = repository.listAllActiveUserProjects(user);
		System.out.println(projects);
		
		assertEquals(0, projects.size());
	}
}
