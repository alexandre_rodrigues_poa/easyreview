package org.easyreview.domain;

import static org.junit.Assert.*;

import org.easyreview.domain.Password;
import org.junit.Test;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public class PasswordTest {
	private String passwordValue = "123456";

	@Test
	public void testHash() {
		Password password = Password.newInstance(passwordValue);
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		assertTrue(encoder.matches(passwordValue, password.getHash()));
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testPasswordNull() {
		Password.newInstance(null); 
	}

	@Test(expected=IllegalArgumentException.class)
	public void testPasswordIsEmpty() {
		Password.newInstance(" "); 
	}
}
