package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.*;

import javax.persistence.EntityManager;

import org.easyreview.domain.Email;
import org.easyreview.domain.Password;
import org.easyreview.domain.SystemUser;
import org.easyreview.domain.SystemUserRepository;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class, SystemUser.class})
public class SystemUserTest {

		private final Email email = Email.newInstance("user@domain.com");
		private final Password password = Password.newInstance("password");
		private final String userName = "User Name";
		private final boolean isAdmin = true;

		@Mock private SystemUserRepository repository;
		@Mock private EntityManager entityManager;
		@Mock private RuleEmailCanNotBeInUseByAnotherUser rule;

		@Before
		public void setUp() throws Exception {
			MockitoAnnotations.initMocks(this);

			PowerMockito.mockStatic(ContainerFactory.class);
			PowerMockito.when(ContainerFactory.get(SystemUserRepository.class)).thenReturn(repository);

			PowerMockito.mockStatic(EntityManager.class);
			PowerMockito.when(ContainerFactory.get(EntityManager.class)).thenReturn(entityManager);

			PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(email, 0L).thenReturn(rule);
			when(rule.notInAccord()).thenReturn(false);
		}

		@Test
		public void testPersist() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			verify(entityManager, times(1)).persist(user);
		}

		@Test
		public void testListAll() {
			when(repository.listAllActive()).thenReturn(new ArrayList<SystemUser>());

			List<SystemUser> allUsers = SystemUser.listAllActiveOrderedByName();
			assertNotNull(allUsers);
			verify(repository, times(1)).listAllActive();
		}

		@Test
		public void testFindById() {
			SystemUser userMock = mock(SystemUser.class);
			Long userId = 2L;

			when(repository.findUserBy(userId)).thenReturn(userMock);

			SystemUser user = SystemUser.findBy(userId);
			assertNotNull(user);

			verify(repository, times(1)).findUserBy(userId);
		}

		@Test
		public void testGetUserName() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertEquals(userName, user.getName());
		}

		@Test(expected=IllegalArgumentException.class)
		public void testGetUserNameCanNotBeNull() {
			SystemUser.newInstance(null, email, password, isAdmin);
		}

		@Test(expected=IllegalArgumentException.class)
		public void testUserNameCanNotBeEmpty() {
			SystemUser.newInstance(" ", email, password, isAdmin);
		}

		@Test(expected=IllegalStateException.class)
		public void testCanNotEditNameInARemovedUser() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			user.remove();
			user.setName(userName);
		}

		@Test
		public void testGetEmail() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertEquals(email, user.getEmail());
		}

		@Test(expected=IllegalArgumentException.class)
		public void testEmailCanNotBeNull() {
			SystemUser.newInstance(userName, null, password, isAdmin);
		}

		@Test(expected=IllegalArgumentException.class)
		public void testCanNotInsertWhenEmailInUse() throws Exception {
			Email emailInUse = Email.newInstance("emailInUse@domain.com");

			RuleEmailCanNotBeInUseByAnotherUser ruleMock = mock(RuleEmailCanNotBeInUseByAnotherUser.class);
			PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(emailInUse, 0L).thenReturn(ruleMock);
			when(ruleMock.notInAccord()).thenReturn(true);

			SystemUser.newInstance(userName, emailInUse, password, isAdmin);
		}

		@Test
		public void testSetSameEmail() throws Exception {
			SystemUser systemUser = SystemUser.newInstance(userName, email, password, isAdmin);
			Email sameEmail = Email.newInstance(email.getAddress());
			systemUser.setEmail(sameEmail);
		}

		@Test(expected=IllegalArgumentException.class)
		public void testCanNotSetEmailInUse() throws Exception {
			Email sameEmail = Email.newInstance("emailInUse@domainemail.com");

			RuleEmailCanNotBeInUseByAnotherUser rule  = mock(RuleEmailCanNotBeInUseByAnotherUser.class);
			PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(sameEmail, 0L).thenReturn(rule);
			when(rule.notInAccord()).thenReturn(true);

			SystemUser systemUser = SystemUser.newInstance(userName, email, password, isAdmin);
			systemUser.setEmail(sameEmail);
		}

		@Test(expected=IllegalStateException.class)
		public void testCanNotEditEmailInARemovedUser() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			user.remove();
			user.setEmail(email);
		}

		@Test
		public void testGetEmailAddress() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertEquals(email.getAddress(), user.getEmailAddress());
		}

		@Test
		public void testGetPassword() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertEquals(password, user.getPassword());
		}

		@Test(expected=IllegalArgumentException.class)
		public void testPasswordCanNotBeNull() {
			SystemUser.newInstance(userName, email, null, isAdmin);
		}

		@Test
		public void testRemoveUser() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			user.remove();
			assertTrue(user.removed());
		}

		@Test(expected=IllegalStateException.class)
		public void testCanNotEditPasswordInARemovedUser() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			user.remove();
			user.setPassword(password);
		}

		@Test
		public void testGetPasswordHash() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertEquals(password.getHash(), user.getPasswordHash());
		}

		@Test
		public void testIsAdmin() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			assertTrue(user.isAdmin());
		}

		@Test(expected=IllegalStateException.class)
		public void testCanNotEditIsAdminInARemovedUser() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			user.remove();
			user.setAdmin(false);
		}

		@Test(expected=UnsupportedOperationException.class)
		public void testCanNotEditProjects() {
			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			Set<Project> projects = user.getProjects();
			projects.clear();
		}

		@Test
		public void testRemovingTheUserMustRemoveProjects() {
			Set<Project> projects = new HashSet<>();
			projects.add(mock(Project.class));

			SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
			Whitebox.setInternalState(user, "projects", projects);

			user.remove();
			assertTrue(projects.isEmpty());
		}
}
