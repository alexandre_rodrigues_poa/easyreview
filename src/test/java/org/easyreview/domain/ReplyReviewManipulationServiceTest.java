package org.easyreview.domain;

import static org.mockito.Mockito.*;

import java.lang.reflect.Method;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Review.class, SystemUser.class, ReplyReview.class})
public class ReplyReviewManipulationServiceTest {

	@Mock 
	private SystemUser author;
	@Mock 
	private Review review;
	@Mock
	private Project project;
	@Mock
	private NotificationService notify;
	@Mock
	private ReviewRepository reviewRepository;
	
	private ReplyReviewManipulationService service = new ReplyReviewManipulationService();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Whitebox.setInternalState(service, "notify", notify);
		Whitebox.setInternalState(service, "reviewRepository", reviewRepository);
		
		when(review.getProject()).thenReturn(project);
	}

	@Test
	public void testInsertNewReply() throws Exception {
		String text = "Reply text";
		Long userId = 20L;
		Long reviewId = 10L;
		String accessUrl = "http://localhost:8080/test/insert";
		
		PowerMockito.mockStatic(Review.class);
		when(Review.findBy(reviewId)).thenReturn(review);

		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId)).thenReturn(author);
	
		ReplyReview reply = mock(ReplyReview.class);
		
		PowerMockito.mockStatic(ReplyReview.class);
		when(ReplyReview.newInstance(author, text)).thenReturn(reply);
		
		
		ReplyReview insertedReply = service.insertNewReply(userId, reviewId, text, accessUrl);
		
		assertEquals(reply, insertedReply);
		
		verify(review, times(1)).addIfNotExist(insertedReply);
		verify(notify, times(1)).addedNewReply(project, accessUrl);
	}

	@Test
	public void testMethodInsertNewReplyHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ReplyReviewManipulationService> objectClass = ReplyReviewManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("insertNewReply", Long.class, Long.class, String.class, String.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testEditReply() throws Exception {
		String text = "Reply text";
		Long userId = 12L;
		Long id = 10L;
		String accessUrl = "http://localhost:8080/test/edit";
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId)).thenReturn(author);

		ReplyReview reply = mock(ReplyReview.class);

		PowerMockito.mockStatic(ReplyReview.class);
		when(ReplyReview.findBy(id)).thenReturn(reply);
		
		when(reviewRepository.findReviewBy(reply)).thenReturn(review);
		when(review.getProject()).thenReturn(project);
		
		ReplyReview editedReply = service.editReply(userId, id, text, accessUrl);
		assertEquals(reply, editedReply);
		
		verify(editedReply, times(1)).setText(text, author);
		verify(notify, times(1)).editedReply(project, accessUrl);
	}

	@Test
	public void testMethodEditReviewHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ReplyReviewManipulationService> objectClass = ReplyReviewManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("editReply", Long.class, Long.class, String.class, String.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}
}
