package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;

import org.easyreview.factorys.ContainerFactory;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;


@RunWith(PowerMockRunner.class)
@PrepareForTest(ContainerFactory.class)
public class RuleProjectRepositoryPathCanNotBeInUseByAnotherProjectTest {
	
	private final long projectId = 432L;
	@Mock private ProjectRepository repository;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
	}
	
	@Test
	public void testRuleWithRepositoryPathNotInUse() {
		File repositoryPath = new File("/repository");
		when(repository.repositoryPathInUse(repositoryPath)).thenReturn(false);
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = new RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(repositoryPath, projectId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleWithRepositoryPathInUseByInformedProject() {
		File repositoryPath = new File("/repository");
		
		when(repository.repositoryPathInUse(repositoryPath)).thenReturn(true);
		when(repository.findActiveProjectIdBy(repositoryPath)).thenReturn(projectId);
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = new RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(repositoryPath, projectId);
		assertTrue(rule.inAccord());
	}

	@Test
	public void testRuleNotInAccord() {
		File repositoryPath = new File("/repository");
		
		when(repository.repositoryPathInUse(repositoryPath)).thenReturn(true);
		when(repository.findActiveProjectIdBy(repositoryPath)).thenReturn(234L);
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = new RuleProjectRepositoryPathCanNotBeInUseByAnotherProject(repositoryPath, projectId);
		assertFalse(rule.inAccord());
		assertTrue(rule.notInAccord());
	}
}
