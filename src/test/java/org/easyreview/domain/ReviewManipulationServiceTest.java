package org.easyreview.domain;

import static org.mockito.Mockito.*;

import java.io.File;
import java.lang.reflect.Method;

import javax.transaction.Transactional;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Project.class, SystemUser.class, Review.class, ReviewManipulationService.class})
public class ReviewManipulationServiceTest {

	@Mock 
	private SystemUser author;
	@Mock
	private Project project;
	@Mock
	private GitRepository gitRepository;
	@Mock
	private GitCommit gitCommit;
	@Mock
	private NotificationService notify;
	
	private ReviewManipulationService service = new ReviewManipulationService();
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		Whitebox.setInternalState(service, "notify", notify);
	}

	@Test
	public void testInsertNewReview() throws Exception {
		Long userId = 20L;
		Long projectId = 10L;
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		String file = "/test/add/review.test";
		CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.OK;
		String comment = "Test comment";
		String url = "http://localhost:8080";
		
		PowerMockito.mockStatic(Project.class);
		when(Project.findBy(projectId)).thenReturn(project);
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId)).thenReturn(author);
		
		PowerMockito.whenNew(GitRepository.class).withArguments(project).thenReturn(gitRepository);
		when(gitRepository.findCommitBy(commitId)).thenReturn(gitCommit);
		
		Review review = mock(Review.class);

		PowerMockito.mockStatic(Review.class);
		when(Review.newInstance(author, project, gitCommit, new File(file), comment, analysisStatus)).thenReturn(review);
		
		Review insertedReview = service.insertNewReview(userId, projectId, commitId, file, analysisStatus, comment, url);
		assertEquals(review, insertedReview);
		
		verify(notify, times(1)).addedNewReview(project, url);
	}

	@Test
	public void testMethodInsertNewReviewHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ReviewManipulationService> objectClass = ReviewManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("insertNewReview", Long.class, Long.class, String.class, String.class, CodeAnalysisStatus.class, String.class, String.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}

	@Test
	public void testEditReview() throws Exception {
		Long userId = 20L;
		Long reviewId = 5L;
		String url = "http://localhost:8080";
		
		PowerMockito.mockStatic(SystemUser.class);
		when(SystemUser.findBy(userId)).thenReturn(author);

		CodeAnalysisStatus analysisStatus = CodeAnalysisStatus.CAUTION;
		String comment = "Test comment edited";
		
		Review review = mock(Review.class);
				
		PowerMockito.mockStatic(Review.class);
		when(Review.findBy(reviewId)).thenReturn(review);
		
		when(review.getProject()).thenReturn(project);
		
		Review editedReview = service.editReview(userId, reviewId, analysisStatus, comment, url);
		
		verify(editedReview, times(1)).setAnalysisStatus(analysisStatus, author);
		verify(editedReview, times(1)).setComment(comment, author);
		verify(notify, times(1)).editedReview(project, url);
	}

	@Test
	public void testMethodEditReviewHasTransactionAnnotation() throws NoSuchMethodException {
		Class<ReviewManipulationService> objectClass = ReviewManipulationService.class;
		Method method =  objectClass.getDeclaredMethod("editReview", Long.class, Long.class, CodeAnalysisStatus.class, String.class, String.class);
		assertNotNull(method.getAnnotation(Transactional.class));
	}
}
