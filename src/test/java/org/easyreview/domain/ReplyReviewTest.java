package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;

import javax.persistence.EntityManager;

import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.util.reflection.Whitebox;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class, LocalDateTime.class, ReplyReview.class})
public class ReplyReviewTest {

	@Mock private SystemUser author;
	private final String text = "Reply review test";

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testGetAuthor() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		assertEquals(author, reply.getAuthor());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testAuthorParameterCanNotBeNull() {
		ReplyReview.newInstance(null, text);
	}
	
	@Test
	public void testGetText() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		assertEquals(text, reply.getText());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTextParameterCanNotBeBlank() {
		ReplyReview.newInstance(author, " ");
	}

	@Test
	public void testGetDateTime() {
		LocalDateTime dateTime = LocalDateTime.now();
		
		PowerMockito.mockStatic(LocalDateTime.class);
		when(LocalDateTime.now()).thenReturn(dateTime);
		
		ReplyReview reply = ReplyReview.newInstance(author, text);
		assertEquals(dateTime, reply.getDateTime());
	}

	@Test
	public void testSetText() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		
		String newText = "New text review";
		reply.setText(newText, author);
		
		assertEquals(newText, reply.getText());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testOnlyTheOriginalAuthorCanModifyTheText() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		SystemUser newAuhtor = mock(SystemUser.class);

		String newText = "New text review";
		reply.setText(newText, newAuhtor);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTextParameterCanNotBeBlankOnSetText() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		reply.setText(" ", author);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTextParameterCanNotBeNullOnSetText() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		reply.setText(null, author);
	}

	@Test
	public void testLastEdition() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		
		LocalDateTime lastEdition = LocalDateTime.now();
		
		PowerMockito.mockStatic(LocalDateTime.class);
		when(LocalDateTime.now()).thenReturn(lastEdition);
		
		reply.setText("Last edition text", author);
		assertEquals(lastEdition, reply.getLastEdition());
	}

	@Test
	public void testEdited() {
		ReplyReview reply = ReplyReview.newInstance(author, text);
		assertFalse(reply.edited());
		reply.setText("Edited text", author);
		assertTrue(reply.edited());
	}
	
	@Test
	public void testFind() {
		Long id = 19L;
		ReplyReview reply = ReplyReview.newInstance(author, text);
		
		Whitebox.setInternalState(reply, "id", id);
		
		EntityManager entityManager = mock(EntityManager.class);

		PowerMockito.mockStatic(ContainerFactory.class);
		when(ContainerFactory.get(EntityManager.class)).thenReturn(entityManager);
		when(entityManager.find(ReplyReview.class, id)).thenReturn(reply);

		assertEquals(reply, ReplyReview.findBy(id));
	}
}
