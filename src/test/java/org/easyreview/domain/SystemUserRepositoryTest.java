package org.easyreview.domain;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.easyreview.EasyreviewApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes=EasyreviewApplication.class)
@TestPropertySource("/test.properties")
public class SystemUserRepositoryTest {

	private Password password = Password.newInstance("password");
	private Email email = Email.newInstance("user@domain.com");
	private String userName = "User Name";
	private boolean isAdmin = false;

	@Autowired
	private SystemUserRepository repository;

	@PersistenceContext
	private EntityManager entityManager;
	
	@Test @Transactional
	public void testEmailInUse() {
		SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
		assertTrue(repository.emailInUse(email));
		
		user.remove();
		
		assertFalse(repository.emailInUse(email));
	}

	@Test @Transactional
	public void testListAllActive() {
		List<SystemUser> users = repository.listAllActive();
		assertTrue(users.isEmpty());
		
		SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
		
		users = repository.listAllActive();
		assertFalse(users.isEmpty());
		
		user.remove();
		
		users = repository.listAllActive();
		assertTrue(users.isEmpty());
	}
	
	@Test @Transactional
	public void testfindIdFromActiveUserByEmail() {
		SystemUser user = SystemUser.newInstance(userName, email, password, isAdmin);
		
		Long id = repository.findIdUserBy(email);
		assertEquals("User ID", user.getId(), id);
		
		user.remove();
		
		id = repository.findIdUserBy(email);
		assertNull(id);
	}

	@Test @Transactional
	public void testFindActiveUserById() {
		SystemUser userExpected = SystemUser.newInstance(userName, email, password, isAdmin);
		
		entityManager.clear();
		
		Stri id = userExpected.getId();
		
		SystemUser userActual = repository.findUserBy(id);
		
		assertEquals(userExpected, userActual);
	}

	@Test @Transactional
	public void testFindUserByEmail() {
		SystemUser userExpected = SystemUser.newInstance(userName, email, password, isAdmin);
		
		entityManager.clear();
		
		SystemUser userActual = repository.findUserBy(email);
		
		assertEquals(userExpected, userActual);
		
		userActual.remove();
		entityManager.flush();
		entityManager.clear();
		
		assertNull(repository.findUserBy(email));
	}

	@Test @Transactional
	public void testHasAdminUser() {
		SystemUser.newInstance(userName, email, password, false);
		assertFalse(repository.hasAdminUser());
		
		email = Email.newInstance("admin@test.net");
		SystemUser user = SystemUser.newInstance(userName, email, password, true);
		
		assertTrue(repository.hasAdminUser());
		
		user.remove();
		
		assertFalse(repository.hasAdminUser());
	}
}
