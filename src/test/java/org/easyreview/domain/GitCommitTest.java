package org.easyreview.domain;

import static org.junit.Assert.*;

import java.io.File;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.*;

import org.easyreview.repositories.git.GitCommitedFiles;
import org.easyreview.repositories.git.ModifiedFile;
import org.easyreview.repositories.git.RepositoryFactory;
import org.easyreview.repositories.git.ServiceGitCommit;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.PersonIdent;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.powermock.api.mockito.PowerMockito.*;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames = {"org.eclipse.jgit.revwalk.*", "org.easyreview.domain.*", "org.easyreview.repositories.git.*"})
public class GitCommitTest {

	private Project project;
	
	private RevCommit commit;
	private PersonIdent person;
	
	@Before
	public void setUp() {
		project = mock(Project.class);
		commit = mock(RevCommit.class);
		person = mock(PersonIdent.class);
	}

	@Test
	public void testGetProjectId() {
		Long projectId = 403L;
		when(project.getId()).thenReturn(projectId);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertEquals(projectId, gitCommit.getProjectId());
	}

	@Test
	public void testGetMessage() {
		String message = "Commit message";
		when(commit.getShortMessage()).thenReturn(message);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertEquals(message, gitCommit.getMessage());
	}

	@Test
	public void testGetDateTime() {
		Date date = new Date();
		TimeZone timeZone = TimeZone.getDefault();
		
		LocalDateTime dateTime = date.toInstant().atZone(timeZone.toZoneId()).toLocalDateTime();

		when(commit.getCommitterIdent()).thenReturn(person);
		when(person.getWhen()).thenReturn(date);
		when(person.getTimeZone()).thenReturn(timeZone);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		
		assertEquals(dateTime, gitCommit.getDateTime());
	}

	@Test
	public void testGetDateTimeWhenTimeZoneIsNull() {
		Date date = new Date();
		TimeZone timeZone = null;
		
		LocalDateTime dateTime = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();

		when(commit.getCommitterIdent()).thenReturn(person);
		when(person.getWhen()).thenReturn(date);
		when(person.getTimeZone()).thenReturn(timeZone);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		
		assertEquals(dateTime, gitCommit.getDateTime());
	}

	@Test
	public void testGetCommitterEmail() {
		Email email = Email.newInstance("committer@git.com");
		
		when(commit.getCommitterIdent()).thenReturn(person);
		when(person.getEmailAddress()).thenReturn(email.getAddress());
		
		GitCommit gitCommit = new GitCommit(project, commit);
		
		assertEquals(email, gitCommit.getCommitterEmail());
	}

	@Test
	public void testGetCommitterName() {
		String name = "Committer Name";
		
		when(commit.getCommitterIdent()).thenReturn(person);
		when(person.getName()).thenReturn(name);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		
		assertEquals(name, gitCommit.getCommitterName());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTheParameterRepositoryPathCanNotBeNull() {
		new GitCommit(null, commit);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheParameterCommitCanNotBeNull() {
		new GitCommit(project, null);
	}

	@Test
	public void testListFiles() throws Exception {
		File repositoryPath = new File("/test/repository/files");
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		
		Repository repository = mock(Repository.class);
		PowerMockito.mockStatic(RepositoryFactory.class);
		when(RepositoryFactory.createFrom(repositoryPath)).thenReturn(repository);
		
		GitCommitedFiles commitedFiles = mock(GitCommitedFiles.class);
		PowerMockito.whenNew(GitCommitedFiles.class).withArguments(repository, commit).thenReturn(commitedFiles);

		when(commitedFiles.list()).thenReturn(new ArrayList<>());
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertNotNull(gitCommit.listFiles());
	}
	
	@Test
	public void testReadFile() throws Exception {
		File repositoryPath = new File("/test/repository/file");
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		
		Repository repository = mock(Repository.class);
		PowerMockito.mockStatic(RepositoryFactory.class);
		when(RepositoryFactory.createFrom(repositoryPath)).thenReturn(repository);
		
		ServiceGitCommit service = mock(ServiceGitCommit.class);
		PowerMockito.whenNew(ServiceGitCommit.class).withArguments(repository).thenReturn(service);

		String filePath = "/repository/test";
		String file = "File Test"; 
		when(service.readFileFromCommit(commit, filePath)).thenReturn(file);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertEquals(file, gitCommit.readFile(filePath));
	}

	@Test
	public void testReadModifiedFile() throws Exception {
		File repositoryPath = new File("/test/repository/modifiedFile");
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		
		Repository repository = mock(Repository.class);
		PowerMockito.mockStatic(RepositoryFactory.class);
		when(RepositoryFactory.createFrom(repositoryPath)).thenReturn(repository);
		
		String filePath = "/repository/test";
		ModifiedFile modifiedFile = mock(ModifiedFile.class);
		PowerMockito.whenNew(ModifiedFile.class).withArguments(repository, commit, filePath).thenReturn(modifiedFile);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertEquals(modifiedFile, gitCommit.readModifiedFile(filePath));
	}

	@Test
	public void testReadRemovedFile() throws Exception {
		File repositoryPath = new File("/test/repository/removed/");
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		
		Repository repository = mock(Repository.class);
		PowerMockito.mockStatic(RepositoryFactory.class);
		when(RepositoryFactory.createFrom(repositoryPath)).thenReturn(repository);
		
		ServiceGitCommit service = mock(ServiceGitCommit.class);
		PowerMockito.whenNew(ServiceGitCommit.class).withArguments(repository).thenReturn(service);

		RevCommit parentCommit = mock(RevCommit.class);
		when(commit.getParent(0)).thenReturn(parentCommit);
		
		ObjectId parentCommitId = mock(ObjectId.class);
		when(parentCommit.getId()).thenReturn(parentCommitId);
		
		RevCommit parent = mock(RevCommit.class);
		when(service.findCommitBy(parentCommitId)).thenReturn(parent);
		
		String filePath = "/repository/test";
		String file = "File Removed"; 
		
		when(service.readFileFromCommit(parent, filePath)).thenReturn(file);
		
		GitCommit gitCommit = new GitCommit(project, commit);
		assertEquals(file, gitCommit.readRemovedFile(filePath));
	}
}
