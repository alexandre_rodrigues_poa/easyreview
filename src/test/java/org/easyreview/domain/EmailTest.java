package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.Email;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class})
public class EmailTest {
	
	@Mock private SystemUserRepository repositoryMock;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(SystemUserRepository.class)).thenReturn(repositoryMock);
	}

	@Test
	public void testEmailAddress() {
		String emailAddress = "test@domain.com";
		Email email = Email.newInstance(emailAddress);
		assertEquals(emailAddress, email.getAddress());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testInvalidEmailAddress() {
		String emailAddress = "testdomain.com";
		Email.newInstance(emailAddress);
	}
	
	@Test
	public void testEmailInUse() {
		Email email = Email.newInstance("email@domain.com");
		when(repositoryMock.emailInUse(email)).thenReturn(true);
		assertTrue("e-mail not in use", email.inUse());
	}

	@Test
	public void testIsOfTheUserWithId() {
		Long id = 1l;
		
		Email email = Email.newInstance("email@domain.com");
		when(repositoryMock.findIdUserBy(email)).thenReturn(id);
		assertTrue("e-mail is not of the user.", email.isOfTheUserWith(id));
		
		when(repositoryMock.findIdUserBy(email)).thenReturn(null);
		assertFalse("e-mail is not of the user.", email.isOfTheUserWith(id));
	}

	@Test
	public void testIsOfTheUserWithIdNull() {
		Long id = null;
		
		Email email = Email.newInstance("email@domain.com");
		when(repositoryMock.findIdUserBy(email)).thenReturn(id);
		
		assertFalse("e-mail is not of the user.", email.isOfTheUserWith(id));
	}

	@Test
	public void testNotValidateEmailAddress() {
		String emailAddress = "test@tes-domain";
		Email.newInstance(emailAddress, false);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testNotValidateEmailAddressButEmailCanNotBlank() {
		String emailAddress = " ";
		Email.newInstance(emailAddress, false);
	}
}
