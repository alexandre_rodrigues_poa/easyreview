package org.easyreview.domain;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.easyreview.domain.RepositoryType.*;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.EntityManager;

import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;

import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class, Project.class, RepositoryType.class, ProjectRepository.class})
public class ProjectTest {

	@Mock private EntityManager entityManager;
	@Mock private RuleProjectNameCanNotBeInUseByAnotherProject ruleProjectNameCanNotBeInUseByAnotherProject;
	@Mock private RuleProjectRepositoryPathCanNotBeInUseByAnotherProject ruleProjectRepositoryPathCanNotBeInUseByAnotherProject;
	@Mock private SystemUser user;
	private LocalDate commitsFromDate = LocalDate.now(); 
	
	private Set<SystemUser> users = new HashSet<>();
	private String name = "Project Name";
	private File repositoryPath = new File("/test");
	private RepositoryType repositoryType = RepositoryType.SVN;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(EntityManager.class)).thenReturn(entityManager);
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withArguments(name, 0L).thenReturn(ruleProjectNameCanNotBeInUseByAnotherProject);
		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withArguments(repositoryPath, 0L).thenReturn(ruleProjectRepositoryPathCanNotBeInUseByAnotherProject);
		
		users.add(user);
	}

	@Test
	public void testNewInstance() {		
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertNotNull(project);
		verify(entityManager, times(1)).persist(project);
	}
	
	@Test
	public void testGetName() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(name, project.getName());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheParameterNameCanNotBeNull() {
		Project.newInstance(null, repositoryPath, repositoryType, users, commitsFromDate);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testTheParameterNameCanNotBeBlank() {
		Project.newInstance(" ", repositoryPath, repositoryType, users, commitsFromDate);
	}
	
	@Test
	public void testGetRepositoryPath() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(repositoryPath, project.getRepositoryPath());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheParameterGetRepositoryPathCanNotBeNull() {
		Project.newInstance(name, null, SVN, users, commitsFromDate);
	}

	@Test
	public void testGetUsers() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(users, project.getUsers());
	}

	@Test
	public void testGetType() throws Exception {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(repositoryType, project.getRepositoryType());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTypeCanNotBeNull() throws Exception {
		Project.newInstance(name, repositoryPath, null, users, commitsFromDate);
	}

	@Test
	public void testModifyTheParameterUsersCanNotModifyUsersFromProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		users.clear();
		assertFalse("The users set from project can't be empty", project.getUsers().isEmpty());
	}

	@Test(expected=UnsupportedOperationException.class)
	public void testCanNotModifyUserSetFromGetUsers() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		Set<SystemUser> users = project.getUsers();
		users.clear();
	}

	@Test(expected=NullPointerException.class)
	public void testrUsersParameteCanNotBeNull() {
		Project.newInstance(name, repositoryPath, GIT, null, commitsFromDate);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testUsersParameterCanNotContainANullUser() {
		users.add(null);
		Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
	}
	
	
	@Test(expected=IllegalArgumentException.class)
	public void testUsersParameterCanNotContainARemovedUser() {
		when(user.removed()).thenReturn(true);
		Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
	}
	
	@Test
	public void testGetCommitsFromDate() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(commitsFromDate, project.getCommitsFromDate());
	}	

	@Test(expected=IllegalArgumentException.class)
	public void testCommitsFromDateParameterCanNotBeNull() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, null);
		assertEquals(commitsFromDate, project.getCommitsFromDate());
	}	

	@Test
	public void testGetGitRepository() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		GitRepository gitRepository = new GitRepository(project);
		assertEquals(gitRepository, project.getGitRepository());
	}

	@Test
	public void testUserIsProjectMembership() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertTrue(project.isProjectMembership(user));
		
	}

	@Test(expected=IllegalArgumentException.class)
	public void testNameCanNotBeInUse() throws Exception {
		String name = "Name Can Not Be In Use";
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = mock(RuleProjectNameCanNotBeInUseByAnotherProject.class);
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withArguments(name, 0L).thenReturn(rule);
		when(rule.notInAccord()).thenReturn(true);
		
		Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testRepositoryPathCanNotBeInUse() throws Exception {
		File path = new File("/repository");
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = mock(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class);
		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withArguments(path, 0L).thenReturn(rule);
		when(rule.notInAccord()).thenReturn(true);
		
		Project.newInstance(name, path, repositoryType, users, commitsFromDate);
	}

	@Test
	public void testGetNewCommits() throws Exception {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(0, project.getNewCommits());
		
		int commits = 3;
		project.setNewCommits(commits);
		
		assertEquals(commits, project.getNewCommits());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheNumberOfNewCommitsCanNotBeZero() throws Exception {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(0, project.getNewCommits());
		project.setNewCommits(0);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testTheNumberOfNewCommitsCanNotBeNegative() throws Exception {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		assertEquals(0, project.getNewCommits());
		project.setNewCommits(-1);
	}

	@Test
	public void testListProjects() {
		ProjectRepository repository = mock(ProjectRepository.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
		when(repository.listAllActiveProjects()).thenReturn(new ArrayList<>());
		
		List<Project> projects = Project.listAllOrderedByName();
		assertNotNull(projects);
		verify(repository, times(1)).listAllActiveProjects();
	}

	@Test
	public void testListUserProjects() {
		SystemUser user = mock(SystemUser.class);
		
		ProjectRepository repository = mock(ProjectRepository.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
		when(repository.listAllActiveUserProjects(user)).thenReturn(new ArrayList<>());
		
		List<Project> projects = Project.listUser(user);
		assertNotNull(projects);
		verify(repository, times(1)).listAllActiveUserProjects(user);
	}

	@Test
	public void testFindById() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		
		Long id = project.getId();
		
		ProjectRepository repository = mock(ProjectRepository.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
		
		when(repository.findProjectBy(id)).thenReturn(project);
		
		assertEquals(project, Project.findBy(id));
	}

	@Test(expected=IllegalStateException.class)
	public void testCanNotSetNameAfterRemoveTheProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		project.remove();
		project.setName(name);
	}

	@Test(expected=IllegalStateException.class)
	public void testCanNotSetRepositoryPathAfterRemoveTheProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		project.remove();
		project.setRepositoryPath(repositoryPath);
	}

	@Test(expected=IllegalStateException.class)
	public void testCanNotSetUsersAfterRemoveTheProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		project.remove();
		project.setUsers(users);
	}
	
	@Test(expected=IllegalStateException.class)
	public void testCanNotSetTypeAfterRemoveTheProject() {
		Project project = Project.newInstance(name, repositoryPath, repositoryType, users, commitsFromDate);
		project.remove();
		project.setRepositoryType(RepositoryType.GIT);
	}

}
