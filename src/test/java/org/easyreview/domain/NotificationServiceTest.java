package org.easyreview.domain;

import static org.mockito.Mockito.*;

import java.util.HashSet;
import java.util.Set;

import org.easyreview.mail.MailSendingService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class NotificationServiceTest {

	@Mock
	private MailSendingService mail;
	@Mock
	private Project project;
	@Mock
	private SystemUser user;
	
	final String emailAddress = "user@easyreview.org";
	
	final String to[] = {emailAddress};
	final String accessUrl = "http://localhost:8080/test/notification"; 
	
	private NotificationService notify = new NotificationService();
	private final String projectName = "Project Name";

	@Before
	public void setUp() {
		Whitebox.setInternalState(notify, "mail", mail);
		
		Set<SystemUser> users = new HashSet<>();
		users.add(user);
		when(user.getEmail()).thenReturn(Email.newInstance(emailAddress));
		
		when(project.getName()).thenReturn(projectName);
		when(project.getUsers()).thenReturn(users);
	}
	
	@Test
	public void testSendingAddedNewReviewMessage() {
		String subject = "EasyReview - Added new review for the project " + projectName;
		String message = String.format("Added new review for the project %s! <a href='%s'>Click here to access</a>", projectName, accessUrl);
		
		notify.addedNewReview(project, accessUrl);
		
		verify(mail, times(1)).send(to, subject, message, true);
	}

	@Test
	public void testSendingEditedReviewMessage() {
		String subject = "EasyReview - Edited the review of the project " + projectName;
		String message = String.format("Edited the review of the project %s! <a href='%s'>Click here to access</a>", projectName, accessUrl);
		
		notify.editedReview(project, accessUrl);
		
		verify(mail, times(1)).send(to, subject, message, true);
	}

	@Test
	public void testSendingAddedNewReplyMessage() {
		String subject = "EasyReview - Added new reply to revision of the project " + projectName;
		String message = String.format("Added new reply to revision of the project %s! <a href='%s'>Click here to access</a>", projectName, accessUrl);
		
		notify.addedNewReply(project, accessUrl);
		
		verify(mail, times(1)).send(to, subject, message, true);
	}

	@Test
	public void testSendingrEditedReplyMessage() {
		String subject = "EasyReview - Edited the reply of the review! Project: " + projectName;
		String message = String.format("Edited the reply of the review! Project: %s! <a href='%s'>Click here to access</a>", projectName, accessUrl);
		
		notify.editedReply(project, accessUrl);
		
		verify(mail, times(1)).send(to, subject, message, true);
	}
}
