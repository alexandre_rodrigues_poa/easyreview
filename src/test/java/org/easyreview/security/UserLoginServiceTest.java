package org.easyreview.security;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.Email;
import org.easyreview.domain.SystemUser;
import org.easyreview.domain.SystemUserRepository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.internal.util.reflection.Whitebox;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

@RunWith(MockitoJUnitRunner.class)
public class UserLoginServiceTest {

	@Mock private SystemUserRepository repository;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDefaultUser() {
		String emailAddress = "sysadmin@easyreview.org";
		Email email = Email.newInstance(emailAddress);
		when(repository.findUserBy(email)).thenReturn(null);
		when(repository.notHasAdminUser()).thenReturn(true);
		
		UserLoginService service = new UserLoginService();
		Whitebox.setInternalState(service, "repository", repository);
		
		LoginUser user = (LoginUser) service.loadUserByUsername(emailAddress);
		
		assertEquals("SysAdmin", user.getName());
		assertEquals(emailAddress, user.getUsername());
		assertNotNull(user.getPassword());
		assertFalse(user.isAdmin());
		assertTrue(user.isDefaultUser());
		
		verify(repository, times(1)).findUserBy(email);
		verify(repository, times(1)).notHasAdminUser();
	}

	@Test
	public void testUser() {
		String name = "User Name";
		String emailAddress = "user@easyreview.org";
		boolean isAdmin = true;
		String passwordHash = "passwordhash";
		
		SystemUser systemUser = mock(SystemUser.class);
		when(systemUser.getName()).thenReturn(name);
		when(systemUser.getEmailAddress()).thenReturn(emailAddress);
		when(systemUser.getPasswordHash()).thenReturn(passwordHash);
		when(systemUser.isAdmin()).thenReturn(isAdmin);
		
		when(repository.findUserBy(Email.newInstance(emailAddress))).thenReturn(systemUser);
		
		UserLoginService service = new UserLoginService();
		Whitebox.setInternalState(service, "repository", repository);
		
		LoginUser user = (LoginUser) service.loadUserByUsername(emailAddress);
		
		assertEquals(name, user.getName());
		assertEquals(emailAddress, user.getUsername());
		assertEquals(passwordHash, user.getPassword());
		assertTrue(user.isAdmin());
	}

	@Test(expected=UsernameNotFoundException.class)
	public void testUserNotFound() {
		String emailAddress = "userNotFound@easyreview.org";
		when(repository.findUserBy(Email.newInstance(emailAddress))).thenReturn(null);
		
		UserLoginService service = new UserLoginService();
		Whitebox.setInternalState(service, "repository", repository);
		
		service.loadUserByUsername(emailAddress);
	}
}
