package org.easyreview.security;

import static org.junit.Assert.*;

import org.junit.Test;

public class DefaultUserAuthorityTest {

	@Test
	public void testGetAuthority() {
		DefaultUserAuthority authority = new DefaultUserAuthority();
		assertEquals("ROLE_DEFAULT_USER", authority.getAuthority());
	}
}
