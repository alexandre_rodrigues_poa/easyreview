package org.easyreview.security;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Collection;

import org.easyreview.domain.SystemUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.security.core.GrantedAuthority;

@RunWith(MockitoJUnitRunner.class)
public class LoginUserTest {
	
	@Mock private SystemUser user; 
	private final String name = "User Name";
	private final String userName = "userName@test.com";
	private final String password = "password";
	private Long userId = 237L;
	private final boolean isAdmin = true;
	
	@Before
	public void setUp() throws Exception {
		when(user.getId()).thenReturn(userId);
		when(user.getName()).thenReturn(name);
		when(user.getEmailAddress()).thenReturn(userName);
		when(user.getPasswordHash()).thenReturn(password);
		when(user.isAdmin()).thenReturn(isAdmin);
	}
	
	@Test
	public void testLoginUserSystemUserGetUserId() {
		LoginUser loginUser = new LoginUser(user);
		assertEquals(userId, loginUser.getUserId());
	}

	@Test
	public void testLoginUserSystemUserGetName() {
		LoginUser loginUser = new LoginUser(user);
		assertEquals(name, loginUser.getName());
	}

	@Test
	public void testLoginUserSystemUserGetUserName() {
		LoginUser loginUser = new LoginUser(user);
		assertEquals(userName, loginUser.getUsername());
	}

	@Test
	public void testLoginUserSystemUserGetPassword() {
		LoginUser loginUser = new LoginUser(user);
		assertEquals(password, loginUser.getPassword());
	}
	
	@Test
	public void testLoginUserSystemUserGetIsAdmin() {
		LoginUser loginUser = new LoginUser(user);
		assertTrue(loginUser.isAdmin());
	}

	@Test
	public void testLoginUserSystemUserIsAdmin() {
		LoginUser loginUser = new LoginUser(user);
		GrantedAuthority authority = new AdminAuthority();
		
		Collection<GrantedAuthority> authorities = loginUser.getAuthorities();
		assertTrue(authorities.contains(authority));
	}

	@Test
	public void testLoginUserSystemUserIsNotDefaultUser() {
		LoginUser loginUser = new LoginUser(user);
		GrantedAuthority authority = new DefaultUserAuthority();
		
		Collection<GrantedAuthority> authorities = loginUser.getAuthorities();
		assertFalse(authorities.contains(authority));
	}

	@Test
	public void testGetName() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertEquals(name, loginUser.getName());
	}
	
	@Test
	public void testGetUserName() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertEquals(userName, loginUser.getUsername());
	}
	@Test

	public void testGetIsAdmin() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertFalse(loginUser.isAdmin());
	}
	
	@Test
	public void testGetIsDefaultUser() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertTrue(loginUser.isDefaultUser());
	}
	
	@Test
	public void testGetPassword() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertEquals(password, loginUser.getPassword());
	}

	@Test
	public void testUserIsNotAdmin() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		
		GrantedAuthority authority = new AdminAuthority();
		
		Collection<GrantedAuthority> authorities = loginUser.getAuthorities();
		assertFalse(authorities.contains(authority));
	}

	@Test
	public void testUserIsDefaultUser() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		GrantedAuthority authority = new DefaultUserAuthority();
		
		Collection<GrantedAuthority> authorities = loginUser.getAuthorities();
		assertTrue(authorities.contains(authority));
	}

	@Test
	public void testIsAccountNonExpired() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertTrue(loginUser.isAccountNonExpired());
	}

	@Test
	public void testIsAccountNonLocked() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertTrue(loginUser.isAccountNonLocked());
	}

	@Test
	public void testIsCredentialsNonExpired() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertTrue(loginUser.isCredentialsNonExpired());
	}

	@Test
	public void testIsEnabled() {
		LoginUser loginUser = new LoginUser(name, userName, password);
		assertTrue(loginUser.isEnabled());
	}
}
