package org.easyreview.security;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.Review;
import org.easyreview.domain.ReplyReview;
import org.easyreview.domain.SystemUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SecurityContextHolder.class)
public class LoggedUserTest {

	@Mock private SecurityContext securityContext;
	@Mock private Authentication authentication;
	@Mock private LoginUser loginUser;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(SecurityContextHolder.class);
		when(SecurityContextHolder.getContext()).thenReturn(securityContext);
		when(securityContext.getAuthentication()).thenReturn(authentication);
	}

	@Test
	public void testGetUserIsLogged() {
		LoggedUser loggedUser = new LoggedUser();
		
		when(authentication.getPrincipal()).thenReturn(null);
		assertFalse(loggedUser.getUserIsLogged());
		
		when(authentication.getPrincipal()).thenReturn(loginUser);
		assertTrue(loggedUser.getUserIsLogged());
	}

	@Test
	public void testGetIsAdminWhenUserLogged() {
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.isAdmin()).thenReturn(true);
		
		LoggedUser loggedUser = new LoggedUser();
		assertTrue(loggedUser.getIsAdmin());
	}

	@Test
	public void testGetIsAdminWhenUserNotLogged() {
		when(authentication.getPrincipal()).thenReturn(null);
		
		LoggedUser loggedUser = new LoggedUser();
		assertFalse(loggedUser.getIsAdmin());
	}

	@Test
	public void testGetNameWhenUserLogged() {
		String name = "User Name";
		
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.getName()).thenReturn(name);
		
		LoggedUser loggedUser = new LoggedUser();
		assertEquals(name, loggedUser.getName());
	}
	
	@Test
	public void testGetNameWhenUserNotLogged() {
		when(authentication.getPrincipal()).thenReturn(null);
		when(loginUser.getName()).thenReturn("Name");
		
		LoggedUser loggedUser = new LoggedUser();
		assertNull(loggedUser.getName());
	}

	@Test
	public void testGetUserIdWhenUserLogged() {
		Long userId = 89L;
		
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.getUserId()).thenReturn(userId);
		
		LoggedUser loggedUser = new LoggedUser();
		assertEquals(userId, loggedUser.getUserId());
	}
	
	@Test
	public void testGetUserIdWhenUserNotLogged() {
		when(authentication.getPrincipal()).thenReturn(null);
		when(loginUser.getUserId()).thenReturn(10L);
		
		LoggedUser loggedUser = new LoggedUser();
		assertNull(loggedUser.getUserId());
	}

	@Test
	public void testGetIsDefaultUserWhenUserLogged() {
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.isDefaultUser()).thenReturn(true);
		
		LoggedUser loggedUser = new LoggedUser();
		assertTrue(loggedUser.getIsDefaultUser());
	}

	@Test
	public void testGetIsDefaultUserWhenUserNotLogged() {
		when(authentication.getPrincipal()).thenReturn(null);
		when(loginUser.isDefaultUser()).thenReturn(true);
		
		LoggedUser loggedUser = new LoggedUser();
		assertFalse(loggedUser.getIsDefaultUser());
	}

	@Test
	public void testGetCanManageUserWhenUserLogged() {
		when(authentication.getPrincipal()).thenReturn(loginUser);
		
		LoggedUser loggedUser = new LoggedUser();
		
		when(loginUser.isDefaultUser()).thenReturn(false);
		when(loginUser.isAdmin()).thenReturn(false);
		
		assertFalse(loggedUser.getCanManageUsers());
		
		when(loginUser.isDefaultUser()).thenReturn(true);
		when(loginUser.isAdmin()).thenReturn(false);
		
		assertTrue(loggedUser.getCanManageUsers());
	
		when(loginUser.isDefaultUser()).thenReturn(false);
		when(loginUser.isAdmin()).thenReturn(true);
		
		assertTrue(loggedUser.getCanManageUsers());
	}

	@Test
	public void testGetCanManageUserWhenUserNotLogged() {
		when(authentication.getPrincipal()).thenReturn(null);
		
		LoggedUser loggedUser = new LoggedUser();
		
		when(loginUser.isDefaultUser()).thenReturn(false);
		when(loginUser.isAdmin()).thenReturn(false);
		
		assertFalse(loggedUser.getCanManageUsers());
		
		when(loginUser.isDefaultUser()).thenReturn(true);
		when(loginUser.isAdmin()).thenReturn(false);
		
		assertFalse(loggedUser.getCanManageUsers());
	
		when(loginUser.isDefaultUser()).thenReturn(false);
		when(loginUser.isAdmin()).thenReturn(true);
		
		assertFalse(loggedUser.getCanManageUsers());
	}

	@Test
	public void testIsAuthorOfReview() {
		Long loggedUserId = 89L;
		Long authorId = loggedUserId;
		
		SystemUser author = mock(SystemUser.class);
		when(author.getId()).thenReturn(authorId);
		
		Review review = mock(Review.class);
		when(review.getAuthor()).thenReturn(author);
		
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.getUserId()).thenReturn(loggedUserId);
		
		LoggedUser loggedUser = new LoggedUser();
		assertTrue(loggedUser.isAuthorOf(review));
	}

	@Test
	public void testIsAuthorOfReviewWhenUserNotLogged() {
		Long loggedUserId = 89L;
		Long authorId = loggedUserId;
		
		SystemUser author = mock(SystemUser.class);
		when(author.getId()).thenReturn(authorId);
		
		Review review = mock(Review.class);
		when(review.getAuthor()).thenReturn(author);
		
		when(authentication.getPrincipal()).thenReturn(null);
		when(loginUser.getUserId()).thenReturn(loggedUserId);
		
		LoggedUser loggedUser = new LoggedUser();
		assertFalse(loggedUser.isAuthorOf(review));
	}

	@Test
	public void testIsAuthorOfReplyReview() {
		Long loggedUserId = 171L;
		Long authorId = loggedUserId;
		
		SystemUser author = mock(SystemUser.class);
		when(author.getId()).thenReturn(authorId);
		
		ReplyReview reply = mock(ReplyReview.class);
		when(reply.getAuthor()).thenReturn(author);
		
		when(authentication.getPrincipal()).thenReturn(loginUser);
		when(loginUser.getUserId()).thenReturn(loggedUserId);
		
		LoggedUser loggedUser = new LoggedUser();
		assertTrue(loggedUser.isAuthorOf(reply));
	}

	@Test
	public void testIsAuthorOfReplyReviewWhenUserNotLogged() {
		Long loggedUserId = 171L;
		Long authorId = loggedUserId;
		
		SystemUser author = mock(SystemUser.class);
		when(author.getId()).thenReturn(authorId);
		
		ReplyReview reply = mock(ReplyReview.class);
		when(reply.getAuthor()).thenReturn(author);
		
		when(authentication.getPrincipal()).thenReturn(null);
		when(loginUser.getUserId()).thenReturn(loggedUserId);
		
		LoggedUser loggedUser = new LoggedUser();
		assertFalse(loggedUser.isAuthorOf(reply));
	}
}
