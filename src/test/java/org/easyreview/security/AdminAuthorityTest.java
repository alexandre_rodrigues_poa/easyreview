package org.easyreview.security;

import static org.junit.Assert.*;

import org.junit.Test;

public class AdminAuthorityTest {

	@Test
	public void testGetAuthority() {
		AdminAuthority authority = new AdminAuthority();
		assertEquals("ROLE_ADMIN", authority.getAuthority());
	}
}
