package org.easyreview.controllers.users;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.domain.Email;
import org.easyreview.domain.RuleEmailCanNotBeInUseByAnotherUser;
import org.easyreview.domain.SystemUser;
import org.easyreview.validation.EmailCanNotBeInUseByAnotherUserValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EmailCanNotBeInUseByAnotherUserValidator.class)
public class UserFormEditTest {
	private final Long id = 1l;
	private String name = "User Name";
	private Email email = Email.newInstance("email@domain.com");
	private boolean isAdmin = true;
	
	private Validator validator;

	@Mock private RuleEmailCanNotBeInUseByAnotherUser rule;
	@Mock private SystemUser systemUser;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		when(systemUser.getId()).thenReturn(id);
		when(systemUser.getName()).thenReturn(name);
		when(systemUser.getEmailAddress()).thenReturn(email.getAddress());
		when(systemUser.isAdmin()).thenReturn(isAdmin);

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		
		PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(email, id).thenReturn(rule);
		when(rule.inAccord()).thenReturn(true);
	}

	@Test
	public void testNew() {
		UserFormEdit user = new UserFormEdit(systemUser);
		assertEquals(id, user.getId());
		assertEquals(name, user.getName());
		assertEquals(email.getAddress(), user.getEmailAddress());
		assertEquals(isAdmin, user.isAdmin());
	}

	@Test
	public void testNameCanNotBeBlank() {
		UserFormEdit user = new UserFormEdit(systemUser);

		user.setName(null);
		validate(user);
		
		user.setName(" ");
		validate(user);
	}
	
	@Test
	public void testEmailAddressCanNotBeBlank() {
		UserFormEdit user = new UserFormEdit(systemUser);
		user.setEmailAddress(null);
		
		validate(user);
		
		user.setEmailAddress("");
		
		validate(user);
	}

	@Test
	public void testEmailAddressInvalid() {
		UserFormEdit user = new UserFormEdit(systemUser);
		user.setEmailAddress("email@test.");
		
		validate(user);
	}

	@Test
	public void testEmailAddressInUseByAnotherUser() throws Exception {
		
		Email emailInUse = Email.newInstance("emailInUseBy@another.com");
		PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(emailInUse, id).thenReturn(rule);
		when(rule.inAccord()).thenReturn(false);
		
		UserFormEdit user = new UserFormEdit(systemUser);
		user.setEmailAddress(emailInUse.getAddress());
		
		validate(user);
	}

	private void validate(UserFormEdit user) {
		Set<ConstraintViolation<UserFormEdit>> violations = validator.validate(user);
		assertEquals("Expected a violation", 1, violations.size());
	}
}
