package org.easyreview.controllers.users;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.domain.SystemUserRepository;
import org.easyreview.factorys.ContainerFactory;
import org.easyreview.validation.EmailCanNotBeInUse;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ContainerFactory.class})
public class UserFormInsertTest {
	
	private Validator validator;
	
	@Mock private SystemUserRepository repositoryMock;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
		
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(SystemUserRepository.class)).thenReturn(repositoryMock);
	}
	
	@Test
	public void testName() {
		String name = "User Name"; 
		
		UserFormInsert user = new UserFormInsert();
		user.setName(name);
		
		assertEquals(name, user.getName());
	}

	@Test
	public void testNameCanNotBeNull() {
		UserFormInsert user = new UserFormInsert();
		user.setEmailAddress("addres@domain.com");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		 Set<ConstraintViolation<UserFormInsert>> violations = validator.validate(user);
		 assertEquals(1, violations.size());
		 
		 ConstraintViolation<UserFormInsert> violation = violations.iterator().next();
		 assertTrue(violation.getConstraintDescriptor().getAnnotation().toString().contains(NotBlank.class.getName()));
	}
	@Test
	public void testNameCanNotBeEmpty() {
		UserFormInsert user = new UserFormInsert();
		
		user.setName("   ");
		user.setEmailAddress("addres@domain.com");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		 Set<ConstraintViolation<UserFormInsert>> violations = validator.validate(user);
		 assertEquals(1, violations.size());
		 
		 ConstraintViolation<UserFormInsert> violation = violations.iterator().next();
		 Annotation annotation = violation.getConstraintDescriptor().getAnnotation(); 
		 
		 assertTrue(annotation.toString().contains(NotBlank.class.getName()));
	}

	@Test
	public void testEmailAddressCanNotBeNull() {
		UserFormInsert user = new UserFormInsert();
		
		user.setEmailAddress(null);
		user.setName("Nome do Usuario");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		validate(user, NotBlank.class);
	}

	@Test
	public void testEmailAddressCanNotBeEmpty() {
		UserFormInsert user = new UserFormInsert();
		
		user.setEmailAddress("");
		user.setName("Nome do Usuario");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		validate(user, NotBlank.class);
	}

	@Test
	public void testEmailAddressCanNotBeInvalid() {
		UserFormInsert user = new UserFormInsert();
		
		user.setEmailAddress("email@@email.com");
		user.setName("Nome do Usuario");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		 validate(user, Email.class);
	}

	@Test
	public void testEmailAddressCanNotBeInUse() {
		UserFormInsert user = new UserFormInsert();
		
		String emailAddress = "addres@domain.com";
		org.easyreview.domain.Email email = org.easyreview.domain.Email.newInstance(emailAddress);
		when(repositoryMock.emailInUse(email)).thenReturn(true);		
		
		user.setEmailAddress(emailAddress);
		user.setName("Nome do Usuario");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		 validate(user, EmailCanNotBeInUse.class);
	}

	@Test
	public void testGetPassword() {
		UserFormInsert user = new UserFormInsert();
		
		user.setName("Nome do Usuario");
		user.setEmailAddress("addres@domain.com");
		user.getUserPassword().setPassword("password");
		user.getUserPassword().setConfirmPassword("password");
		
		assertEquals(user.getUserPassword().getPassword(), user.getPassword());
	}

	@Test
	public void testValidateUserPassword() {
		UserFormInsert user = new UserFormInsert();
		
		user.setName("Nome do Usuario");
		user.setEmailAddress("addres@domain.com");
		
		Set<ConstraintViolation<UserFormInsert>> violations = validator.validate(user);
		assertFalse(violations.isEmpty());
	}

	private void validate(UserFormInsert user, Class<?> annotationExpected) {
		Set<ConstraintViolation<UserFormInsert>> violations = validator.validate(user);
		assertEquals(1, violations.size());
		 
		ConstraintViolation<UserFormInsert> violation = violations.iterator().next();
		Annotation annotation = violation.getConstraintDescriptor().getAnnotation(); 
		 
		assertTrue(annotation.toString().contains(annotationExpected.getName()));
	}
}
