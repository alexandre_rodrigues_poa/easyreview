package org.easyreview.controllers.users;

import static org.junit.Assert.*;

import java.lang.annotation.Annotation;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.validation.PasswordMatch;
import org.hibernate.validator.constraints.NotBlank;
import org.junit.Before;
import org.junit.Test;

public class UserFormPasswordTest {
	
	private Validator validator;
	
	@Before
	public void setUp() {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}
	
	@Test
	public void testPasswordCanNotBeBlank() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword(null);
		userPassword.setConfirmPassword("password");
		
		validate(userPassword, NotBlank.class);
		
		userPassword.setPassword(" ");
		userPassword.setConfirmPassword("password");
		
		Set<ConstraintViolation<UserFormPassword>> violations = validator.validate(userPassword);
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testConfirmPasswordCanNotBeBlank() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword("password");
		userPassword.setConfirmPassword(null);
		
		validate(userPassword, NotBlank.class);
		
		userPassword.setConfirmPassword(" ");
		
		Set<ConstraintViolation<UserFormPassword>> violations = validator.validate(userPassword);
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testPasswordAndConfirmPasswordDoNotMatch() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword("password");
		userPassword.setConfirmPassword("PASSWORD");
		
		validate(userPassword, PasswordMatch.class);
	}

	private void validate(UserFormPassword password, Class<?> annotationExpected) {
		Set<ConstraintViolation<UserFormPassword>> violations = validator.validate(password);
		assertEquals("Expected a violation", 1, violations.size());
		 
		ConstraintViolation<UserFormPassword> violation = violations.iterator().next();
		Annotation annotation = violation.getConstraintDescriptor().getAnnotation(); 
		
		assertTrue(annotation.toString().contains(annotationExpected.getName()));
	}
}
