package org.easyreview.controllers.projects;

import static org.junit.Assert.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.List;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.Email;
import org.easyreview.domain.GitCommit;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class CommitViewTest {
	@Mock private GitCommit gitCommit;
	private final int filesReviewed = 10;
	private final boolean loggedUserReviewed = true;
	private List<CodeAnalysisStatus> analysisStatusList = new ArrayList<>();
	
	@Test
	public void testGetMessage() {
		String message = "Commit Message";
		Mockito.when(gitCommit.getMessage()).thenReturn(message);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		
		assertEquals(message, view.getMessage());
	}

	@Test
	public void testGetCommitterName() {
		String name = "Committer name";
		
		Mockito.when(gitCommit.getCommitterName()).thenReturn(name);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		
		assertEquals(name, view.getCommitterName());
	}
	
	@Test
	public void testGetCommitterEmail() {
		Email email = Email.newInstance("committer@email.com");
		
		Mockito.when(gitCommit.getCommitterEmail()).thenReturn(email);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		
		assertEquals(email.getAddress(), view.getCommitterEmail());
	}

	@Test
	public void testGetDate() {
		FormatStyle style = FormatStyle.MEDIUM;
		DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDateTime(style);
		LocalDateTime localDateTime = LocalDateTime.now();
		String date = localDateTime.format(formatter);
		
		Mockito.when(gitCommit.getDateTime()).thenReturn(localDateTime);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		assertEquals(date, view.getDate());
	}

	@Test
	public void testGetCommitId() {
		String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
		
		Mockito.when(gitCommit.getId()).thenReturn(commitId);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		assertEquals(commitId, view.getCommitId());
	}

	@Test
	public void testProjectId() {
		Long projectId = 23286L;
		
		Mockito.when(gitCommit.getProjectId()).thenReturn(projectId);
		CommitView view = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		assertEquals(projectId, view.getProjectId());
	}

	@Test
	public void testCompareDate() {
		LocalDateTime localDateTime01 = LocalDateTime.now();
		LocalDateTime localDateTime02 = localDateTime01.minusHours(1);
		
		Mockito.when(gitCommit.getDateTime()).thenReturn(localDateTime01);
		CommitView commit01 = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);

		Mockito.when(gitCommit.getDateTime()).thenReturn(localDateTime02);
		CommitView commit02 = new CommitView(gitCommit, filesReviewed, loggedUserReviewed, analysisStatusList);
		
		assertEquals(-1, commit01.compareDate(commit02));
		assertEquals(1, commit02.compareDate(commit01));
		assertEquals(0, commit02.compareDate(commit02));
	}

	@Test
	public void testGetFilesReviewed() {
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertEquals(filesReviewed, view.getFilesReviewed());
	}

	@Test
	public void testGetNumberOfFiles() {
		int numberOfFiles = 28;
		Mockito.when(gitCommit.getNumberOfFiles()).thenReturn(numberOfFiles);
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertEquals(numberOfFiles, view.getNumberOfFiles());
	}

	@Test
	public void testGetLoggedUserReviewed() {
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertEquals(loggedUserReviewed, view.getLoggedUserReviewed());
	}

	@Test
	public void testGetHasAnOkReview() {
		analysisStatusList.add(CodeAnalysisStatus.OK);
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertTrue(view.getHasAnOkReview());
	}

	@Test
	public void testGetHasAnCautionReview() {
		analysisStatusList.add(CodeAnalysisStatus.CAUTION);
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertTrue(view.getHasAnCautionReview());
	}

	@Test
	public void testGetHasAnErrorReview() {
		analysisStatusList.add(CodeAnalysisStatus.ERROR);
		CommitView view = new CommitView(gitCommit, filesReviewed , loggedUserReviewed, analysisStatusList);
		assertTrue(view.getHasAnErrorReview());
	}
}
