package org.easyreview.controllers.projects.reviews;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.controllers.projects.reviews.ReviewForm;
import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.Review;
import org.easyreview.domain.SystemUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReviewFormTest {

	@Mock private Review review;
	@Mock private SystemUser author;
	
	private final File filePath = new File("/test/file");
	
	private Validator validator;
	
	@Before
	public void setUp() throws Exception {
		when(review.getFile()).thenReturn(filePath);
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testEmptyConstructor() {
		new ReviewForm();
	}

	@Test
	public void testGetId() {
		Long id = 1L;
		when(review.getId()).thenReturn(id);
		ReviewForm reviewForm = new ReviewForm(review);
		assertEquals(id, reviewForm.getId());
	}

	@Test
	public void testSetId() {
		Long id = 12L;
		ReviewForm reviewForm = new ReviewForm(review);
		reviewForm.setId(id);
		assertEquals(id, reviewForm.getId());
	}
	
	@Test
	public void testSetProjectId() {
		ReviewForm reviewData = new ReviewForm();
		
		Long projectId = 86L;
		reviewData.setProjectId(projectId);
		
		assertEquals(projectId, reviewData.getProjectId());
	}

	@Test
	public void testGetCommitId() {
		String commitId = "aaad445b902fe4f537138fb94b4af2ae30e84607";
		when(review.getCommitId()).thenReturn(commitId);
		ReviewForm reviewData = new ReviewForm(review);
		assertEquals(commitId, reviewData.getCommitId());
	}
	
	@Test
	public void testSetCommitId() {
		ReviewForm reviewData = new ReviewForm();
		
		String commitId = "aaad445b902fe4f537138fb94b4af2ae30e84607";
		reviewData.setCommitId(commitId);
		
		assertEquals(commitId, reviewData.getCommitId());
	}
	
	@Test
	public void testGetFilePath() {
		ReviewForm reviewData = new ReviewForm(review);
		assertEquals(filePath.getPath(), reviewData.getFile());
	}
	
	@Test
	public void testSetFilePath() {
		ReviewForm reviewForm = new ReviewForm();
		
		String file = "/test/set/file";
		reviewForm.setFile(file);
		
		assertEquals(file, reviewForm.getFile());
	}
	
	@Test
	public void testGetAnalysisStatus() {
		CodeAnalysisStatus status = CodeAnalysisStatus.ERROR;
		when(review.getAnalysisStatus()).thenReturn(status);
		ReviewForm reviewForm = new ReviewForm(review);
		assertEquals(status, reviewForm.getAnalysisStatus());
	}
	
	@Test
	public void testSetAnalysisStatus() {
		ReviewForm reviewForm = new ReviewForm(review);
		
		CodeAnalysisStatus status = CodeAnalysisStatus.CAUTION;
		reviewForm.setAnalysisStatus(status);
		
		assertEquals(status, reviewForm.getAnalysisStatus());
	}
	
	@Test
	public void testAnalysisStatusCanNotBeNull() {
		ReviewForm reviewForm = new ReviewForm(review);
		
		Set<ConstraintViolation<ReviewForm>> violations = validator.validateProperty(reviewForm, "analysisStatus");
		assertFalse(violations.isEmpty());
		
		reviewForm.setAnalysisStatus(CodeAnalysisStatus.OK);
		violations = validator.validateProperty(reviewForm, "analysisStatus");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testGetComment() {
		String comment = "Comment of review";
		when(review.getComment()).thenReturn(comment);
		ReviewForm reviewData = new ReviewForm(review);
		assertEquals(comment, reviewData.getComment());
	}

	@Test
	public void testSetComment() {
		ReviewForm reviewForm = new ReviewForm();
		
		String comment = "Test set comment of review";
		reviewForm.setComment(comment);
		
		assertEquals(comment, reviewForm.getComment());
	}
	
	@Test
	public void testCommentCanNotBeBlank() {
		ReviewForm reviewForm = new ReviewForm();
		reviewForm.setComment(" ");
		
		Set<ConstraintViolation<ReviewForm>> violations = validator.validateProperty(reviewForm, "comment");
		assertFalse(violations.isEmpty());
		
		reviewForm.setComment("New Comment");
		violations  = validator.validateProperty(reviewForm, "comment");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testSetPageUrl() {
		String pageUrl = "http://localhost:8080";

		ReviewForm reviewForm = new ReviewForm();
		reviewForm.setPageUrl(pageUrl);

		assertEquals(pageUrl, reviewForm.getPageUrl());
	}
}
