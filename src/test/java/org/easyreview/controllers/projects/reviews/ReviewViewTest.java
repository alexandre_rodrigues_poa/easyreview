package org.easyreview.controllers.projects.reviews;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.easyreview.Registry;
import org.easyreview.controllers.projects.reviews.CodeAnalysisStatusView;
import org.easyreview.controllers.projects.reviews.ReviewView;
import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.Review;
import org.easyreview.domain.ReplyReview;
import org.easyreview.domain.SystemUser;
import org.easyreview.security.LoggedUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.pegdown.PegDownProcessor;

@RunWith(MockitoJUnitRunner.class)
public class ReviewViewTest {

	@Mock private Review review;
	@Mock private SystemUser author;
	@Mock private LoggedUser loggedUser;
	
	@Before
	public void setUp() {
		when(review.getAnalysisStatus()).thenReturn(CodeAnalysisStatus.OK);
		when(review.getAuthor()).thenReturn(author);
	}

	@Test
	public void testGetId() {
		Long id = 1L;
		when(review.getId()).thenReturn(id);
		ReviewView reviewView = new ReviewView(review, loggedUser);
		assertEquals(id, reviewView.getId());
	}
	
	@Test
	public void testGetAnalysisStatus() {
		CodeAnalysisStatus status = CodeAnalysisStatus.ERROR;
		when(review.getAnalysisStatus()).thenReturn(status);
		
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		CodeAnalysisStatusView statusView = new CodeAnalysisStatusView(status); 

		assertEquals(statusView, reviewView.getAnalysisStatus());
	}
	
	@Test
	public void testGetComment() {
		String comment = "# Comment of review";
		when(review.getComment()).thenReturn(comment);
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		PegDownProcessor processor = new PegDownProcessor();
		String formattedComment = processor.markdownToHtml(comment);
		
		assertEquals(formattedComment, reviewView.getComment());
	}

	@Test
	public void testGetDate() {
		LocalDateTime reviewDateTime = LocalDateTime.now();
		when(review.getDateTime()).thenReturn(reviewDateTime);
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		
		String date = formatter.format(reviewDateTime);
		assertEquals(date, reviewView.getDate());
	}
	
	@Test
	public void testGetAuthorName() {
		String authorName = "Author Name";

		when(author.getName()).thenReturn(authorName);
		
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		assertEquals(authorName, reviewView.getAuthorName());
	}

	@Test
	public void testGetEdited() {
		when(review.edited()).thenReturn(true);
		
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		assertTrue(reviewView.getEdited());
	}	

	@Test
	public void testLastEdition() {
		LocalDateTime reviewEditDateTime = LocalDateTime.now();
		when(review.lastEdition()).thenReturn(reviewEditDateTime);
		ReviewView reviewView = new ReviewView(review, loggedUser);
		
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		
		String date = formatter.format(reviewEditDateTime);
		assertEquals(date, reviewView.getLastEdition());
	}	
	
	@Test
	public void testGeLoggedUserIsAuthor() {
		when(loggedUser.isAuthorOf(review)).thenReturn(true);
		ReviewView reviewView = new ReviewView(review, loggedUser);
		assertTrue(reviewView.getLoggedUserIsAuthor());
	}

	@Test
	public void testGetReplies() {
		ReplyReview reply01 = mock(ReplyReview.class);
		ReplyReview reply02 = mock(ReplyReview.class);
		
		when(reply01.getId()).thenReturn(1L);
		when(reply01.getAuthor()).thenReturn(author);
		
		when(reply02.getId()).thenReturn(2L);
		when(reply02.getAuthor()).thenReturn(author);
		
		when(review.listReplies()).thenReturn(Arrays.asList(reply01, reply02));
		
		List<ReplyReviewView> replies = new ArrayList<>(Arrays.asList(new ReplyReviewView(reply01, loggedUser), new ReplyReviewView(reply02, loggedUser)));
		
		ReviewView reviewView = new ReviewView(review, loggedUser);
		assertEquals(replies, reviewView.getReplies());
	}	

	@Test
	public void testGetSomeoneHasReplied() {
		ReplyReview reply = mock(ReplyReview.class);
		
		when(reply.getId()).thenReturn(1L);
		when(reply.getAuthor()).thenReturn(author);
		
		when(review.listReplies()).thenReturn(Arrays.asList(reply));
		
		ReviewView reviewView = new ReviewView(review, loggedUser);
		assertTrue(reviewView.getSomeoneHasReplied());
	}
}
