package org.easyreview.controllers.projects.reviews;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.easyreview.Registry;
import org.easyreview.domain.ReplyReview;
import org.easyreview.domain.SystemUser;
import org.easyreview.security.LoggedUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.pegdown.PegDownProcessor;

@RunWith(MockitoJUnitRunner.class)
public class ReplyReviewViewTest {

	@Mock private ReplyReview reply;
	@Mock private SystemUser author;
	@Mock private LoggedUser loggedUser;

	@Before
	public void setUp() {
		when(reply.getAuthor()).thenReturn(author);
	}

	@Test
	public void testGetId() {
		Long id = 25L;
		when(reply.getId()).thenReturn(id);
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		
		assertEquals(id, view.getId());
	}

	@Test
	public void testGetText() {
		String text = "# Formatted reply";
		
		when(reply.getText()).thenReturn(text);
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		
		PegDownProcessor processor = new PegDownProcessor();
		
		String formattedText = processor.markdownToHtml(text);
		assertEquals(formattedText, view.getText());
	}
	
	@Test
	public void getAuthorName() {
		String authorName = "Author name";
		when(author.getName()).thenReturn(authorName);
		
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		assertEquals(authorName, view.getAuthorName());
	}

	@Test
	public void testGetDate() {
		LocalDateTime replyDateTime = LocalDateTime.now();
		when(reply.getDateTime()).thenReturn(replyDateTime);
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		String date = formatter.format(replyDateTime);
		assertEquals(date, view.getDate());
	}

	@Test
	public void testGetEdited() {
		when(reply.edited()).thenReturn(true);
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		assertTrue(view.getEdited());
	}

	@Test
	public void testGetLastEdition() {
		LocalDateTime replyEditDateTime = LocalDateTime.now();
		when(reply.getLastEdition()).thenReturn(replyEditDateTime);
		
		DateTimeFormatter formatter = Registry.getDateTimeFormatter();
		String date = formatter.format(replyEditDateTime);

		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		
		assertEquals(date, view.getLastEdition());
	}

	@Test
	public void testGeLoggedUserIsAuthor() {
		when(loggedUser.isAuthorOf(reply)).thenReturn(true);
		ReplyReviewView view = new ReplyReviewView(reply, loggedUser);
		assertTrue(view.getLoggedUserIsAuthor());
	}
}
