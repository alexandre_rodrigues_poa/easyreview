package org.easyreview.controllers.projects.reviews;

import static org.junit.Assert.*;

import org.easyreview.controllers.projects.reviews.CodeAnalysisStatusView;
import org.easyreview.domain.CodeAnalysisStatus;
import org.junit.Test;

public class CodeAnalysisStatusViewTest {

	@Test
	public void testGetCodeAnalysisStatus() {
		CodeAnalysisStatus status = CodeAnalysisStatus.CAUTION;
		CodeAnalysisStatusView view = new CodeAnalysisStatusView(status);
		assertEquals(status, view.getCodeAnalysisStatus());
	}

	@Test
	public void testGetInternationalizedDescription() {
		CodeAnalysisStatus status = CodeAnalysisStatus.ERROR;
		String stringStatus = status.toString();
		String description = "review.code.analysis.status." + stringStatus.toLowerCase();
		
		CodeAnalysisStatusView view = new CodeAnalysisStatusView(status);
		assertEquals(description, view.getInternationalizedDescription());
	}
	
	@Test
	public void testGetStringStatus() {
		CodeAnalysisStatus status = CodeAnalysisStatus.OK;
		String stringStatus = status.toString();
		
		
		CodeAnalysisStatusView view = new CodeAnalysisStatusView(status);
		assertEquals(stringStatus, view.getStringStatus());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testParameterStatusCanNotBeNull() {
		new CodeAnalysisStatusView(null);
	}
}
