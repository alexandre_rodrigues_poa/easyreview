package org.easyreview.controllers.projects.reviews;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.domain.ReplyReview;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ReplyReviewFormTest {

	@Mock
	private ReplyReview reply;
	
	private Validator validator;
	
	@Before
	public void setUp() throws Exception {
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testEmptyConstructor() {
		new ReplyReviewForm();
	}

	@Test
	public void testGetId() {
		Long replyId = 10L;
		when(reply.getId()).thenReturn(replyId);
		
		ReplyReviewForm replyForm =new ReplyReviewForm(reply);
		
		assertEquals(replyId, replyForm.getId());
	}

	@Test
	public void testSetId() {
		Long replyId = 21L;
		
		ReplyReviewForm replyForm =new ReplyReviewForm();
		replyForm.setId(replyId);
		assertEquals(replyId, replyForm.getId());
	}

	@Test
	public void testSetReviewId() {
		Long reviewId = 101L;
		ReplyReviewForm replyForm =new ReplyReviewForm();
		
		replyForm.setReviewId(reviewId);
		assertEquals(reviewId, replyForm.getReviewId());
	}

	@Test
	public void testGetText() {
		String text = "Reply text";
		when(reply.getText()).thenReturn(text);
		ReplyReviewForm replyForm = new ReplyReviewForm(reply);
		assertEquals(text, replyForm.getText());
	}

	@Test
	public void testSetText() {
		String text = "Reply text";
		ReplyReviewForm replyForm = new ReplyReviewForm();
		replyForm.setText(text);
		assertEquals(text, replyForm.getText());
	}
	
	@Test
	public void testTextCanNotBeBlank() {
		ReplyReviewForm replyForm = new ReplyReviewForm();
		replyForm.setText(" ");
		
		Set<ConstraintViolation<ReplyReviewForm>> violations = validator.validateProperty(replyForm, "text");
		assertFalse(violations.isEmpty());
		
		replyForm.setText("New reply");
		violations  = validator.validateProperty(replyForm, "text");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testSetPageUrl() {
		String pageUrl = "http://www.easyreview.org/test";
		
		ReplyReviewForm replyForm = new ReplyReviewForm();
		replyForm.setPageUrl(pageUrl);

		assertEquals(pageUrl, replyForm.getPageUrl());
	}
}
