package org.easyreview.controllers.projects;

import static org.junit.Assert.*;

import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.domain.SystemUser;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class UserDataTest {

	@Mock private SystemUser systemUser;
	private final boolean isMemberOfProject = false;

	private Validator validator;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}
	
	@Test
	public void testEmptyConstructor() {
		new UserData();
	}

	@Test
	public void testIsProjectMembership() {
		boolean isProjectMembership = true;
		
		UserData user = new UserData(isProjectMembership, systemUser);
		assertTrue(user.getIsProjectMembership());
	}

	@Test
	public void testId() {
		Long userId = 13L;
		Mockito.when(systemUser.getId()).thenReturn(userId);
		
		UserData user = new UserData(isMemberOfProject, systemUser);
		assertEquals(userId, user.getId());
	}

	@Test
	public void testIdCanNotBeNull() {
		Long userId = 21L;
		Mockito.when(systemUser.getId()).thenReturn(userId);
		
		UserData user = new UserData(isMemberOfProject, systemUser);
		
		Set<ConstraintViolation<UserData>> violations = validator.validateProperty(user, "id");
		assertTrue(violations.isEmpty());
		
		user.setId(null);
		violations = validator.validateProperty(user, "id");
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testUserName() {
		String name = "User Name";
		Mockito.when(systemUser.getName()).thenReturn(name);
		
		UserData user = new UserData(isMemberOfProject, systemUser);
		assertEquals(name, user.getName());
	}

	@Test
	public void testUserEmail() {
		String email = "email@domain.com";
		Mockito.when(systemUser.getEmailAddress()).thenReturn(email);
		UserData user = new UserData(isMemberOfProject, systemUser);
		assertEquals(email, user.getEmail());
	}
}
