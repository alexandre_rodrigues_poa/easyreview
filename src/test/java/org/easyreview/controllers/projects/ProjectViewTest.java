package org.easyreview.controllers.projects;

import static org.junit.Assert.*;

import org.easyreview.domain.Project;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProjectViewTest {

	@Mock private Project project;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetId() {
		long id = 321L;
		Mockito.when(project.getId()).thenReturn(id);
		
		ProjectView view = new ProjectView(project);
		assertEquals(id, view.getId());
	}

	@Test
	public void testGetProjectName() {
		String name = "Project Name";
		Mockito.when(project.getName()).thenReturn(name);
		
		ProjectView view = new ProjectView(project);
		assertEquals(name, view.getName());
	}

	@Test
	public void testGetNumberOfCommits() {
		int numberOfCommits = 34;
		Mockito.when(project.getNumberOfCommits()).thenReturn(numberOfCommits);
		
		ProjectView view = new ProjectView(project);
		assertEquals(numberOfCommits, view.getNumberOfCommits());
	}

	@Test
	public void testGetHasError() {
		Mockito.when(project.getNumberOfCommits()).thenThrow(new RuntimeException());
		ProjectView view = new ProjectView(project);
		assertTrue(view.getHasError());
	}
	
	@Test
	public void testGetNewCommits() {
		int newCommits = 10;
		Mockito.when(project.getNewCommits()).thenReturn(newCommits);
		ProjectView view = new ProjectView(project);
		
		assertEquals(newCommits, view.getNewCommits());
	}
}
