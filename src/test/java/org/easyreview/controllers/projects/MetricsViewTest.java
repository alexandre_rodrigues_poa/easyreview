package org.easyreview.controllers.projects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.ReviewsMetrics;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class MetricsViewTest {

	@Mock
	private ReviewsMetrics metrics;

	@Test
	public void testGetNumberOfFilesRevised() {
		long numberOfFilesRevised = 8;
		when(metrics.numberOfFilesRevised()).thenReturn(numberOfFilesRevised);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfFilesRevised, view.getNumberOfFilesRevised());
	}

	@Test
	public void testGetNumberOfFilesWithErrors() {
		long numberOfFilesWithErrors = 16;
		when(metrics.numberOfFilesWithErrors()).thenReturn(numberOfFilesWithErrors);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfFilesWithErrors, view.getNumberOfFilesWithErrors());
	}

	@Test
	public void testGetNumberOfFilesWithoutProblemsAndErrors() {
		long numberOfFilesWithoutProblemsAndErrors = 32;
		when(metrics.numberOfFilesWithoutProblemsAndErrors()).thenReturn(numberOfFilesWithoutProblemsAndErrors);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfFilesWithoutProblemsAndErrors, view.getNumberOfFilesWithoutProblemsAndErrors());
	}
	
	@Test
	public void testGetNumberOfFilesWithProblems() {
		long numberOfFilesWithProblems = 64;
		when(metrics.numberOfFilesWithProblems()).thenReturn(numberOfFilesWithProblems);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfFilesWithProblems, view.getNumberOfFilesWithProblems());
	}

	@Test
	public void testGetNumberOfReviews() {
		long numberOfReviews = 11;
		when(metrics.numberOfReviews()).thenReturn(numberOfReviews);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfReviews, view.getNumberOfReviews());
	}

	@Test
	public void testGetNumberOfReviewsWithCautionStatus() {
		long numberOfReviewsWithCautionStatus = 22;
		when(metrics.numberOfReviewsWithCautionStatus()).thenReturn(numberOfReviewsWithCautionStatus);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfReviewsWithCautionStatus, view.getNumberOfReviewsWithCautionStatus());
	}

	@Test
	public void testGetNumberOfReviewsWithErrorStatus() {
		long numberOfReviewsWithErrorStatus = 33;
		when(metrics.numberOfReviewsWithErrorStatus()).thenReturn(numberOfReviewsWithErrorStatus);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfReviewsWithErrorStatus, view.getNumberOfReviewsWithErrorStatus());
	}

	@Test
	public void testGetNumberOfReviewsWithOkStatus() {
		long numberOfReviewsWithOkStatus = 44;
		when(metrics.numberOfReviewsWithOkStatus()).thenReturn(numberOfReviewsWithOkStatus);
		
		MetricsView view = new MetricsView(metrics);
		assertEquals(numberOfReviewsWithOkStatus, view.getNumberOfReviewsWithOkStatus());
	}

	@Test
	public void testGetPercentageOfFilesWithErrorsRounded() {
		double percentageOfFilesWithErrors = 12.78;
		when(metrics.percentageOfFilesWithErrors()).thenReturn(percentageOfFilesWithErrors);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfFilesWithErrors);
		assertEquals(percentageRounded, view.getPercentageOfFilesWithErrorsRounded());
	}

	@Test
	public void testGetPercentageOfFilesWithoutProblemsAndErrorsRounded() {
		double percentageOfFilesWithoutProblemsAndErrors = 2.50;
		when(metrics.percentageOfFilesWithoutProblemsAndErrors()).thenReturn(percentageOfFilesWithoutProblemsAndErrors);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfFilesWithoutProblemsAndErrors);
		assertEquals(percentageRounded, view.getPercentageOfFilesWithoutProblemsAndErrorsRounded());
	}

	@Test
	public void testGetPercentageOfFilesWithProblemsRounded() {
		double percentageOfFilesWithProblems = 6.49;
		when(metrics.percentageOfFilesWithProblems()).thenReturn(percentageOfFilesWithProblems);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfFilesWithProblems);
		assertEquals(percentageRounded, view.getPercentageOfFilesWithProblemsRounded());
	}

	@Test
	public void testGetPercentageOfReviewsWithCautionStatusRounded() {
		double percentageOfReviewsWithCautionStatus = 8.18;
		when(metrics.percentageOfReviewsWithCautionStatus()).thenReturn(percentageOfReviewsWithCautionStatus);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfReviewsWithCautionStatus);
		assertEquals(percentageRounded, view.getPercentageOfReviewsWithCautionStatusRounded());
	}

	@Test
	public void testGetPercentageOfReviewsWithErrorStatusRounded() {
		double percentageOfReviewsWithErrorStatus = 0.69;
		when(metrics.percentageOfReviewsWithErrorStatus()).thenReturn(percentageOfReviewsWithErrorStatus);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfReviewsWithErrorStatus);
		assertEquals(percentageRounded, view.getPercentageOfReviewsWithErrorStatusRounded());
	}

	@Test
	public void testGetPercentageOfReviewsWithOkStatusRounded() {
		double percentageOfReviewsWithOkStatus = 4.99;
		when(metrics.percentageOfReviewsWithOkStatus()).thenReturn(percentageOfReviewsWithOkStatus);
		
		MetricsView view = new MetricsView(metrics);
		long percentageRounded = Math.round(percentageOfReviewsWithOkStatus);
		assertEquals(percentageRounded, view.getPercentageOfReviewsWithOkStatusRounded());
	}
}
