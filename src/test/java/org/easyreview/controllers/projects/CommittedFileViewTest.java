package org.easyreview.controllers.projects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;

import static org.easyreview.repositories.CommittedFileStatus.*;

import org.easyreview.domain.CodeAnalysisStatus;
import org.easyreview.domain.GitCommit;
import org.easyreview.repositories.CommittedFile;
import org.easyreview.repositories.CommittedFileStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class CommittedFileViewTest {

	@Mock private CommittedFile committedFile;
	@Mock private GitCommit gitCommit;
	
	private final Long projectId = 156L;
	private final String commitId = "a23286d546d038fc01dbc237a0bcab3247268e94";
	private final String fileName = "/test/test.java";
	private final CommittedFileStatus status = CommittedFileStatus.CHANGED;
	private final long numberOfReviews = 4L;
	private boolean loggedUserReviewedTheFile = true;
	private List<CodeAnalysisStatus> analysisStatusList = new ArrayList<>();
	
	@Before
	public void setUp() throws Exception {
		when(gitCommit.getProjectId()).thenReturn(projectId);
		when(gitCommit.getId()).thenReturn(commitId);
		when(committedFile.getName()).thenReturn(fileName);
		when(committedFile.getStatus()).thenReturn(status);
	}

	@Test
	public void testGetProjecId() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);  
		assertEquals(projectId, committedFileView.getProjectId());
	}

	@Test
	public void testGetCommitId() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);  
		assertEquals(commitId, committedFileView.getCommitId());
	}
	
	@Test
	public void testGetFileName() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);  
		assertEquals(fileName, committedFileView.getFileName());
	}

	@Test
	public void testGetFileStatus() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		String fileStatus = "project.file.status." + status.toString().toLowerCase();
		assertEquals(fileStatus, committedFileView.getFileStatus());
	}
	
	@Test
	public void testNumberOfReviews() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertEquals(numberOfReviews, committedFileView.getNumberOfReviews());
	}
	
	@Test
	public void testLoggedUserReviewedTheFile() {
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertEquals(loggedUserReviewedTheFile, committedFileView.getLoggedUserReviewed());
	}

	@Test
	public void testGetHasAnOkReview() {
		analysisStatusList.add(CodeAnalysisStatus.OK);
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertTrue(committedFileView.getHasAnOkReview());
	}

	@Test
	public void testGetHasAnCautionReview() {
		analysisStatusList.add(CodeAnalysisStatus.CAUTION);
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertTrue(committedFileView.getHasAnCautionReview());
	}

	@Test
	public void testGetHasAnErrorReview() {
		analysisStatusList.add(CodeAnalysisStatus.ERROR);
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertTrue(committedFileView.getHasAnErrorReview());
	}

	@Test
	public void testGetRevisonPathForModifiedFile() {
		reset(committedFile);
		when(committedFile.getStatus()).thenReturn(CHANGED);
		when(committedFile.getName()).thenReturn(fileName);
		
		String path = String.format("review/modifiedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertEquals(path, committedFileView.getRevisonPath());
	}

	@Test
	public void testGetRevisonPathForAddedFile() {
		reset(committedFile);
		when(committedFile.getStatus()).thenReturn(ADDED);
		when(committedFile.getName()).thenReturn(fileName);
		
		String path = String.format("review/addedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertEquals(path, committedFileView.getRevisonPath());
	}

	@Test
	public void testGetRevisonPathForRemovedFile() {
		reset(committedFile);
		when(committedFile.getStatus()).thenReturn(ADDED);
		when(committedFile.getName()).thenReturn(fileName);
		
		String path = String.format("review/addedFile/%s/%s?fileName=%s", projectId, commitId, fileName);
		
		CommittedFileView committedFileView = new CommittedFileView(gitCommit, committedFile, numberOfReviews, loggedUserReviewedTheFile, analysisStatusList);
		assertEquals(path, committedFileView.getRevisonPath());
	}
}
