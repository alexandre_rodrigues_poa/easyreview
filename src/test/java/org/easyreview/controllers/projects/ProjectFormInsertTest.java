package org.easyreview.controllers.projects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.easyreview.domain.ProjectRepository;
import org.easyreview.domain.RepositoryType;
import org.easyreview.domain.SystemUser;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SystemUser.class, ContainerFactory.class})
public class ProjectFormInsertTest {

	@Mock private SystemUser systemUser01;
	@Mock private SystemUser systemUser02;
	@Mock private ProjectRepository repository;

	private Validator validator;

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);

		List<SystemUser> systemUsers = new ArrayList<>();
		systemUsers.add(systemUser01);
		systemUsers.add(systemUser02);

		PowerMockito.mockStatic(SystemUser.class);
		PowerMockito.when(SystemUser.listAllActiveOrderedByName()).thenReturn(systemUsers);

		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);

		when(systemUser01.getId()).thenReturn(1L);
		when(systemUser02.getId()).thenReturn(2L);

		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();
	}

	@Test
	public void testPopulateUsers() {
		ProjectFormInsert form = new ProjectFormInsert();
		assertFalse(form.getUsers().isEmpty());

		List<UserData> users = form.getUsers();
		assertFalse(users.isEmpty());

		assertTrue(users.contains(new UserData(false, systemUser01)));
		assertTrue(users.contains(new UserData(false, systemUser02)));
	}

	@Test
	public void testGetName() {
		String name = "Project Name";

		ProjectFormInsert form = new ProjectFormInsert();
		form.setName(name);

		assertEquals(name, form.getName());
	}

	@Test
	public void testNameCanNotBeBlank() {
		ProjectFormInsert form = new ProjectFormInsert();
		form.setName(" ");

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(form, "name");
		assertFalse(violations.isEmpty());

		form.setName("Project Name");

		violations = validator.validateProperty(form, "name");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testNameCanNotBeInUse() {
		String name = "Project Name In Use";

		when(repository.nameInUse(name)).thenReturn(true);

		ProjectFormInsert form = new ProjectFormInsert();
		form.setName(name);

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(form, "name");
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testGetRepositoryPath() {
		String repositoryPath = "D:\\Test\\Repository\\";

		ProjectFormInsert form = new ProjectFormInsert();
		form.setRepositoryPath(repositoryPath);

		assertEquals(repositoryPath, form.getRepositoryPath());
	}

	@Test
	public void testRepositoryPathCanNotBeBlank() {
		ProjectFormInsert form = new ProjectFormInsert();
		form.setRepositoryPath(" ");

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(form, "repositoryPath");
		assertFalse(violations.isEmpty());

		form.setRepositoryPath("/");

		violations = validator.validateProperty(form, "repositoryPath");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testRepositoryPathCanNotBeInUse() {
		String repositoryPath = "D:\\Test\\Repository\\In\\Use";
		File path = new File(repositoryPath);

		when(repository.repositoryPathInUse(path)).thenReturn(true);

		ProjectFormInsert formInsert = new ProjectFormInsert();
		formInsert.setRepositoryPath(repositoryPath);

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(formInsert, "repositoryPath");
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testGetCommitsFromDate() {
		LocalDate fromDate = LocalDate.now();

		ProjectFormInsert formInsert = new ProjectFormInsert();
		formInsert.setCommitsFromDate(fromDate);

		assertEquals(fromDate, formInsert.getCommitsFromDate());
	}

	@Test
	public void testCommitsFromDateCanNotBeNull() {
		ProjectFormInsert form = new ProjectFormInsert();

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(form, "commitsFromDate");
		assertFalse(violations.isEmpty());

		form.setCommitsFromDate(LocalDate.now());
		violations = validator.validateProperty(form, "commitsFromDate");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testGetRepositoryType() {
		RepositoryType repositoryType = RepositoryType.GIT;

		ProjectFormInsert form = new ProjectFormInsert();
		form.setRepositoryType(repositoryType);

		assertEquals(repositoryType, form.getRepositoryType());
	}

	@Test
	public void testRepositoryTypeCanNotBeNull () {
		ProjectFormInsert form = new ProjectFormInsert();
		form.setRepositoryType(RepositoryType.SVN);

		Set<ConstraintViolation<ProjectFormInsert>> violations = validator.validateProperty(form, "repositoryType");
		assertTrue(violations.isEmpty());

		form.setRepositoryType(null);

		violations = validator.validateProperty(form, "repositoryType");
		assertFalse(violations.isEmpty());

	}
}
