package org.easyreview.controllers.projects;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;
import java.time.LocalDate;
import java.util.*;

import javax.validation.*;

import org.easyreview.domain.*;
import org.easyreview.validation.*;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({SystemUser.class, ProjectNameCanNotBeInUseByAnotherProjectValidator.class, RepositoryPathCanNotBeInUseByAnotherProjectValidator.class})
public class ProjectFormConfigTest {

	@Mock private SystemUser systemUser01;
	@Mock private SystemUser systemUser02;
	@Mock private RuleProjectNameCanNotBeInUseByAnotherProject ruleProjectName;
	@Mock private RuleProjectRepositoryPathCanNotBeInUseByAnotherProject ruleRepositoryPath;
	
	private Validator validator;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		List<SystemUser> systemUsers = new ArrayList<>();
		systemUsers.add(systemUser01);
		systemUsers.add(systemUser02);
		
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withAnyArguments().thenReturn(ruleProjectName);
		when(ruleProjectName.inAccord()).thenReturn(true);
		
		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withAnyArguments().thenReturn(ruleRepositoryPath);
		when(ruleRepositoryPath.inAccord()).thenReturn(true);
		
		PowerMockito.mockStatic(SystemUser.class);
		PowerMockito.when(SystemUser.listAllActiveOrderedByName()).thenReturn(systemUsers);
		
		when(systemUser01.getId()).thenReturn(1L);
		when(systemUser02.getId()).thenReturn(2L);
		
		ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
		validator = factory.getValidator();		
	}

	@Test
	public void testEmptyConstructor() {
		new ProjectFormConfig();
	}

	@Test
	public void testConstructorByProject() {
		Long id = 391L;
		String projectName = "Projec Name";
		File repositoryPath = new File("/repository/test");
		RepositoryType repositoryType = RepositoryType.SVN;
		LocalDate fromDate = LocalDate.now();
		
		Project project = mock(Project.class);
		
		when(project.getId()).thenReturn(id);
		when(project.getName()).thenReturn(projectName);
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.getRepositoryType()).thenReturn(repositoryType);

		when(project.getCommitsFromDate()).thenReturn(fromDate);
		
		ProjectFormConfig formConfig = new ProjectFormConfig(project);
		
		assertEquals(id, formConfig.getId());
		assertEquals(projectName, formConfig.getName());
		assertEquals(repositoryPath.getPath(), formConfig.getRepositoryPath());
		assertEquals(repositoryType, formConfig.getRepositoryType());
		assertFalse(formConfig.getUsers().isEmpty());
		assertEquals(fromDate, formConfig.getCommitsFromDate());
	}

	@Test
	public void testSetId() {
		Long id = 23L;
		ProjectFormConfig formConfig = new ProjectFormConfig();
		formConfig.setId(id);

		assertEquals(id, formConfig.getId());
	}

	@Test
	public void testIdCanNotBeNull() {
		ProjectFormConfig formConfig = new ProjectFormConfig();
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validateProperty(formConfig, "id");
		assertFalse(violations.isEmpty());
		
		formConfig.setId(0L);

		violations = validator.validateProperty(formConfig, "id");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testUserIsProjectMembership() {
		File repositoryPath = new File("/repository/test");

		Project project = mock(Project.class);
		
		when(project.getRepositoryPath()).thenReturn(repositoryPath);
		when(project.isProjectMembership(systemUser02)).thenReturn(true);

		ProjectFormConfig formConfig = new ProjectFormConfig(project);
		
		for (UserData user : formConfig.getUsers())
			if (user.getIsProjectMembership())
				if (!user.getId().equals(systemUser02.getId()))
					fail("User is not membership");
				else
					return;
		fail("User membership not found");
	}

	@Test
	public void testSetName() {
		String name = "Project Name";
		
		ProjectFormConfig formConfig = new ProjectFormConfig();
		formConfig.setName(name);
		
		assertEquals(name, formConfig.getName());
	}

	@Test
	public void testNameCanNotBeBlank() {
		ProjectFormConfig formConfig = new ProjectFormConfig();
		formConfig.setName(" ");
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validateProperty(formConfig, "name");
		assertFalse(violations.isEmpty());
		
		formConfig.setName("Project Name");

		violations = validator.validateProperty(formConfig, "name");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testNameCanNotBeInUseByAnotherUser() throws Exception {
		Long projectId = 1L;
		String projectName = "Project Name In Use";
		String repositoryPath = "/test/repository";
		
		RuleProjectNameCanNotBeInUseByAnotherProject rule = mock(RuleProjectNameCanNotBeInUseByAnotherProject.class);
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withArguments(projectName, projectId).thenReturn(rule);
		when(rule.inAccord()).thenReturn(false);

		ProjectFormConfig formConfig = new ProjectFormConfig();
		formConfig.setId(projectId );
		formConfig.setName(projectName);
		formConfig.setRepositoryPath(repositoryPath);
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validate(formConfig);
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testSetRepositoryPath() {
		String repositoryPath = "D:\\Test\\Repository\\";
		
		ProjectFormConfig form = new ProjectFormConfig();
		form.setRepositoryPath(repositoryPath);
		
		assertEquals(repositoryPath, form.getRepositoryPath());
	}

	@Test
	public void testRepositoryPathCanNotBeBlank() {
		ProjectFormConfig form = new ProjectFormConfig();
		form.setRepositoryPath(" ");
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validateProperty(form, "repositoryPath");
		assertFalse(violations.isEmpty());
		
		form.setRepositoryPath("/");

		violations = validator.validateProperty(form, "repositoryPath");
		assertTrue(violations.isEmpty());
	}

	@Test
	public void testRepositoryPathCanNotBeInUseByAnotherProject() throws Exception {
		Long projectId = 1L;
		String projectName = "Project Name";
		String repositoryPath = "/test/repository/inUse";
		
		RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule = mock(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class);
		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withArguments(new File(repositoryPath), projectId).thenReturn(rule);
		when(rule.inAccord()).thenReturn(false);

		ProjectFormConfig formConfig = new ProjectFormConfig();
		formConfig.setId(projectId );
		formConfig.setName(projectName);
		formConfig.setRepositoryPath(repositoryPath);
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validate(formConfig);
		System.out.println(violations);
		assertFalse(violations.isEmpty());
	}

	@Test
	public void testSetRepositoryType() {
		RepositoryType repositoryType = RepositoryType.SVN;
		
		ProjectFormConfig form = new ProjectFormConfig();
		form.setRepositoryType(repositoryType);
		
		assertEquals(repositoryType, form.getRepositoryType());
	}

	@Test
	public void testSetRepositoryTypeCanNotBeNull() {
		
		ProjectFormConfig form = new ProjectFormConfig();
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validateProperty(form, "repositoryType");
		assertFalse(violations.isEmpty());

		form.setRepositoryType(RepositoryType.GIT);
		violations = validator.validateProperty(form, "repositoryType");

		assertTrue(violations.isEmpty());
	}

	@Test
	public void testSetCommitsFromDate() {
		LocalDate fromDate = LocalDate.now();
		
		ProjectFormConfig form = new ProjectFormConfig();
		form.setCommitsFromDate(fromDate);
		
		assertEquals(fromDate, form.getCommitsFromDate());
	}

	@Test
	public void testCommitsFromDateCanNotBeNull() {
		ProjectFormConfig form = new ProjectFormConfig();
		
		Set<ConstraintViolation<ProjectFormConfig>> violations = validator.validateProperty(form, "commitsFromDate");
		assertFalse(violations.isEmpty());

		form.setCommitsFromDate(LocalDate.now());
		violations = validator.validateProperty(form, "commitsFromDate");
		assertTrue(violations.isEmpty());
	}
}
