package org.easyreview.controllers.projects;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class FileViewModeTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDefaultMode() {
		String filePath = "/repository/file.properties";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text", fileViewMode.get(filePath));	
	}

	@Test
	public void testJavaMode() {
		String filePath = "/repository/javaFile.java";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/x-java", fileViewMode.get(filePath));	
	}

	@Test
	public void testXmlMode() {
		String filePath = "/repository/xmlFile.xml";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("application/xml", fileViewMode.get(filePath));	
	}

	@Test
	public void testHtmlMode() {
		String filePath = "/repository/htmlFile.html";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/html", fileViewMode.get(filePath));	
	}

	@Test
	public void testXHtmlMode() {
		String filePath = "/repository/xHtmlFile.xhtml";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/html", fileViewMode.get(filePath));	
	}

	@Test
	public void testHtmMode() {
		String filePath = "/repository/htmFile.htm";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/html", fileViewMode.get(filePath));	
	}

	@Test
	public void testCssMode() {
		String filePath = "/repository/cssFile.css";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/css", fileViewMode.get(filePath));	
	}

	@Test
	public void testJavascriptMode() {
		String filePath = "/repository/javascriptFile.js";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/javascript", fileViewMode.get(filePath));	
	}
	
	@Test
	public void testJSonMode() {
		String filePath = "/repository/jsonFile.json";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("application/json", fileViewMode.get(filePath));	
	}

	@Test
	public void testJspMode() {
		String filePath = "/repository/jspFile.jsp";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("application/x-jsp", fileViewMode.get(filePath));	
	}

	@Test
	public void testSqlMode() {
		String filePath = "/repository/sqlFile.sql";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/x-sql", fileViewMode.get(filePath));	
	}

	@Test
	public void testPascalMode() {
		String filePath = "/repository/pascalFile.pas";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/x-pascal", fileViewMode.get(filePath));	
	}

	@Test
	public void testPhpMode() {
		String filePath = "/repository/phpFile.php";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("application/x-httpd-php", fileViewMode.get(filePath));	
	}

	@Test
	public void testRubyMode() {
		String filePath = "/repository/rubyFile.rb";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/x-ruby", fileViewMode.get(filePath));	
	}
	
	@Test
	public void testScalaMode() {
		String filePath = "/repository/scalaFile.scala";
		FileViewMode fileViewMode = new FileViewMode();
		assertEquals("text/x-scala", fileViewMode.get(filePath));	
	}
	
	//.
}
