package org.easyreview.repositories;

import static org.junit.Assert.*;

import org.easyreview.repositories.CommittedFile;
import org.easyreview.repositories.CommittedFileStatus;
import org.junit.*;

public class CommittedFileTest {
	private final String name = "QuantisComum/src/test/java/br/com/digilogos/dominio/regras/TestaTradeFinderLow.java";
	private final CommittedFileStatus status = CommittedFileStatus.CHANGED; 

	@Test
	public void testFileName() {
		CommittedFile file = new CommittedFile(name, status);
		assertEquals(name, file.getName());
	}

	@Test
	public void testFileStatus() {
		CommittedFile file = new CommittedFile(name, status);
		assertEquals(status, file.getStatus());
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFileNameCanNotBeNull() {
		String name = null;
		new CommittedFile(name, status);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFileNameCanNotBeEmpty() {
		String name = "";
		new CommittedFile(name, status);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testFileNameCanNotBeBlank() {
		String name = "  ";
		new CommittedFile(name, status);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFileStausCanNotBeNull() {
		CommittedFileStatus status = null; 
		new CommittedFile(name, status);
	}
}
