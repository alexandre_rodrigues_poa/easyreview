package org.easyreview.repositories.git;

import static org.junit.Assert.*;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class ServiceGitCommitTest {

	private Repository gitRepository;
	
	@Before
	public void setUp() throws IOException {
		File directory = new File("D:\\Backup Dell Inspiron\\Desenvolvimento\\Java\\Quantis");
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		gitRepository = builder.findGitDir(directory).build();
	}

	@Before
	public void tearDown() throws IOException {
		gitRepository.close();
	}

	@Test
	public void listCommitsFromDate() throws NoHeadException, GitAPIException, IOException {
		@SuppressWarnings("deprecation")
		Date fromDate = new Date(2000, 01, 01);
		
		Git git = new Git(gitRepository);
		
		List<RevCommit> listCommitsExpected = new ArrayList<>();
		try {
			LogCommand log = git.log();
			Iterable<RevCommit> iterable = log.all().call();
			for (RevCommit commit : iterable)
				listCommitsExpected.add(commit);
		} finally {
			git.close();
		}
		
		ServiceGitCommit service = new ServiceGitCommit(gitRepository);
		
		List<RevCommit> allCommits = service.listCommitsFrom(fromDate);
		assertFalse("The list of commit can not be empty", allCommits.isEmpty());
		
		assertEquals(listCommitsExpected, allCommits);
	}

	@Test(expected=IllegalArgumentException.class)
	public void testRepositoryCanNotBeNull() {
		new ServiceGitCommit(null);
	}

	@Test
	public void testCountNumberOfCommits() throws NoHeadException, GitAPIException, IOException {
		@SuppressWarnings("deprecation")
		Date fromDate = new Date(2000, 01, 01);

		Git git = new Git(gitRepository);
		
		int count = 0;
		try {
			Iterable<RevCommit> iterable = git.log().all().call();
			for (Iterator<RevCommit> iterator = iterable.iterator(); iterator.hasNext(); iterator.next())
				count++;
		} finally {
			git.close();
		}
		
		ServiceGitCommit service = new ServiceGitCommit(gitRepository);
		
		assertEquals(count, service.countNumberOfCommitsFrom(fromDate));
	}

	@Test
	public void testFindCommitBy() throws NoHeadException, GitAPIException, IOException {
		String commitId = "19031cc731bbc2aabf94b1db81030cf349a2b63c";
		
		RevWalk revWalk = new RevWalk(gitRepository);
		try {
			ObjectId id = gitRepository.resolve(commitId);
			RevCommit commit = revWalk.parseCommit(id);
			ServiceGitCommit service = new ServiceGitCommit(gitRepository);
			assertEquals(commit, service.findCommitBy(commitId));
		} finally {
			revWalk.close();
		}
	}

	@Test
	public void testReadFileFromCommit() throws NoHeadException, GitAPIException, IOException {
		String filePath = "QuantisComum/pom.xml";
		String commitId = "d3d68bd5d32593c46288e7fb051f0d7b47f9b1e9";
		
		try (RevWalk revWalk = new RevWalk(gitRepository); TreeWalk treeWalk = new TreeWalk(gitRepository)) {
			ObjectId id = gitRepository.resolve(commitId); 	
			RevCommit commit = revWalk.parseCommit(id);
			
			treeWalk.addTree(commit.getTree());
			treeWalk.setRecursive(true);
			treeWalk.setFilter(PathFilter.create(filePath));
			
			treeWalk.next();
			
			ObjectId fileId = treeWalk.getObjectId(0);
            ObjectLoader loader = gitRepository.open(fileId);
            ByteArrayOutputStream file = new ByteArrayOutputStream(); 
            loader.copyTo(file);
            
            ServiceGitCommit service = new ServiceGitCommit(gitRepository);
            assertEquals(file.toString(), service.readFileFromCommit(commit, filePath));
		}
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testFileNotFounOnReadFileFromCommit() throws NoHeadException, GitAPIException, IOException {
		String filePath = "QuantisComum/_pom_.xml";
		String commitId = "d3d68bd5d32593c46288e7fb051f0d7b47f9b1e9";
		
		try (RevWalk revWalk = new RevWalk(gitRepository); TreeWalk treeWalk = new TreeWalk(gitRepository)) {
			ObjectId id = gitRepository.resolve(commitId); 	
			RevCommit commit = revWalk.parseCommit(id);
            
            ServiceGitCommit service = new ServiceGitCommit(gitRepository);
            service.readFileFromCommit(commit, filePath);
		}
	}
}
