package org.easyreview.repositories.git;

import static org.junit.Assert.*;

import java.io.File;

import org.eclipse.jgit.lib.Repository;
import org.junit.Ignore;
import org.junit.Test;

@Ignore
public class RepositoryFactoryTest {

	@Test
	public void testCreate() {
		File directory = new File("/Backup Dell Inspiron/Desenvolvimento/Java/Quantis");
		Repository repository = RepositoryFactory.createFrom(directory);
		assertNotNull(repository);
	}

}
