package org.easyreview.repositories.git;

import static org.junit.Assert.*;

import java.io.File;

import org.junit.Test;

public class GitUpdateCommandBuilderTest {

	@Test
	public void testBuildGitSvnFetchCommand() {
		GitUpdateCommandBuilder builder = new GitSvnFetchCommandBuilder();
		
		File dir = new File("/test/repository");
		assertEquals(new GitSvnFetchCommand(dir), builder.build(dir));
	}

	@Test
	public void testBuildGitPullCommand() {
		GitUpdateCommandBuilder builder = new GitPullCommandBuilder();
		
		File dir = new File("/test/repository");
		assertEquals(new GitPullCommand(dir), builder.build(dir));
	}
}
