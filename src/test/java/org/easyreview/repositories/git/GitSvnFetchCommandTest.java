package org.easyreview.repositories.git;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.easyreview.domain.GitException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({Runtime.class, GitSvnFetchCommand.class})
public class GitSvnFetchCommandTest {
	
	@Mock
	private Runtime run;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(Runtime.class);
		when(Runtime.getRuntime()).thenReturn(run);
	}
	
	@Test @Ignore("Created to test the command execution and after formulate the tests")
	public void testCommand() throws IOException, InterruptedException {
		Runtime run = Runtime.getRuntime();
		File dir = new File("D:\\teste_mirror_svn\\trunk");
		Process process = run.exec("git svn fetch", null, dir);
		process.waitFor(10, TimeUnit.MINUTES);

		if (process.exitValue() == 0) {
			try (Scanner scanner = new Scanner(process.getInputStream())) {
				while(scanner.hasNext())
					System.out.println(scanner.nextLine());
			}
		} else {
			try (Scanner scanner = new Scanner(process.getErrorStream())) {
				while(scanner.hasNext())
					System.out.println(scanner.nextLine());
			}
		}
		
	}
	
	@Test
	public void testOk() throws IOException, InterruptedException {
		File dir = new File("/test/repository");
		Process process = mock(Process.class);
		
		when(run.exec("git svn fetch", null, dir)).thenReturn(process);
		when(process.exitValue()).thenReturn(0);
		when(process.getInputStream()).thenReturn(new ByteArrayInputStream("OK".getBytes()));
		
		GitSvnFetchCommand command = new GitSvnFetchCommand(dir);
		
		assertEquals("OK\n", command.execute());
		
		verify(process, times(1)).waitFor(10, TimeUnit.MINUTES);
	}
	
	@Test
	public void testError() throws IOException, InterruptedException {
		File dir = new File("/test/repository");
		Process process = mock(Process.class);
		
		when(run.exec("git svn fetch", null, dir)).thenReturn(process);
		when(process.exitValue()).thenReturn(258);
		when(process.getErrorStream()).thenReturn(new ByteArrayInputStream("NOK".getBytes()));
		
		GitSvnFetchCommand command = new GitSvnFetchCommand(dir);
		
		try {
			command.execute();
			fail("Should fail :(!");
		} catch (GitException e) {
			assertEquals("NOK\n", e.getMessage());
		}
		
		verify(process, times(1)).waitFor(10, TimeUnit.MINUTES);
	}

	@Test(expected=IllegalArgumentException.class) 
	public void testDirParameterCanNotBeNull() {
		new GitSvnFetchCommand(null);
	}
	
	@Test
	public void testEquals() {
		File dir = new File("/test/repository");
		assertEquals(new GitSvnFetchCommand(dir), new GitSvnFetchCommand(dir));
		assertNotEquals(new GitSvnFetchCommand(dir), new GitPullCommand(dir));
	}
}
