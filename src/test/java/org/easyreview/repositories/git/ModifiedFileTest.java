package org.easyreview.repositories.git;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.IOException;

import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(fullyQualifiedNames={"org.eclipse.jgit.revwalk.*", "org.easyreview.repositories.git.ModifiedFile"})
public class ModifiedFileTest {

	private RevCommit commit = PowerMockito.mock(RevCommit.class);
	private RevCommit parent = PowerMockito.mock(RevCommit.class);
	
	@Mock private ObjectId parendId;
	@Mock private RevCommit parsedParent;
	@Mock private Repository repository;
	@Mock private ServiceGitCommit service;
	
	private final String filePath = "/repository/file";
	private final String currentlFile = "Current File";
	private final String previousFile = "Previous File";
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		
		when(commit.getParent(0)).thenReturn(parent);
		when(parent.getId()).thenReturn(parendId);
		
		PowerMockito.whenNew(ServiceGitCommit.class).withArguments(repository).thenReturn(service);
		when(service.findCommitBy(parendId)).thenReturn(parsedParent);
	}

	@Test
	public void testGetCurrentFile() throws IOException, GitAPIException {
		when(service.readFileFromCommit(commit, filePath)).thenReturn(currentlFile);
		ModifiedFile modifiedFile = new ModifiedFile(repository, commit, filePath); 
		assertEquals(currentlFile, modifiedFile.getCurrentFile());
	}

	@Test
	public void testGetPreviousFile() throws IOException, GitAPIException {
		when(service.readFileFromCommit(parsedParent, filePath)).thenReturn(previousFile);
		ModifiedFile modifiedFile = new ModifiedFile(repository, commit, filePath); 
		assertEquals(previousFile, modifiedFile.getPreviousFile());
	}
}
