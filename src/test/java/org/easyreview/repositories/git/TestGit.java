package org.easyreview.repositories.git;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Collection;
import java.util.List;

import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.LogCommand;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRefNameException;
import org.eclipse.jgit.api.errors.NoHeadException;
import org.eclipse.jgit.diff.DiffEntry;
import org.eclipse.jgit.diff.DiffFormatter;
import org.eclipse.jgit.diff.RawTextComparator;
import org.eclipse.jgit.errors.AmbiguousObjectException;
import org.eclipse.jgit.errors.CorruptObjectException;
import org.eclipse.jgit.errors.IncorrectObjectTypeException;
import org.eclipse.jgit.errors.RevisionSyntaxException;
import org.eclipse.jgit.lib.CheckoutEntry;
import org.eclipse.jgit.lib.IndexDiff;
import org.eclipse.jgit.lib.ObjectId;
import org.eclipse.jgit.lib.ObjectLoader;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.lib.ReflogEntry;
import org.eclipse.jgit.lib.Repository;
import org.eclipse.jgit.revwalk.RevCommit;
import org.eclipse.jgit.revwalk.RevTree;
import org.eclipse.jgit.revwalk.RevWalk;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;
import org.eclipse.jgit.treewalk.FileTreeIterator;
import org.eclipse.jgit.treewalk.TreeWalk;
import org.eclipse.jgit.treewalk.filter.PathFilter;
import org.eclipse.jgit.util.io.DisabledOutputStream;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

public class TestGit {

	private Repository repository;
	
	@Before
	public void setUp() throws IOException {
		File directory = new File("D:\\Backup Dell Inspiron\\Desenvolvimento\\Java\\Quantis");
		FileRepositoryBuilder builder = new FileRepositoryBuilder();
		repository = builder.findGitDir(directory) // scan up the file system tree
							.build();
	}
	 	
	@After
	public void tearDown() {
		repository.close();
	}
	
	@Test @Ignore
	public void testaBranches() throws GitAPIException, CorruptObjectException, IOException {
		Git git = new Git(repository);
		TreeWalk treeWalk = new TreeWalk(repository);
		try {
			List<Ref> branchs = git.branchList().call();
			for(Ref branch : branchs) {
				System.out.println("---------------------------------------------------------------------");
				System.out.println(" Branch: " + branch.getName());
				System.out.println(" Commits:");
				
				LogCommand logs = git.log();
				logs = logs.add(branch.getObjectId());
				
				for(RevCommit commit : logs.call()) {
					System.out.println(" --------------------------------------------------------------------");
					System.out.println("  Committer:" + commit.getCommitterIdent());
					System.out.println("  Descrição:" + commit.getShortMessage());
					System.out.println(" ---------------------------------------------------------------------");
					
					RevTree tree = commit.getTree();
					
					treeWalk.reset();
					treeWalk.addTree(tree);
					treeWalk.setRecursive(false);
					
					System.out.println("  Arquivos: ");
					
					while (treeWalk.next()) {
						if (treeWalk.isSubtree()) {
					        treeWalk.enterSubtree();
					    } else {
					        System.out.println("   file: " + treeWalk.getPathString());
					    }
					}
				}
			}
		} finally {
			git.close();
			treeWalk.close();
		}
	}

	@Test @Ignore
	public void testaModifiedFiles() throws GitAPIException, CorruptObjectException, IOException {
		Git git = new Git(repository);
		
		try {
			List<Ref> branchs = git.branchList().call();
			for(Ref branch : branchs) {
				System.out.println("---------------------------------------------------------------------");
				System.out.println(" Branch: " + branch.getName());
				System.out.println(" Commits:");
				
				LogCommand logs = git.log();
				logs = logs.add(branch.getObjectId());
				
				for(RevCommit commit : logs.call()) {
					System.out.println(" --------------------------------------------------------------------");
					System.out.println("  ID Commit:" + commit.getName());
					System.out.println("  Committer:" + commit.getCommitterIdent());
					System.out.println("  Descrição:" + commit.getShortMessage());
					
					FileTreeIterator fileTreeIterator = new FileTreeIterator(repository);
					IndexDiff indexDiff = new IndexDiff(repository, commit.getId(), fileTreeIterator);
					
					boolean hasDiff = indexDiff.diff();
					if (hasDiff) {
						System.out.println("  Changed files :" + indexDiff.getChanged()); 
						System.out.println("  Added files :" + indexDiff.getAdded()); 
						System.out.println("  Removed files :" + indexDiff.getRemoved()); 
						System.out.println("  Missing files :" + indexDiff.getMissing()); 
						System.out.println("  Assume unchanged files :" + indexDiff.getAssumeUnchanged()); 
					}
				}
			}
			System.out.println(" ---------------------------------------------------------------------");
		} finally {
			git.close();
		}
	}

	@Test @Ignore
	public void testLog() throws IOException, NoHeadException, GitAPIException {
		Git git = new Git(repository);
		try {
			LogCommand log = git.log();
			Iterable<RevCommit> commits = log.all().call();
			
			for (RevCommit commit : commits) {
			
				System.out.println(" --------------------------------------------------------------------");
				System.out.println("  ID:" + commit.getName());
				System.out.println("  Descrição:" + commit.getFullMessage());
				System.out.println("  Committer:" + commit.getCommitterIdent().getEmailAddress());
				System.out.println("  Branch:" + getFromBranch(commit));
				
				RevCommit parent = commit.getParent(0);
				
				DiffFormatter formatter = new DiffFormatter(DisabledOutputStream.INSTANCE);
				formatter.setRepository(repository);
				formatter.setDiffComparator(RawTextComparator.DEFAULT);
				formatter.setDetectRenames(true);
				List<DiffEntry> diffs = formatter.scan(parent.getTree(), commit.getTree());
				for (DiffEntry diff : diffs) {
				    System.out.println(MessageFormat.format("({0} {1} {2}", diff.getChangeType().name(), diff.getNewMode().getBits(), diff.getNewPath()));
				}
				formatter.close();
				
				break;
//				FileTreeIterator fileTreeIterator = new FileTreeIterator(repository);
//				IndexDiff indexDiff = new IndexDiff(repository, commit.getId(), fileTreeIterator);
			
//				boolean hasDiff = indexDiff.diff();
//				if (hasDiff) {
//					System.out.println("  Changed files :" + indexDiff.getChanged()); 
//					System.out.println("  Added files :" + indexDiff.getAdded()); 
//					System.out.println("  Removed files :" + indexDiff.getRemoved()); 
//					System.out.println("  Missing files :" + indexDiff.getMissing()); 
//					System.out.println("  Assume unchanged files :" + indexDiff.getAssumeUnchanged()); 
//				}
			}
			System.out.println(" ---------------------------------------------------------------------");
		} finally {
			git.close();
		}
	}
	
	@Test @Ignore
	public void testListFiles() throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException {
		ObjectId objectId = repository.resolve("aaad445b902fe4f537138fb94b4af2ae30e84607");
		RevWalk revWalk = new RevWalk(repository);
		RevCommit commit = revWalk.parseCommit(objectId);
		revWalk.close();
		
		RevTree tree = commit.getTree();
		TreeWalk treeWalk = new TreeWalk(repository);
		try {
			treeWalk.addTree(tree);
			treeWalk.setRecursive(false);
			
			while (treeWalk.next()) {
			    if (!treeWalk.isSubtree()) {
			        treeWalk.enterSubtree();
			    } else {
			    	
			        System.out.println("file: " + treeWalk.getNameString());
			    }
			}
		} finally {
			treeWalk.close();
		}
	}
	
	@Test @Ignore
	public void testLoadFile() throws RevisionSyntaxException, AmbiguousObjectException, IncorrectObjectTypeException, IOException {
		String filePath = "QuantisComum/pom.xml";
		ObjectId objectId = repository.resolve("d3d68bd5d32593c46288e7fb051f0d7b47f9b1e9");
		RevWalk revWalk = new RevWalk(repository);
		RevCommit commit = revWalk.parseCommit(objectId);
		revWalk.close();
		
		RevTree tree = commit.getTree();
		TreeWalk treeWalk = new TreeWalk(repository);
		try {
			treeWalk.addTree(tree);
			treeWalk.setRecursive(true);
			treeWalk.setFilter(PathFilter.create(filePath));
			if (treeWalk.next()) {
				System.out.println(treeWalk.getNameString());
				
				ObjectId fileId = treeWalk.getObjectId(0);
                ObjectLoader loader = repository.open(fileId);
                ByteArrayOutputStream out = new ByteArrayOutputStream(); 
                loader.copyTo(out);
                System.out.println(out.toString());
//                ObjectReader reader = repository.newObjectReader();
//                byte[] data = reader.open(fileId).getBytes();
//                
//                String file = new String(data, "ISO-8859-8");
//                System.out.println(file);
			}
			else
				System.out.println("File not found");
		} finally {
			treeWalk.close();
		}

	
	}
	
	public String getFromBranch(RevCommit commit) throws InvalidRefNameException, GitAPIException {
		Git git = new Git(repository);
//		LogCommand logCommand = git.log();
		
		try {
	        Collection<ReflogEntry> entries = git.reflog().call();
	        for (ReflogEntry entry:entries){
	            if (!entry.getOldId().getName().equals(commit.getName())){
	                continue;
	            }

	            CheckoutEntry checkOutEntry = entry.parseCheckout();
	            if (checkOutEntry != null){
	                return checkOutEntry.getFromBranch();
	            }
	        }
		} finally {
			git.close();
		}
        return null;
	}
}
