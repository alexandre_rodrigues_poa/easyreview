package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.Email;
import org.easyreview.domain.SystemUserRepository;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContainerFactory.class)
public class EmailCanNotBeInUseValidatorTest {

	@Mock private SystemUserRepository repository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(SystemUserRepository.class)).thenReturn(repository);
	}
	
	@Test
	public void testEmailCanNotBeInUse() {
		Email email = Email.newInstance("email@domain.com");
		when(repository.emailInUse(email)).thenReturn(false);

		EmailCanNotBeInUseValidator validator = new EmailCanNotBeInUseValidator();
		assertTrue(validator.isValid(email.getAddress(), null));
		
		when(repository.emailInUse(email)).thenReturn(true);
		assertFalse(validator.isValid(email.getAddress(), null));
	}

	@Test
	public void testEmailInvalid() {
		EmailCanNotBeInUseValidator validator = new EmailCanNotBeInUseValidator();
		assertTrue(validator.isValid(" ", null));
	}
}
