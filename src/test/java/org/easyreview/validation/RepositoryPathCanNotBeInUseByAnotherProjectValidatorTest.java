package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;

import org.easyreview.controllers.projects.ProjectFormConfig;
import org.easyreview.domain.RuleProjectRepositoryPathCanNotBeInUseByAnotherProject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(RepositoryPathCanNotBeInUseByAnotherProjectValidator.class)
public class RepositoryPathCanNotBeInUseByAnotherProjectValidatorTest {

	@Mock private RuleProjectRepositoryPathCanNotBeInUseByAnotherProject rule;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testIsValid() throws Exception {
		Long id = 1L;
		String repositoryPath = "/not/in/use";
		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withArguments(new File(repositoryPath), id).thenReturn(rule);
		when(rule.inAccord()).thenReturn(true);
		
		ProjectFormConfig form = new ProjectFormConfig();
		form.setId(id);
		form.setRepositoryPath(repositoryPath);
		
		RepositoryPathCanNotBeInUseByAnotherProjectValidator validator = new RepositoryPathCanNotBeInUseByAnotherProjectValidator();
		assertTrue(validator.isValid(form, null));
	
		repositoryPath = "/in/use";

		PowerMockito.whenNew(RuleProjectRepositoryPathCanNotBeInUseByAnotherProject.class).withArguments(new File(repositoryPath), id).thenReturn(rule);
		when(rule.inAccord()).thenReturn(false);
		
		form.setRepositoryPath(repositoryPath);
		assertFalse(validator.isValid(form, null));
	}

}
