package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.controllers.users.UserFormEdit;
import org.easyreview.domain.Email;
import org.easyreview.domain.RuleEmailCanNotBeInUseByAnotherUser;
import org.easyreview.domain.SystemUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(EmailCanNotBeInUseByAnotherUserValidator.class)
public class EmailCanNotBeInUseByAnotherUserValidatorTest {

	@Mock private RuleEmailCanNotBeInUseByAnotherUser rule;
	@Mock private SystemUser systemUser;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);

	}

	@Test
	public void testIsValid() throws Exception {
		String emailAddress = "validator@email.com"; 
		Email email = Email.newInstance(emailAddress);
		long userId = 10L;
		
		when(systemUser.getId()).thenReturn(userId);
		when(systemUser.getEmailAddress()).thenReturn(emailAddress);

		PowerMockito.whenNew(RuleEmailCanNotBeInUseByAnotherUser.class).withArguments(email, userId).thenReturn(rule);

		when(rule.inAccord()).thenReturn(true);
		
		UserFormEdit user = new UserFormEdit(systemUser);
		
		EmailCanNotBeInUseByAnotherUserValidator validator = new EmailCanNotBeInUseByAnotherUserValidator();
		assertTrue(validator.isValid(user, null));
	}

	@Test
	public void testEmailInvalid() {
		long userId = 10L;
		
		when(systemUser.getId()).thenReturn(userId);
		when(systemUser.getEmailAddress()).thenReturn(null);

		UserFormEdit user = new UserFormEdit(systemUser);
		
		EmailCanNotBeInUseByAnotherUserValidator validator = new EmailCanNotBeInUseByAnotherUserValidator();
		assertTrue(validator.isValid(user, null));
	}
}
