package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.controllers.projects.ProjectFormConfig;
import org.easyreview.domain.RuleProjectNameCanNotBeInUseByAnotherProject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ProjectNameCanNotBeInUseByAnotherProjectValidator.class, RuleProjectNameCanNotBeInUseByAnotherProject.class})
public class ProjectNameCanNotBeInUseByAnotherProjectValidatorTest {

	@Mock private RuleProjectNameCanNotBeInUseByAnotherProject rule;
	@Mock private ProjectFormConfig project;
	
	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testIsValid() throws Exception {
		String projectName = "Name Not In Use";
		long projectId = 134L;
		
		when(project.getName()).thenReturn(projectName);
		when(project.getId()).thenReturn(projectId);
		
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withArguments(projectName, projectId).thenReturn(rule);
		when(rule.inAccord()).thenReturn(true);
		
		ProjectNameCanNotBeInUseByAnotherProjectValidator validator = new ProjectNameCanNotBeInUseByAnotherProjectValidator();
		assertTrue(validator.isValid(project, null));
	}

	@Test
	public void testIsNotValid() throws Exception {
		String projectName = "Name In Use";
		long projectId = 341L;
		
		when(project.getName()).thenReturn(projectName);
		when(project.getId()).thenReturn(projectId);
		
		PowerMockito.whenNew(RuleProjectNameCanNotBeInUseByAnotherProject.class).withArguments(projectName, projectId).thenReturn(rule);
		when(rule.inAccord()).thenReturn(false);
		
		ProjectNameCanNotBeInUseByAnotherProjectValidator validator = new ProjectNameCanNotBeInUseByAnotherProjectValidator();
		assertFalse(validator.isValid(project, null));
	}
}
