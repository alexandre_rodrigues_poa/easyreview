package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import java.io.File;

import org.easyreview.domain.ProjectRepository;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContainerFactory.class)
public class RepositoryPathCanNotBeInUseValidatorTest {

	@Mock private ProjectRepository repository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
	}
	
	@Test
	public void testRepositoryPathCanNotBeInUse() {
		String repositoryPath = "/repository";
		
		RepositoryPathCanNotBeInUseValidator validator = new RepositoryPathCanNotBeInUseValidator();
		
		assertTrue(validator.isValid(repositoryPath, null));
		
		when(repository.repositoryPathInUse(new File(repositoryPath))).thenReturn(true);
		
		assertFalse(validator.isValid(repositoryPath, null));
	}

	@Test
	public void testRepositoryPathNull() {
		String repositoryPath = null;
		
		RepositoryPathCanNotBeInUseValidator validator = new RepositoryPathCanNotBeInUseValidator();
		assertTrue(validator.isValid(repositoryPath, null));
	}
}
