package org.easyreview.validation;

import static org.junit.Assert.*;

import org.easyreview.controllers.users.UserFormPassword;
import org.junit.Test;

public class PasswordMacthValidatorTest {

	@Test
	public void testPasswordAndConfirmationPasswordMatch() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword("Password");
		userPassword.setConfirmPassword("Password");
		
		PasswordMacthValidator validator = new PasswordMacthValidator();
		assertTrue(validator.isValid(userPassword, null));
		
		userPassword.setConfirmPassword("Senha");
		assertFalse(validator.isValid(userPassword, null));
	}

	@Test
	public void testPasswordIsNullAndConfirmPasswordIsNotNull() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword(null);
		userPassword.setConfirmPassword("Password");
		
		PasswordMacthValidator validator = new PasswordMacthValidator();
		assertTrue(validator.isValid(userPassword, null));
	}
	
	@Test
	public void testConfirPasswordIsNullAndPasswordIsNotNull() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword("Password");
		userPassword.setConfirmPassword(null);
		
		PasswordMacthValidator validator = new PasswordMacthValidator();
		assertTrue(validator.isValid(userPassword, null));
	}

	@Test
	public void testPasswordIsNullAndConfirmPasswordIstNull() {
		UserFormPassword userPassword = new UserFormPassword();
		userPassword.setPassword(null);
		userPassword.setConfirmPassword(null);
		
		PasswordMacthValidator validator = new PasswordMacthValidator();
		assertTrue(validator.isValid(userPassword, null));
	}	
}
