package org.easyreview.validation;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.easyreview.domain.ProjectRepository;
import org.easyreview.factorys.ContainerFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

@RunWith(PowerMockRunner.class)
@PrepareForTest(ContainerFactory.class)
public class ProjectNameCanNotBeInUseValidatorTest {

	@Mock private ProjectRepository repository;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		PowerMockito.mockStatic(ContainerFactory.class);
		PowerMockito.when(ContainerFactory.get(ProjectRepository.class)).thenReturn(repository);
	}
	
	@Test
	public void testProjectNameCanNotBeInUse() {
		String name = "Project Name";
		
		ProjectNameCanNotBeInUseValidator validator = new ProjectNameCanNotBeInUseValidator();
		
		assertTrue(validator.isValid(name, null));
		
		when(repository.nameInUse(name)).thenReturn(true);
		
		assertFalse(validator.isValid(name, null));
	}
}
